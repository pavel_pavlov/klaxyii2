<?php
header("Location: /index.php?r=auth/login/index");
exit;

// change the following paths if necessary
$yii='../framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();


/*
SELECT count(ac."id") as count_ref, ac2."header", ac2.dt_create
FROM app_clients ac
JOIN app_questions aq ON aq."app_client_ref" = ac."id"
JOIN app_clients ac2 ON ac."app_client_ref" = ac2."id"
GROUP BY ac2."header", ac2.dt_create
ORDER BY dt_create, count_ref DESC
*/
