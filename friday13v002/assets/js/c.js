(function($){

	$(window).load(function(){

		if( $('.page--index').length > 0 ){

				$('#blood').animate({
					'top': '-=100px',
				}, 1000, function(){
					$('#loader').fadeOut(600)
				})
		}

		if( $('.quest-zone .row').length > 0){

			$('.quest-zone .row').isotope({
				itemSelector: '.quest',
				masonry: {
				  columnWidth: 300,
				  gutter:10,
				  isFitWidth: true
				}
			})
		}

	});
	if( $('#big-quest').length > 0){
		$("#big-quest").owlCarousel({
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true
		});
	}

	//ВЫБОР РЕГИОНОВ
	$('.regions').on('click','span', function(){
		$('.regions .cit-list').toggle();
	})
	$('.regions').on('click','.cit-list p a', function(){
		var region = $(this).text();
		$('.regions span').html(region);
		$('.regions .cit-list').fadeOut();
	})


	//НАЧАЛО: рэйтинг
	// Подгрузка дополнительных полей таблицы
	$('.rating-search').on('click','a.show-more', function(e){
		//e.preventDefault();

		/*
			Действие, ajax запрос ...
		*/
		//$('.rating-search table').append('<tr><td class="hidden-xs">13</td><td>3541</td><td>Аркадий Н.</td><td>+7(910)***-4568</td><td class="hidden-xs">1</td><td>10%</td></tr>');

	})
	/*
		или по событию скрола ...
	*/
	// $(window).on('scroll',function(e){
	// 	var scrollY = e.currentTarget.scrollY;
	// 	var height = $(document).height();
	// 	if( scrollY > height - 1300 ){
	// 			Действие, ajax запрос ...
	// 	}
	// });

	//КОНЕЦ: рейтинг


	/*====================================
	=            FILTER QUEST            =
	====================================*/

	$('.filter li a.active').click()
	// Клик по селекту
	$('.quest-zone').on('click','.filter li.select', function(){
		$('.quest-zone .filter li').css({'display':'block'});
	})
	// Клик для фильтации
	$('.quest-zone').on('click','.filter li a', function(){
		$('.quest-zone .filter li a').removeClass('active');
		$(this).addClass('active');
		var filter = $(this).attr('data-filter');

		var filter_text = $(this).text();
		$('.visible-xs-block.select').text( filter_text );
		$('.quest-zone .filter li').hide();
		$('.quest-zone .row').isotope({ filter: filter });
	})

	// Клик по выбору времени
	$('.quest-zone').on('click','.quest', function(){
		var link = $(this).attr('data-link');
		window.location.href = link;
	})

	/*-----  End of FILTER QUEST  ------*/

	/*===============================
	=            PARALAX            =
	===============================*/

		paralaxLeft(0);

		$(window).on('scroll',function(e){
			var scrollY = e.currentTarget.scrollY;
			// Паралакс
			paralaxLeft(scrollY)
		});

		$(window).resize(function(){
			resizeScreen();
		});
		resizeScreen()

		var a=1,b=1;
		// большие глаза
		var timerId = setInterval(function(e) {
			a++;
			if( a%2 == 0){
				$('.eye2').animate({'opacity': 1}, 500);
			} else{
				$('.eye2').animate({'opacity': 0}, 500);
			}
		}, 4000);
		// маленькие глаза
		var timerId = setInterval(function(e) {
			b++;
			if( b%2 == 0){
				$('.eye1').animate({'opacity': 1}, 500);
			} else{
				$('.eye1').animate({'opacity': 0}, 500);
			}
		}, 1800);
	/*-----  End of PARALAX  ------*/

	// Клик по выбору даты на мобильной версии
	$('.date-choise').on('click','td span', function(){
		$('.date-choise td span').removeClass('active');
		$(this).addClass('active');
		console.log('Действие по выбору даты! МОБИЛА');
	})
	// Клик по выбору даты на десктопной версии
	$('.date-choise-desktop').on('click','li span', function(){
		$('.date-choise-desktop li span').removeClass('active');
		$(this).addClass('active');
		console.log('Действие по выбору даты! КОМП');
	})

	// Клик по выбору времени
	$('.timing-time--item').on('click','.time span', function(){
		$('#OrderTime').modal();
	})

	// Клилк по бутерброду
	$('body').on('click','.navbar-header button', function(){
		resizeScreen()
	})

})(jQuery);


function paralaxLeft( scrollY ){
	var scrollY = $(window).scrollTop();
	$('.tree').css('top',  (0  -(scrollY*.45))+'px');
	$('.branch').css('top',(650-(scrollY*.25))+'px');
	$('.fog').css('top',   (760-(scrollY*0.37))+'px');
	$('.face').css('top',  (800-(scrollY*.29))+'px');

	$('.top_leaves').css('top',  (0  -(scrollY*.75))+'px');
	$('.mid_leaves').css('top',  (460-(scrollY*1.2))+'px');
	$('.fogs').css('top',        (400-(scrollY*.95))+'px');
	$('.top_leaves_2').css('top',(820-(scrollY*.77))+'px');

	$('.eye2').css('top',(950-(scrollY*.27))+'px');
	$('.bat').css('top',(100+(scrollY*.17))+'px');

	// сюда нужно добавить проверку на высоту контентной области по квестам
	//console.log(((scrollY*.2)-140))
	if( (50-(scrollY*.091)) > -50 ){
		$('.content-zone--index h2').css('top',(50-(scrollY*.091))+'px');
	}
	if( ((scrollY*.2)-140) < 81 && ((scrollY*.2)-140) > 2 ){
		$('footer .safe').css('top',((scrollY*.2)-140)+'px');
	} else{
		$('footer .safe').css('top','80px');
	}
}

function resizeScreen(){
	var width = $(document).width();
	var left = - (100 - (100*width)/1600);
	var right = (1600 - width)/2;

	if( width < 1600){
		$('.tree').css({'left':left+'vw'});
		$('.branch').css({'left':left+'vw'});
		$('.fog').css({'left':left+'vw'});
		$('.face').css({'left':left+'vw'});

		$('.top_leaves').css({'width':320 - right+'px'});
		$('.mid_leaves').css({'width':352 - right+'px'});
		$('.fogs').css({'width':868 - right+'px'});
		$('.top_leaves_2').css({'width':311 - right+'px'});
		$('.bat').show();
		$('.bat').css({'right':270 - right+'px'});
	}
	if( width < 1077){
		$('.bat').hide();
	}


}