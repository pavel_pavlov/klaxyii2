/*
Navicat PGSQL Data Transfer

Source Server         : n2 PostgreSQL
Source Server Version : 90403
Source Host           : n2.userdev.ru:54111
Source Database       : clients_dev
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90403
File Encoding         : 65001

Date: 2015-09-15 11:40:03
*/


-- ----------------------------
-- Sequence structure for acl_users_id_seq
-- ----------------------------
DROP SEQUENCE "public"."acl_users_id_seq";
CREATE SEQUENCE "public"."acl_users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."acl_users_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for app_additional_fields_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_additional_fields_id_seq";
CREATE SEQUENCE "public"."app_additional_fields_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_client_field_values_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_client_field_values_id_seq";
CREATE SEQUENCE "public"."app_client_field_values_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_client_has_groups_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_client_has_groups_id_seq";
CREATE SEQUENCE "public"."app_client_has_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 54
 CACHE 1;
SELECT setval('"public"."app_client_has_groups_id_seq"', 54, true);

-- ----------------------------
-- Sequence structure for app_client_referal_counts_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_client_referal_counts_id_seq";
CREATE SEQUENCE "public"."app_client_referal_counts_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_clients_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_clients_id_seq";
CREATE SEQUENCE "public"."app_clients_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;
SELECT setval('"public"."app_clients_id_seq"', 24, true);

-- ----------------------------
-- Sequence structure for app_condition_types_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_condition_types_id_seq";
CREATE SEQUENCE "public"."app_condition_types_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."app_condition_types_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for app_conditionss_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_conditionss_id_seq";
CREATE SEQUENCE "public"."app_conditionss_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."app_conditionss_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for app_contact_field_values_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_contact_field_values_id_seq";
CREATE SEQUENCE "public"."app_contact_field_values_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 44
 CACHE 1;
SELECT setval('"public"."app_contact_field_values_id_seq"', 44, true);

-- ----------------------------
-- Sequence structure for app_contact_fields_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_contact_fields_id_seq";
CREATE SEQUENCE "public"."app_contact_fields_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;
SELECT setval('"public"."app_contact_fields_id_seq"', 10, true);

-- ----------------------------
-- Sequence structure for app_field_groups_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_field_groups_id_seq";
CREATE SEQUENCE "public"."app_field_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_links_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_links_id_seq";
CREATE SEQUENCE "public"."app_links_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 22
 CACHE 1;
SELECT setval('"public"."app_links_id_seq"', 22, true);

-- ----------------------------
-- Sequence structure for app_promos_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_promos_id_seq";
CREATE SEQUENCE "public"."app_promos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_questions_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_questions_id_seq";
CREATE SEQUENCE "public"."app_questions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 16
 CACHE 1;
SELECT setval('"public"."app_questions_id_seq"', 16, true);

-- ----------------------------
-- Sequence structure for app_rating_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_rating_id_seq";
CREATE SEQUENCE "public"."app_rating_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 39
 CACHE 1;
SELECT setval('"public"."app_rating_id_seq"', 39, true);

-- ----------------------------
-- Sequence structure for app_referals_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_referals_id_seq";
CREATE SEQUENCE "public"."app_referals_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_referers_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_referers_id_seq";
CREATE SEQUENCE "public"."app_referers_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_responsibles_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_responsibles_id_seq";
CREATE SEQUENCE "public"."app_responsibles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 22
 CACHE 1;
SELECT setval('"public"."app_responsibles_id_seq"', 22, true);

-- ----------------------------
-- Sequence structure for app_reward_types_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_reward_types_id_seq";
CREATE SEQUENCE "public"."app_reward_types_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."app_reward_types_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for app_sms_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_sms_id_seq";
CREATE SEQUENCE "public"."app_sms_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_sms_queue_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_sms_queue_id_seq";
CREATE SEQUENCE "public"."app_sms_queue_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_ure_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_ure_id_seq";
CREATE SEQUENCE "public"."app_ure_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_user_groups_id_seq
-- ----------------------------
DROP SEQUENCE "public"."app_user_groups_id_seq";
CREATE SEQUENCE "public"."app_user_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."app_user_groups_id_seq"', 4, true);

-- ----------------------------
-- Table structure for acl_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."acl_users";
CREATE TABLE "public"."acl_users" (
"id" int8 DEFAULT nextval('acl_users_id_seq'::regclass) NOT NULL,
"email" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"dt_create" timestamp(6) DEFAULT now(),
"dt_update" timestamp(6) DEFAULT now(),
"is_deleted" bool DEFAULT false NOT NULL,
"is_active" bool DEFAULT true NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of acl_users
-- ----------------------------
INSERT INTO "public"."acl_users" VALUES ('1', 'admin@mail.ru', '4297f44b13955235245b2497399d7a93', '2015-09-01 16:39:04.774535', '2015-09-01 16:39:04.774535', 'f', 't');
INSERT INTO "public"."acl_users" VALUES ('2', 'klaxwork@mail.ru', '81dc9bdb52d04dc20036dbd8313ed055', '2015-09-11 16:16:53.482712', '2015-09-11 16:16:53.482712', 'f', 't');

-- ----------------------------
-- Table structure for app_additional_fields
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_additional_fields";
CREATE TABLE "public"."app_additional_fields" (
"id" int8 DEFAULT nextval('app_additional_fields_id_seq'::regclass) NOT NULL,
"field_type" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"measure" varchar(255) COLLATE "default",
"is_multiple" bool,
"number" int8,
"string" varchar(255) COLLATE "default",
"dt" timestamp(6),
"list" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_additional_fields
-- ----------------------------

-- ----------------------------
-- Table structure for app_client_field_values
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_client_field_values";
CREATE TABLE "public"."app_client_field_values" (
"id" int8 DEFAULT nextval('app_client_field_values_id_seq'::regclass) NOT NULL,
"app_client_ref" int8,
"app_field_ref" int8,
"val_number" int8,
"val_string" text COLLATE "default",
"val_dt" timestamp(6),
"val_list" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_client_field_values
-- ----------------------------

-- ----------------------------
-- Table structure for app_client_has_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_client_has_groups";
CREATE TABLE "public"."app_client_has_groups" (
"id" int8 DEFAULT nextval('app_client_has_groups_id_seq'::regclass) NOT NULL,
"app_client_ref" int8 NOT NULL,
"app_user_group_ref" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_client_has_groups
-- ----------------------------
INSERT INTO "public"."app_client_has_groups" VALUES ('1', '1', '1');
INSERT INTO "public"."app_client_has_groups" VALUES ('2', '1', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('3', '2', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('4', '3', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('5', '4', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('6', '5', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('7', '6', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('8', '7', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('9', '8', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('10', '9', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('11', '10', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('12', '11', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('13', '12', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('14', '13', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('15', '14', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('16', '15', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('17', '16', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('18', '17', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('19', '18', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('20', '19', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('21', '20', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('22', '21', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('23', '22', '2');
INSERT INTO "public"."app_client_has_groups" VALUES ('54', '24', '4');

-- ----------------------------
-- Table structure for app_client_referal_counts
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_client_referal_counts";
CREATE TABLE "public"."app_client_referal_counts" (
"id" int8 DEFAULT nextval('app_client_referal_counts_id_seq'::regclass) NOT NULL,
"app_client_ref" int8 NOT NULL,
"referal_count" int8 DEFAULT 0
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_client_referal_counts
-- ----------------------------

-- ----------------------------
-- Table structure for app_clients
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_clients";
CREATE TABLE "public"."app_clients" (
"id" int8 DEFAULT nextval('app_clients_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"phones" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"dt_create" timestamp(6) DEFAULT now(),
"dt_update" timestamp(6) DEFAULT now(),
"is_deleted" bool DEFAULT false NOT NULL,
"is_active" bool DEFAULT true NOT NULL,
"acl_user_ref" int8 NOT NULL,
"password" varchar(255) COLLATE "default",
"sms_allow" bool DEFAULT false NOT NULL,
"first_name" varchar(255) COLLATE "default",
"second_name" varchar(255) COLLATE "default",
"middle_name" varchar(255) COLLATE "default",
"header" varchar(255) COLLATE "default",
"manager" varchar(255) COLLATE "default",
"type" int2,
"status" int2,
"url" varchar(255) COLLATE "default",
"app_client_ref" int8,
"is_hide_in_rating" bool DEFAULT false
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_clients"."sms_allow" IS 'Разрешение в получении sms сообщений';
COMMENT ON COLUMN "public"."app_clients"."first_name" IS 'Имя';
COMMENT ON COLUMN "public"."app_clients"."second_name" IS 'Фамилия';
COMMENT ON COLUMN "public"."app_clients"."middle_name" IS 'Отчество';
COMMENT ON COLUMN "public"."app_clients"."header" IS 'Название организации';
COMMENT ON COLUMN "public"."app_clients"."manager" IS 'Менеджер';
COMMENT ON COLUMN "public"."app_clients"."app_client_ref" IS 'Id реферера.';
COMMENT ON COLUMN "public"."app_clients"."is_hide_in_rating" IS 'Показывать ли в рейтинге';

-- ----------------------------
-- Records of app_clients
-- ----------------------------
INSERT INTO "public"."app_clients" VALUES ('1', null, 'deadmin@mail.ru', '89139454170', null, '2015-09-01 17:03:32.484386', '2015-09-01 17:05:20.789081', 'f', 't', '1', null, 'f', null, null, null, 'Администратор', null, null, null, null, null, 't');
INSERT INTO "public"."app_clients" VALUES ('2', null, null, null, null, '2015-09-08 16:26:54.61383', '2015-09-08 16:26:54.61383', 'f', 't', '1', null, 'f', null, null, null, 'ALEXEY DUDIN', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('3', null, null, null, null, '2015-09-08 17:03:58.31502', '2015-09-08 17:03:58.31502', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('4', null, null, null, null, '2015-09-08 17:30:24.638389', '2015-09-08 17:30:24.638389', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('5', null, null, null, null, '2015-09-08 17:34:18.508611', '2015-09-08 17:34:18.508611', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '3', 'f');
INSERT INTO "public"."app_clients" VALUES ('6', null, null, null, null, '2015-09-08 17:34:46.853259', '2015-09-08 17:34:46.853259', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '3', 'f');
INSERT INTO "public"."app_clients" VALUES ('7', null, null, null, null, '2015-09-08 17:38:29.760196', '2015-09-08 17:38:29.760196', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('8', null, null, null, null, '2015-09-08 18:06:21.130022', '2015-09-08 18:06:21.130022', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('9', null, null, null, null, '2015-09-08 18:06:38.783756', '2015-09-08 18:06:38.783756', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('10', null, null, null, null, '2015-09-08 18:14:09.107294', '2015-09-08 18:14:09.107294', 'f', 't', '1', null, 'f', null, null, null, 'Денис Лебедев', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('11', null, null, null, null, '2015-09-08 18:16:00.485565', '2015-09-08 18:16:00.485565', 'f', 't', '1', null, 'f', null, null, null, 'Денис Лебедев', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('12', null, null, null, null, '2015-09-08 18:17:46.063732', '2015-09-08 18:17:46.063732', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('13', null, null, null, null, '2015-09-08 18:19:56.788406', '2015-09-08 18:19:56.788406', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('14', null, null, null, null, '2015-09-08 18:22:39.422524', '2015-09-08 18:22:39.422524', 'f', 't', '1', null, 'f', null, null, null, 'Денис2 Лебедев2', null, null, null, null, '10', 'f');
INSERT INTO "public"."app_clients" VALUES ('15', null, null, null, null, '2015-09-08 18:23:23.809138', '2015-09-08 18:23:23.809138', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('16', null, null, null, null, '2015-09-08 19:39:07.670969', '2015-09-08 19:39:07.670969', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('17', null, null, null, null, '2015-09-08 19:49:07.32332', '2015-09-08 19:49:07.32332', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('18', null, null, null, null, '2015-09-09 19:46:21.170836', '2015-09-09 19:46:21.170836', 'f', 't', '1', null, 'f', null, null, null, 'noname', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('19', null, null, null, null, '2015-09-11 15:00:48.660087', '2015-09-11 15:00:48.660087', 'f', 't', '1', null, 'f', null, null, null, 'Pavel3 Pavlov3', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('20', null, 'klaxwork@mail.ru12', '89806508512', null, '2015-09-11 15:07:57.094091', '2015-09-11 15:07:57.094091', 'f', 't', '1', null, 'f', null, null, null, 'Pavel1 Pavlov2', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('21', null, 'klaxwork@mail.ru', '+79806508567', null, '2015-09-14 19:56:23.70949', '2015-09-14 19:56:23.70949', 'f', 't', '1', null, 'f', null, null, null, 'qwe6 qwe7', null, null, null, null, '1', 'f');
INSERT INTO "public"."app_clients" VALUES ('22', null, 'klaxwork@mail.ru', '+79806508557', null, '2015-09-14 20:37:03.699436', '2015-09-14 20:37:03.699436', 'f', 't', '1', null, 'f', null, null, null, '555 777', null, null, null, null, '9', 'f');
INSERT INTO "public"."app_clients" VALUES ('24', null, '', '', '', '2015-09-15 12:39:36.27', '2015-09-15 13:53:15.946585', 'f', 't', '2', '', 'f', null, null, null, 'Павлов Павел Анатольевич', '', null, null, '', null, 't');

-- ----------------------------
-- Table structure for app_condition_types
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_condition_types";
CREATE TABLE "public"."app_condition_types" (
"id" int8 DEFAULT nextval('app_condition_types_id_seq'::regclass) NOT NULL,
"condition_name" varchar(255) COLLATE "default",
"condition_type" varchar(255) COLLATE "default",
"for" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_condition_types"."condition_name" IS 'Название условия';
COMMENT ON COLUMN "public"."app_condition_types"."condition_type" IS 'Тип условия';
COMMENT ON COLUMN "public"."app_condition_types"."for" IS 'referer или referal';

-- ----------------------------
-- Records of app_condition_types
-- ----------------------------
INSERT INTO "public"."app_condition_types" VALUES ('1', 'Бонус за одну покупку рефералом', 'buy_once', 'referer');
INSERT INTO "public"."app_condition_types" VALUES ('2', 'Скидка на одноразовую покупку', 'buy_once', 'referal');
INSERT INTO "public"."app_condition_types" VALUES ('3', 'Бонус за регистрацию', 'reg', 'referer');
INSERT INTO "public"."app_condition_types" VALUES ('4', 'Бонус за регистрацию реферала', 'reg', 'referal');

-- ----------------------------
-- Table structure for app_conditions
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_conditions";
CREATE TABLE "public"."app_conditions" (
"id" int8 DEFAULT nextval('app_conditionss_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"condition_type_ref" int8 NOT NULL,
"reward_type_ref" int8 NOT NULL,
"value" float4 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_conditions
-- ----------------------------
INSERT INTO "public"."app_conditions" VALUES ('1', 'Условие для реферера: 2 % от стоимости покупки рефералом', '1', '1', '2');
INSERT INTO "public"."app_conditions" VALUES ('2', 'Условие для реферала: скидка 10 % на одну покупку', '2', '2', '10');

-- ----------------------------
-- Table structure for app_contact_field_values
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_contact_field_values";
CREATE TABLE "public"."app_contact_field_values" (
"id" int8 DEFAULT nextval('app_contact_field_values_id_seq'::regclass) NOT NULL,
"app_resp_ref" int8 NOT NULL,
"field_ref" int8 NOT NULL,
"field_value" varchar(255) COLLATE "default",
"send_allow" bool DEFAULT false
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_contact_field_values
-- ----------------------------
INSERT INTO "public"."app_contact_field_values" VALUES ('1', '1', '2', 'alex@phelex.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('2', '1', '6', '89139454170', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('3', '2', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('4', '2', '6', '+79806508500', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('5', '3', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('6', '3', '6', '+79806507654', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('7', '4', '2', 'klaxwork@yandex.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('8', '4', '6', '+79806508546', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('9', '5', '2', 'klaxwork@yandex.rux', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('10', '5', '6', '+79806508547', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('11', '6', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('12', '6', '6', '+79806508512', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('13', '7', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('14', '7', '6', '+79806508579', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('15', '8', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('16', '8', '6', '+79806508556', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('17', '9', '2', 'denis@nays.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('18', '9', '6', '89159999992', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('19', '10', '2', 'denis@nays.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('20', '10', '6', '89159999993', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('21', '11', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('22', '11', '6', '+79806508544', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('23', '12', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('24', '12', '6', '+79806508577', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('25', '13', '2', 'info@nays.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('26', '13', '6', '89159999994', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('27', '14', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('28', '14', '6', '+79806508599', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('29', '15', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('30', '15', '6', '+79806508501', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('31', '16', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('32', '16', '6', '+79806508502', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('33', '17', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('34', '17', '6', '+79806508545', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('35', '18', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('36', '18', '6', '+79806508533', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('37', '19', '2', 'klaxwork@mail.ru12', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('38', '19', '6', '89806508512', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('39', '20', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('40', '20', '6', '+79806508567', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('41', '21', '2', 'klaxwork@mail.ru', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('42', '21', '6', '+79806508557', 'f');
INSERT INTO "public"."app_contact_field_values" VALUES ('43', '22', '2', 'asdf', 't');
INSERT INTO "public"."app_contact_field_values" VALUES ('44', '22', '9', 'klaxwork', 'f');

-- ----------------------------
-- Table structure for app_contact_fields
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_contact_fields";
CREATE TABLE "public"."app_contact_fields" (
"id" int8 DEFAULT nextval('app_contact_fields_id_seq'::regclass) NOT NULL,
"field_type" varchar(255) COLLATE "default",
"field_label" varchar(255) COLLATE "default",
"send_allow" bool DEFAULT false
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_contact_fields
-- ----------------------------
INSERT INTO "public"."app_contact_fields" VALUES ('1', 'url', 'URL сайта', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('2', 'email', 'E-mail', 't');
INSERT INTO "public"."app_contact_fields" VALUES ('3', 'address_fact', 'Адрес проживания/фактический', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('4', 'address_reg', 'Адрес регистрации/юридический', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('5', 'address_post', 'Адрес почтовый', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('6', 'phone_mobile', 'Мобильный телефон', 't');
INSERT INTO "public"."app_contact_fields" VALUES ('7', 'phone_work', 'Рабочий телефон', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('8', 'phone_fax', 'Факс', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('9', 'skype', 'Skype', 'f');
INSERT INTO "public"."app_contact_fields" VALUES ('10', 'icq', 'ICQ', 'f');

-- ----------------------------
-- Table structure for app_field_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_field_groups";
CREATE TABLE "public"."app_field_groups" (
"id" int8 DEFAULT nextval('app_field_groups_id_seq'::regclass) NOT NULL,
"app_field_ref" int8,
"app_group_ref" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_field_groups
-- ----------------------------

-- ----------------------------
-- Table structure for app_links
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_links";
CREATE TABLE "public"."app_links" (
"id" int8 DEFAULT nextval('app_links_id_seq'::regclass) NOT NULL,
"link" varchar(255) COLLATE "default",
"referer_condition_ref" int8 DEFAULT 1,
"referal_condition_ref" int8 DEFAULT 1,
"app_client_ref" int8 NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_links"."link" IS 'Ссылка';
COMMENT ON COLUMN "public"."app_links"."referer_condition_ref" IS 'Условие вознаграждения для реферера';
COMMENT ON COLUMN "public"."app_links"."referal_condition_ref" IS 'Условие вознаграждения для реферала';
COMMENT ON COLUMN "public"."app_links"."app_client_ref" IS 'Id владельца ссылки';

-- ----------------------------
-- Records of app_links
-- ----------------------------
INSERT INTO "public"."app_links" VALUES ('1', 'http://klaxyii.n2.userdev.ru/?ref=2', '1', '1', '2');
INSERT INTO "public"."app_links" VALUES ('2', 'http://klaxyii.n2.userdev.ru/?ref=3', '1', '1', '3');
INSERT INTO "public"."app_links" VALUES ('3', 'http://klaxyii.n2.userdev.ru/?ref=4', '1', '1', '4');
INSERT INTO "public"."app_links" VALUES ('4', 'http://klaxyii.n2.userdev.ru/?ref=5', '1', '1', '5');
INSERT INTO "public"."app_links" VALUES ('5', 'http://klaxyii.n2.userdev.ru/?ref=6', '1', '1', '6');
INSERT INTO "public"."app_links" VALUES ('6', 'http://klaxyii.n2.userdev.ru/?ref=7', '1', '1', '7');
INSERT INTO "public"."app_links" VALUES ('7', 'http://klaxyii.n2.userdev.ru/?ref=8', '1', '1', '8');
INSERT INTO "public"."app_links" VALUES ('8', 'http://klaxyii.n2.userdev.ru/?ref=9', '1', '1', '9');
INSERT INTO "public"."app_links" VALUES ('9', 'http://klaxyii.n2.userdev.ru/?ref=10', '1', '1', '10');
INSERT INTO "public"."app_links" VALUES ('10', 'http://klaxyii.n2.userdev.ru/?ref=11', '1', '1', '11');
INSERT INTO "public"."app_links" VALUES ('11', 'http://klaxyii.n2.userdev.ru/?ref=12', '1', '1', '12');
INSERT INTO "public"."app_links" VALUES ('12', 'http://klaxyii.n2.userdev.ru/?ref=13', '1', '1', '13');
INSERT INTO "public"."app_links" VALUES ('13', 'http://klaxyii.n2.userdev.ru/?ref=14', '1', '1', '14');
INSERT INTO "public"."app_links" VALUES ('14', 'http://klaxyii.n2.userdev.ru/?ref=15', '1', '1', '15');
INSERT INTO "public"."app_links" VALUES ('15', 'http://klaxyii.n2.userdev.ru/?ref=16', '1', '1', '16');
INSERT INTO "public"."app_links" VALUES ('16', 'http://klaxyii.n2.userdev.ru/?ref=17', '1', '1', '17');
INSERT INTO "public"."app_links" VALUES ('17', 'http://klaxyii.n2.userdev.ru/?ref=18', '1', '1', '18');
INSERT INTO "public"."app_links" VALUES ('18', 'http://klaxyii.n2.userdev.ru/?ref=19', '1', '1', '19');
INSERT INTO "public"."app_links" VALUES ('19', 'http://klaxyii.n2.userdev.ru/?ref=20', '1', '1', '20');
INSERT INTO "public"."app_links" VALUES ('20', 'http://klaxyii.n2.userdev.ru/?ref=21', '1', '1', '21');
INSERT INTO "public"."app_links" VALUES ('21', 'http://klaxyii.n2.userdev.ru/?ref=22', '1', '1', '22');
INSERT INTO "public"."app_links" VALUES ('22', 'klaxyii.n2.userdev.ru/?ref=24', '1', '1', '24');

-- ----------------------------
-- Table structure for app_promos
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_promos";
CREATE TABLE "public"."app_promos" (
"id" int8 DEFAULT nextval('app_promos_id_seq'::regclass) NOT NULL,
"promo" varchar(255) COLLATE "default" NOT NULL,
"acl_user_ref" int8 NOT NULL,
"dt_create" timestamp(6) DEFAULT now() NOT NULL,
"dt_start" timestamp(6) NOT NULL,
"dt_finish" timestamp(6),
"dt_activate" timestamp(6),
"referer_condition_ref" int8,
"referal_condition_ref" int8
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_promos"."promo" IS 'Пример кода: 123456-7890AB-CDEF12';
COMMENT ON COLUMN "public"."app_promos"."acl_user_ref" IS 'Админ магазина';
COMMENT ON COLUMN "public"."app_promos"."dt_create" IS 'Дата создания';
COMMENT ON COLUMN "public"."app_promos"."dt_start" IS 'Дата начала действия кода';
COMMENT ON COLUMN "public"."app_promos"."dt_finish" IS 'Дата окончания действия';

-- ----------------------------
-- Records of app_promos
-- ----------------------------

-- ----------------------------
-- Table structure for app_questions
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_questions";
CREATE TABLE "public"."app_questions" (
"id" int8 DEFAULT nextval('app_questions_id_seq'::regclass) NOT NULL,
"app_client_ref" int8,
"req1" int2,
"req2" int2,
"req3" int2,
"req4" int2,
"req5" int2,
"req6" int2,
"req7" int2,
"req0" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_questions
-- ----------------------------
INSERT INTO "public"."app_questions" VALUES ('1', '2', '5', null, null, null, null, '1', '1', null);
INSERT INTO "public"."app_questions" VALUES ('2', '3', null, null, null, '5', null, null, null, null);
INSERT INTO "public"."app_questions" VALUES ('3', '5', '4', '1', '3', '5', '1', '2', '4', null);
INSERT INTO "public"."app_questions" VALUES ('4', '6', '4', '1', '3', '5', '1', '2', '4', null);
INSERT INTO "public"."app_questions" VALUES ('5', '7', '1', null, null, '1', null, null, null, null);
INSERT INTO "public"."app_questions" VALUES ('6', '9', '5', null, null, null, '1', null, null, null);
INSERT INTO "public"."app_questions" VALUES ('7', '10', '1', '1', '1', '1', '1', '1', '1', null);
INSERT INTO "public"."app_questions" VALUES ('8', '13', null, null, null, null, '1', '1', null, null);
INSERT INTO "public"."app_questions" VALUES ('9', '14', '2', '1', '1', '3', '1', '1', '2', null);
INSERT INTO "public"."app_questions" VALUES ('10', '15', '1', '2', null, '1', '1', '2', '4', null);
INSERT INTO "public"."app_questions" VALUES ('11', '16', '5', null, null, null, null, null, '4', null);
INSERT INTO "public"."app_questions" VALUES ('12', '17', null, null, '1', '1', null, null, null, null);
INSERT INTO "public"."app_questions" VALUES ('15', '21', '1', '1', '1', '1', '1', '1', '1', null);
INSERT INTO "public"."app_questions" VALUES ('16', '22', '1', '1', '4', '5', '1', '2', '4', null);

-- ----------------------------
-- Table structure for app_rating
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_rating";
CREATE TABLE "public"."app_rating" (
"id" int8 DEFAULT nextval('app_rating_id_seq'::regclass) NOT NULL,
"place" int4,
"refs_count" int4,
"app_clients_ref" int8 NOT NULL,
"phone" varchar COLLATE "default",
"view_name" varchar COLLATE "default",
"dt_create" date,
"discount" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_rating
-- ----------------------------
INSERT INTO "public"."app_rating" VALUES ('36', '1', '10', '1', 'N/A', 'Администратор', '2015-09-01', '100');
INSERT INTO "public"."app_rating" VALUES ('37', '2', '2', '3', '+79806508500', 'noname', '2015-09-08', '100');
INSERT INTO "public"."app_rating" VALUES ('38', '3', '1', '10', '89159999992', 'Денис Лебедев', '2015-09-08', '100');
INSERT INTO "public"."app_rating" VALUES ('39', '4', '1', '9', '+79806508556', 'noname', '2015-09-08', '100');

-- ----------------------------
-- Table structure for app_referals
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_referals";
CREATE TABLE "public"."app_referals" (
"id" int8 DEFAULT nextval('app_referals_id_seq'::regclass) NOT NULL,
"app_promo_ref" int8 NOT NULL,
"app_client_ref" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_referals
-- ----------------------------

-- ----------------------------
-- Table structure for app_referers
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_referers";
CREATE TABLE "public"."app_referers" (
"id" int8 DEFAULT nextval('app_referers_id_seq'::regclass) NOT NULL,
"app_promo_ref" int8 NOT NULL,
"app_client_ref" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_referers
-- ----------------------------

-- ----------------------------
-- Table structure for app_requisites
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_requisites";
CREATE TABLE "public"."app_requisites" (
"id" int8 DEFAULT nextval('app_ure_id_seq'::regclass) NOT NULL,
"app_client_ref" int8 NOT NULL,
"full_name" varchar(255) COLLATE "default",
"address_juridical" varchar(255) COLLATE "default",
"address_fact" varchar(255) COLLATE "default",
"inn" varchar(255) COLLATE "default",
"kpp" varchar(255) COLLATE "default",
"ogrn" varchar(255) COLLATE "default",
"bank" varchar(255) COLLATE "default",
"bik" varchar(255) COLLATE "default",
"r_schet" varchar(255) COLLATE "default",
"kor_schet" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_requisites"."app_client_ref" IS 'Id клиента';
COMMENT ON COLUMN "public"."app_requisites"."full_name" IS 'Полное наименование организации';
COMMENT ON COLUMN "public"."app_requisites"."address_juridical" IS 'Юридический адрес';
COMMENT ON COLUMN "public"."app_requisites"."address_fact" IS 'Фактический адрес';
COMMENT ON COLUMN "public"."app_requisites"."inn" IS 'ИНН';
COMMENT ON COLUMN "public"."app_requisites"."kpp" IS 'КПП';
COMMENT ON COLUMN "public"."app_requisites"."ogrn" IS 'ОГРН';
COMMENT ON COLUMN "public"."app_requisites"."bank" IS 'Банк';
COMMENT ON COLUMN "public"."app_requisites"."bik" IS 'БИК';
COMMENT ON COLUMN "public"."app_requisites"."r_schet" IS 'Расчетный счет';
COMMENT ON COLUMN "public"."app_requisites"."kor_schet" IS 'Корреспондентский счет';

-- ----------------------------
-- Records of app_requisites
-- ----------------------------

-- ----------------------------
-- Table structure for app_responsibles
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_responsibles";
CREATE TABLE "public"."app_responsibles" (
"id" int8 DEFAULT nextval('app_responsibles_id_seq'::regclass) NOT NULL,
"password" varchar(255) COLLATE "default",
"position" varchar(255) COLLATE "default",
"app_client_ref" int8 NOT NULL,
"is_responsible" bool DEFAULT false,
"first_name" varchar(255) COLLATE "default",
"second_name" varchar(255) COLLATE "default",
"middle_name" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_responsibles"."password" IS 'Пароль ответственного лица';
COMMENT ON COLUMN "public"."app_responsibles"."position" IS 'Должность';
COMMENT ON COLUMN "public"."app_responsibles"."app_client_ref" IS 'Id клиента, которому принадлежит ответственное лицо';
COMMENT ON COLUMN "public"."app_responsibles"."is_responsible" IS 'Является ли это лицо ответственным';
COMMENT ON COLUMN "public"."app_responsibles"."first_name" IS 'Имя';
COMMENT ON COLUMN "public"."app_responsibles"."second_name" IS 'Фамилия';
COMMENT ON COLUMN "public"."app_responsibles"."middle_name" IS 'Отчество';

-- ----------------------------
-- Records of app_responsibles
-- ----------------------------
INSERT INTO "public"."app_responsibles" VALUES ('1', null, null, '2', 't', 'ALEXEY', 'DUDIN', null);
INSERT INTO "public"."app_responsibles" VALUES ('2', null, null, '3', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('3', null, null, '4', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('4', null, null, '5', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('5', null, null, '6', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('6', null, null, '7', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('7', null, null, '8', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('8', null, null, '9', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('9', null, null, '10', 't', 'Денис', 'Лебедев', null);
INSERT INTO "public"."app_responsibles" VALUES ('10', null, null, '11', 't', 'Денис', 'Лебедев', null);
INSERT INTO "public"."app_responsibles" VALUES ('11', null, null, '12', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('12', null, null, '13', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('13', null, null, '14', 't', 'Денис2', 'Лебедев2', null);
INSERT INTO "public"."app_responsibles" VALUES ('14', null, null, '15', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('15', null, null, '16', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('16', null, null, '17', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('17', null, null, '18', 't', '', '', null);
INSERT INTO "public"."app_responsibles" VALUES ('18', null, null, '19', 't', 'Pavel3', 'Pavlov3', null);
INSERT INTO "public"."app_responsibles" VALUES ('19', null, null, '20', 't', 'Pavel1', 'Pavlov2', null);
INSERT INTO "public"."app_responsibles" VALUES ('20', null, null, '21', 't', 'qwe6', 'qwe7', null);
INSERT INTO "public"."app_responsibles" VALUES ('21', null, null, '22', 't', '555', '777', null);
INSERT INTO "public"."app_responsibles" VALUES ('22', '', 'Программист', '24', 't', 'Павел', 'Павлов', 'Анатольевич');

-- ----------------------------
-- Table structure for app_reward_types
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_reward_types";
CREATE TABLE "public"."app_reward_types" (
"id" int8 DEFAULT nextval('app_reward_types_id_seq'::regclass) NOT NULL,
"reward_name" varchar(255) COLLATE "default",
"reward_type" varchar(255) COLLATE "default",
"for" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_reward_types"."reward_name" IS 'Название вознаграждения';
COMMENT ON COLUMN "public"."app_reward_types"."reward_type" IS 'Тип вознаграждения';
COMMENT ON COLUMN "public"."app_reward_types"."for" IS 'referer или referal';

-- ----------------------------
-- Records of app_reward_types
-- ----------------------------
INSERT INTO "public"."app_reward_types" VALUES ('1', 'Получение бонуса в % от стоимости покупки рефералом', '%', 'referer');
INSERT INTO "public"."app_reward_types" VALUES ('2', 'Получение скидки на % от стоимости товара', '%', 'referal');
INSERT INTO "public"."app_reward_types" VALUES ('3', 'Получение бонуса в RUR на счет', 'RUR', 'referer');
INSERT INTO "public"."app_reward_types" VALUES ('4', 'Получение бонуса в RUR на счет', 'RUR', 'referal');

-- ----------------------------
-- Table structure for app_sms_messages
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_sms_messages";
CREATE TABLE "public"."app_sms_messages" (
"id" int8 DEFAULT nextval('app_sms_id_seq'::regclass) NOT NULL,
"sms_text" varchar(255) COLLATE "default" NOT NULL,
"dt_create" timestamp(6) DEFAULT now(),
"acl_users_ref" int8 NOT NULL,
"app_user_group_ref" int8,
"title" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_sms_messages"."id" IS 'Id сообщения';
COMMENT ON COLUMN "public"."app_sms_messages"."sms_text" IS 'Текст сообщения';
COMMENT ON COLUMN "public"."app_sms_messages"."dt_create" IS 'Дата-время отправки';
COMMENT ON COLUMN "public"."app_sms_messages"."acl_users_ref" IS 'Id отправителя';
COMMENT ON COLUMN "public"."app_sms_messages"."app_user_group_ref" IS 'Id группы получателей';
COMMENT ON COLUMN "public"."app_sms_messages"."title" IS 'Название рассылки';

-- ----------------------------
-- Records of app_sms_messages
-- ----------------------------

-- ----------------------------
-- Table structure for app_sms_queue
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_sms_queue";
CREATE TABLE "public"."app_sms_queue" (
"id" int8 DEFAULT nextval('app_sms_queue_id_seq'::regclass) NOT NULL,
"app_sms_messages_ref" int8 NOT NULL,
"is_sended" bool,
"is_delivered" bool,
"app_client_ref" int8 NOT NULL,
"dt_sended" timestamp(6),
"dt_delivered" timestamp(6),
"message_id" varchar(255) COLLATE "default",
"status" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_sms_queue"."id" IS 'Id запроса';
COMMENT ON COLUMN "public"."app_sms_queue"."app_sms_messages_ref" IS 'Id сообщения';
COMMENT ON COLUMN "public"."app_sms_queue"."is_sended" IS 'Отправлено';
COMMENT ON COLUMN "public"."app_sms_queue"."is_delivered" IS 'Доставлено';
COMMENT ON COLUMN "public"."app_sms_queue"."app_client_ref" IS 'Получатель';

-- ----------------------------
-- Records of app_sms_queue
-- ----------------------------

-- ----------------------------
-- Table structure for app_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_user_groups";
CREATE TABLE "public"."app_user_groups" (
"id" int8 DEFAULT nextval('app_user_groups_id_seq'::regclass) NOT NULL,
"group" varchar(255) COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"acl_user_ref" int8,
"is_refs" bool DEFAULT false
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."app_user_groups"."is_refs" IS 'Является ли группа реферальной';

-- ----------------------------
-- Records of app_user_groups
-- ----------------------------
INSERT INTO "public"."app_user_groups" VALUES ('1', 'Главная группа', '', '1', 'f');
INSERT INTO "public"."app_user_groups" VALUES ('2', 'Группа для рефералов', 'Описание группы для рефералов...', '1', 't');
INSERT INTO "public"."app_user_groups" VALUES ('3', 'asdf', 'qqwer', '2', 't');
INSERT INTO "public"."app_user_groups" VALUES ('4', 'qwer', 'qwer', '2', 'f');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."acl_users_id_seq" OWNED BY "acl_users"."id";
ALTER SEQUENCE "public"."app_additional_fields_id_seq" OWNED BY "app_additional_fields"."id";
ALTER SEQUENCE "public"."app_client_field_values_id_seq" OWNED BY "app_client_field_values"."id";
ALTER SEQUENCE "public"."app_client_has_groups_id_seq" OWNED BY "app_client_has_groups"."id";
ALTER SEQUENCE "public"."app_client_referal_counts_id_seq" OWNED BY "app_client_referal_counts"."id";
ALTER SEQUENCE "public"."app_clients_id_seq" OWNED BY "app_clients"."id";
ALTER SEQUENCE "public"."app_condition_types_id_seq" OWNED BY "app_condition_types"."id";
ALTER SEQUENCE "public"."app_conditionss_id_seq" OWNED BY "app_conditions"."id";
ALTER SEQUENCE "public"."app_contact_field_values_id_seq" OWNED BY "app_contact_field_values"."id";
ALTER SEQUENCE "public"."app_contact_fields_id_seq" OWNED BY "app_contact_fields"."id";
ALTER SEQUENCE "public"."app_field_groups_id_seq" OWNED BY "app_field_groups"."id";
ALTER SEQUENCE "public"."app_links_id_seq" OWNED BY "app_links"."id";
ALTER SEQUENCE "public"."app_promos_id_seq" OWNED BY "app_promos"."id";
ALTER SEQUENCE "public"."app_questions_id_seq" OWNED BY "app_questions"."id";
ALTER SEQUENCE "public"."app_rating_id_seq" OWNED BY "app_rating"."id";
ALTER SEQUENCE "public"."app_referals_id_seq" OWNED BY "app_referals"."id";
ALTER SEQUENCE "public"."app_referers_id_seq" OWNED BY "app_referers"."id";
ALTER SEQUENCE "public"."app_responsibles_id_seq" OWNED BY "app_responsibles"."id";
ALTER SEQUENCE "public"."app_reward_types_id_seq" OWNED BY "app_reward_types"."id";
ALTER SEQUENCE "public"."app_sms_id_seq" OWNED BY "app_sms_messages"."id";
ALTER SEQUENCE "public"."app_sms_queue_id_seq" OWNED BY "app_sms_queue"."id";
ALTER SEQUENCE "public"."app_ure_id_seq" OWNED BY "app_requisites"."id";
ALTER SEQUENCE "public"."app_user_groups_id_seq" OWNED BY "app_user_groups"."id";

-- ----------------------------
-- Primary Key structure for table acl_users
-- ----------------------------
ALTER TABLE "public"."acl_users" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_additional_fields
-- ----------------------------
ALTER TABLE "public"."app_additional_fields" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_client_field_values
-- ----------------------------
ALTER TABLE "public"."app_client_field_values" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_client_has_groups
-- ----------------------------
ALTER TABLE "public"."app_client_has_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_client_referal_counts
-- ----------------------------
ALTER TABLE "public"."app_client_referal_counts" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_clients
-- ----------------------------
ALTER TABLE "public"."app_clients" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_condition_types
-- ----------------------------
ALTER TABLE "public"."app_condition_types" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_conditions
-- ----------------------------
ALTER TABLE "public"."app_conditions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_contact_field_values
-- ----------------------------
ALTER TABLE "public"."app_contact_field_values" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_contact_fields
-- ----------------------------
ALTER TABLE "public"."app_contact_fields" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_field_groups
-- ----------------------------
ALTER TABLE "public"."app_field_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_links
-- ----------------------------
ALTER TABLE "public"."app_links" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_promos
-- ----------------------------
ALTER TABLE "public"."app_promos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table app_questions
-- ----------------------------
CREATE TRIGGER "app_questions_tg1" AFTER INSERT OR UPDATE ON "public"."app_questions"
FOR EACH ROW
EXECUTE PROCEDURE "app_rating_update"();

-- ----------------------------
-- Primary Key structure for table app_questions
-- ----------------------------
ALTER TABLE "public"."app_questions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_rating
-- ----------------------------
ALTER TABLE "public"."app_rating" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_referals
-- ----------------------------
ALTER TABLE "public"."app_referals" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_referers
-- ----------------------------
ALTER TABLE "public"."app_referers" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_requisites
-- ----------------------------
ALTER TABLE "public"."app_requisites" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_responsibles
-- ----------------------------
ALTER TABLE "public"."app_responsibles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_reward_types
-- ----------------------------
ALTER TABLE "public"."app_reward_types" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_sms_messages
-- ----------------------------
ALTER TABLE "public"."app_sms_messages" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_sms_queue
-- ----------------------------
ALTER TABLE "public"."app_sms_queue" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_user_groups
-- ----------------------------
ALTER TABLE "public"."app_user_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."app_client_field_values"
-- ----------------------------
ALTER TABLE "public"."app_client_field_values" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_client_field_values" ADD FOREIGN KEY ("app_field_ref") REFERENCES "public"."app_additional_fields" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_client_has_groups"
-- ----------------------------
ALTER TABLE "public"."app_client_has_groups" ADD FOREIGN KEY ("app_user_group_ref") REFERENCES "public"."app_user_groups" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_client_has_groups" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_client_referal_counts"
-- ----------------------------
ALTER TABLE "public"."app_client_referal_counts" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_clients"
-- ----------------------------
ALTER TABLE "public"."app_clients" ADD FOREIGN KEY ("acl_user_ref") REFERENCES "public"."acl_users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_clients" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_conditions"
-- ----------------------------
ALTER TABLE "public"."app_conditions" ADD FOREIGN KEY ("condition_type_ref") REFERENCES "public"."app_condition_types" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_conditions" ADD FOREIGN KEY ("reward_type_ref") REFERENCES "public"."app_reward_types" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_contact_field_values"
-- ----------------------------
ALTER TABLE "public"."app_contact_field_values" ADD FOREIGN KEY ("app_resp_ref") REFERENCES "public"."app_responsibles" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "public"."app_contact_field_values" ADD FOREIGN KEY ("field_ref") REFERENCES "public"."app_contact_fields" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_field_groups"
-- ----------------------------
ALTER TABLE "public"."app_field_groups" ADD FOREIGN KEY ("app_group_ref") REFERENCES "public"."app_user_groups" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_field_groups" ADD FOREIGN KEY ("app_field_ref") REFERENCES "public"."app_additional_fields" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_links"
-- ----------------------------
ALTER TABLE "public"."app_links" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_links" ADD FOREIGN KEY ("referal_condition_ref") REFERENCES "public"."app_conditions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_links" ADD FOREIGN KEY ("referer_condition_ref") REFERENCES "public"."app_conditions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_promos"
-- ----------------------------
ALTER TABLE "public"."app_promos" ADD FOREIGN KEY ("acl_user_ref") REFERENCES "public"."acl_users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_questions"
-- ----------------------------
ALTER TABLE "public"."app_questions" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_rating"
-- ----------------------------
ALTER TABLE "public"."app_rating" ADD FOREIGN KEY ("app_clients_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_referals"
-- ----------------------------
ALTER TABLE "public"."app_referals" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_referals" ADD FOREIGN KEY ("app_promo_ref") REFERENCES "public"."app_promos" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_referers"
-- ----------------------------
ALTER TABLE "public"."app_referers" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_referers" ADD FOREIGN KEY ("app_promo_ref") REFERENCES "public"."app_promos" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_requisites"
-- ----------------------------
ALTER TABLE "public"."app_requisites" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_responsibles"
-- ----------------------------
ALTER TABLE "public"."app_responsibles" ADD FOREIGN KEY ("app_client_ref") REFERENCES "public"."app_clients" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_sms_messages"
-- ----------------------------
ALTER TABLE "public"."app_sms_messages" ADD FOREIGN KEY ("app_user_group_ref") REFERENCES "public"."app_user_groups" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."app_sms_messages" ADD FOREIGN KEY ("acl_users_ref") REFERENCES "public"."acl_users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_sms_queue"
-- ----------------------------
ALTER TABLE "public"."app_sms_queue" ADD FOREIGN KEY ("app_sms_messages_ref") REFERENCES "public"."app_sms_messages" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."app_user_groups"
-- ----------------------------
ALTER TABLE "public"."app_user_groups" ADD FOREIGN KEY ("acl_user_ref") REFERENCES "public"."acl_users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
