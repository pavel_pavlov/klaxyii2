<?php

class DefaultController extends Controller
{
	public function actionIndex() {
		$this->render('index');
	}

	//список редиректов
	public function actionList() {
		$data = array();
		$oRedirects = AppRedirects::model()->findAll();

		if (empty($oRedirects)) {
			$oRedirects = array();
		}
		$redirects = array();
		foreach ($oRedirects as $k => $redirect) {
			$redirects[] = $redirect->attributes;
		}
		$data['redirects'] = $redirects;
		$data['js_redirects'] = CJSON::encode($redirects);

		$this->render('list', $data);

		exit;;
	}

	//список редиректов
	public function actionAddEdit($redirect_id = 0) {
		$data = array();
		$redirect_id = (int)$redirect_id;

		$oRedirect = AppRedirects::model()->findByPk($redirect_id);
		if (empty($oRedirect)) {
			$oRedirect = new AppRedirects();
		}
		//M::printr($oRedirect, '$oRedirect');
		$redirect = $oRedirect->attributes;
		$data['redirect'] = $redirect;

		//если это сохранение
		if (!empty($_POST)) {
			$post = $_POST;
			$JS = array('errors' => false);
			if ($redirect_id === 0) {
				//если это новый
				$oRedirect = new AppRedirects();
				$oRedirect->dt_create = new CDbExpression('now()');
			}
			$oRedirect->attributes = $post;
			$oRedirect->dt_update = new CDbExpression('now()');
			$oRedirect->is_deleted = false;
			//M::printr($oRedirect, '$oRedirect');
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if (!$oRedirect->save()) {
					//M::printr($oRedirect->getErrors(), '$oRedirect->getErrors()');
					throw new Exception(CJSON::encode($oRedirect->getErrors()));
				}
				$JS['errors'] = false;

				$transaction->commit();
			} catch (Exception $E) {
				M::printr($E, '$E');
				$JS['errors'] = true;
				$JS['error'] = $E->getMessage();

				Yii::app()->user->setFlash('error', $E->getMessage());
				$transaction->rollBack();
			}

			//вывод сообщения в ajax
			if (Yii::app()->request->isAjaxRequest) {
				print CJSON::encode($JS);
				exit;
			}
		}


		$this->render('addedit', $data);
	}

	//список редиректов
	public function actionDelete() {
		$data = array();
		$this->render('delete', $data);
	}


}