<style type="text/css">
	.hide_tab {
		display: none;
	}

	.show_tab {
		display: block;
	}

	.border {
		border: 1px solid #000000;
	}

	.border_red {
		border: 1px solid #FF0000;
	}

	.border_green {
		border: 1px solid #00FF00;
	}

	.row {
		background-color: #eeeeee;
		margin: 5px;
	}

	.dx-button-text {
		color: #ffffff;
		font-family: Arial;
		font-size: 13px;
	}

	.dx-button {
		margin: 5px 5px;
	}

	.notify {
		position: absolute;
		right: 30px;
		top: 10px;
		border-radius: 10px;
	}

	.inner-notify {
		border-radius: 10px;
		position: relative;
		width: 100%;
		height: 100%;
		padding: 10px;
		font-family: Arial, Helvetica, Courier, Times;
	}
</style>
<?php
//M::printr($redirect);
?>
<div class="form">
	<?php
	$form = new dxForm('AddEditRedirect');
	if (!empty($redirect)) {
		$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id, 'redirect_id' => $redirect['id']));
	} else {
		$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
	}
	$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));
	$form->init();
	?>

	<div class="row">
		<?php echo CHtml::label('Название', 'name'); ?>
		<?php $form->dxTextField(
			'name',
			array(
				'value' => isset($redirect['name']) ? $redirect['name'] : null
			)
		); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Комментарии', 'description'); ?>
		<?php $form->dxTextArea(
			'description',
			array(
				'height' => 200,
				'value' => isset($redirect['description']) ? $redirect['description'] : null
			)
		);
		?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Код в рекламной ссылке', 'code'); ?>
		<?php $form->dxTextField(
			'code',
			array(
				'value' => isset($redirect['code']) ? new CDbExpression("'" . $redirect['code'] . "'") : null
			)
		); ?>
	</div>

	<div class="row">
		<?php $form->dxButton(
			'gen',
			array(
				'text' => 'Сгенерировать',
				'onClick' => new CDbExpression(
					"function(){
gen_code();
				}"
				)
			)
		); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Куда перенаправлять', 'link'); ?>
		<?php $form->dxTextField(
			'link',
			array(
				'value' => isset($redirect['link']) ? $redirect['link'] : null
			)
		); ?>
	</div>

	<div class="row">
		<button class="dx-button dx-button-default" onClick="save();">
			save
		</button>
		<?php if (isset($redirect['id'])): ?>
			<button class="dx-button dx-button-success" onClick="apply();">
				apply
			</button>
		<?php endif ?>
		<button class="dx-button dx-button-danger" onClick="cancel();">
			cancel
		</button>
	</div>
</div>
<!-- form -->

<script type="text/javascript">
	var is_redirect = false;

	function gen_code() {

	}
	//сбор данных со всепй формы
	function collect_data() {
		var dataForm = {};
		for (var index in <?= $form->formName ?>) {
			dataForm[index] = <?= $form->formName ?>[index].option('value');
		}
		//console.log(dataForm);
		return dataForm;
	}

	//редирект
	function redirect(url) {
		window.location = url;
	}

	//уведомление
	function notify(header, text, type) {
		var head = $('<span>')
			.css({
				'color': '#000000',
				'font-size': '20px'
			})
			.text(header)
			.append('<br>');
		var color = '#ff0000';
		if (type == "success") {
			color = '#bbffbb';
		} else if (type == "warning") {
			color = '#ffbbbb';
		}
		$('.notify').remove();
		var noti = $('.notify-template').clone().removeClass('notify-template').addClass('notify');
		noti.find('.inner-notify').css({'background-color': color}).html(head).append(text);
		$('body').prepend(noti);
	}

	//выполнится после отправки данных
	function after_send(response) {
		console.log(response);
		if (response.errors == false) {
			notify('Save success', 'redirect to "List"', 'success');
			setTimeout(function () {
				if (is_redirect !== false) {
					redirect(redirectUrl);
				}
			}, 3000);
		} else {
			notify('Save error', 'Some errors...', 'warning');
		}
	}

	//отправка данных на сервер
	function send_data(url, data) {
		var post = data;
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data
		}).success(function (response) {
			console.log('[send_data > response]', response);
			after_send(response);
		}).error(function (data, key, value) {
			var arr = {};
			for (var index in data) {
				if (typeof data[index] !== "function") {
					arr[index] = data[index];
				}
			}

			var errorsForLog = {};
			errorsForLog.data = arr;
			errorsForLog.key = key;
			errorsForLog.value = value;
			errorsForLog.post = post;
			serial = JSON.stringify(errorsForLog);
			//saveToLog(errorsForLog);
			console.log(serial);
		});
	}

	//сохранить и закрыть (+ редирект)
	function save() {
		var data = collect_data();
		is_redirect = true;
		send_data(saveUrl, data);
	}

	//только сохранить
	function apply() {
		var data = collect_data();
		console.log('[SAVEURL]', saveUrl);
		var sendResult = send_data(saveUrl, data);
	}

	//редирект без сохранения
	function cancel() {
		notify('OK', 'ok', 'success');
		redirect(redirectUrl);
	}

</script>
