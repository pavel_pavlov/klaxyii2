<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<style type="text/css">
	#gridContainer {
		height: auto;
		width: 100%;
		margin: 50px 0;
	}

	.dx-texteditor-container {
		height: inherit;
	}

	.hide_tab {
		display: none;
	}

	a {
		color: #0000ff;
		text-decoration: underline;
	}
</style>

<div style="width: 1200px;">
	<div id="gridContainer"></div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		var clients = <?= CJSON::encode($redirects) ?>;
		//console.log('[clients]',clients);
		$("#gridContainer").dxDataGrid({
			dataSource: clients,
			paging: {
				pageSize: 20
			},
			pager: {
				showPageSizeSelector: true,
				allowedPageSizes: [5, 10, 20]
			},
			columns: [
				{
					dataField: 'id',
					caption: "ID",
					width: 50
				}, {
					caption: 'Название',
					dataField: 'name',
					cellTemplate: function (container, options) {
						$('<span/>')
							.html('<a href="<?= $this->createUrl('/redirects/default/addedit', array('redirect_id' => '')); ?>' + options.data.id + '">' + options.value + '</a>')
							.appendTo(container);
					}
				},
				{
					dataField: 'code',
					caption: 'Код в рекламной ссылке'
				},
				{
					dataField: 'link',
					caption: 'Куда перенаправлять'
				},
				{
					dataField: 'dt_create',
					caption: 'Дата создания'
				},
				{
					dataField: 'dt_update',
					caption: 'Дата изменения'
				},
			],
			masterDetail: {
				enabled: true,
				template: function (container, options) {
					container.addClass("internal-grid-container");
					//console.log(options.data.description);
					container.append($('<p>').append('http://<?php print $_SERVER['SERVER_NAME']; ?>/?rcode=' + options.data.code));
					container.append($('<p>').append(options.data.description));
					//var z = add_client(options.data);
					//var tag = $('<div>').addClass("internal-grid");
					//tag.append(z);
					//container.append(z);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	function show(obj, name) {
		var x = $(obj).closest('[id^=client_]');
		x.find('.tab').addClass('hide_tab');
		x.find('.tab_' + name).removeClass('hide_tab');
	}

	function add_client(data) {
		console.log('[CLIENT] >', data);
		var
			client = $('#template_client').clone().attr({'id': 'client_' + data.id}),
			index;

		client.find('#type').replaceWith(data.type);
		client.find('#password').replaceWith(data.password);
		client.find('#header').replaceWith(data.header);
		client.find('#manager').replaceWith(data.manager);
		client.find('#status').replaceWith(data.status);
		client.find('#groups').replaceWith(data.groups);
		client.find('#description').replaceWith(data.description);
		if (data.is_hide_in_rating === false) {
			data.is_hide_in_rating = 'Показывать';
			data.is_hide_in_rating = $('<div>').dxCheckBox({text: 'Скрыть в рейтинге', value: false, disabled: true});
		} else {
			data.is_hide_in_rating = 'Не показывать';
			data.is_hide_in_rating = $('<div>').dxCheckBox({text: 'Скрыть в рейтинге', value: true, disabled: true});
		}
		client.find('#is_hide_in_rating').replaceWith(data.is_hide_in_rating);
		client.find('#phones').replaceWith(data.phones);
		client.find('#email').replaceWith(data.email);
		client.find('#url').replaceWith(data.url);

		for (index in data.responsibles) {
			client.find("#responsibles").append(add_responsible(data.responsibles[index]));
		}

		for (index in data.requisites) {
			client.find("#requisites").append(add_requisite(data.requisites[index]));
		}
		return client;
	}

	function add_responsible(data) {
		console.log('[RESPONSIBLE] >', data);
		var
			responsible = $('#template_responsible').clone().attr({'id': 'responsible_' + data.id}),
			index;
		responsible.find('#first_name').replaceWith(data.first_name);
		responsible.find('#middle_name').replaceWith(data.middle_name);
		responsible.find('#second_name').replaceWith(data.second_name);
		responsible.find('#password').replaceWith(data.password);
		responsible.find('#position').replaceWith(data.position);
		if (data.is_responsible === false) {
			data.is_responsible = $('<div>').dxCheckBox({text: 'Ответственное лицо', value: false, disabled: true});
		} else {
			data.is_responsible = $('<div>').dxCheckBox({text: 'Ответственное лицо', value: true, disabled: true});
		}
		responsible.find('#is_responsible').replaceWith(data.is_responsible);

		for (index in data.contacts) {
			responsible.find("#contacts").append(add_contact(data.contacts[index]));
		}

		return responsible;
	}

	function add_contact(data) {
		var contact = $('#template_contact').clone().attr({'id': 'contact_' + data.id});
		console.log('[CONTACT]', data);
		contact.find('#field_label').replaceWith(data.field_label);
		contact.find('#field_value').replaceWith(data.field_value);
		if (data.field_ref === 2 || data.field_ref === 6) {
			if (data.send_allow === false) {
				data.send_allow = $('<div>').dxCheckBox({
					text: 'Принимать рассылки и уведомления',
					value: false,
					disabled: true
				});
			} else {
				data.send_allow = $('<div>').dxCheckBox({
					text: 'Принимать рассылки и уведомления',
					value: true,
					disabled: true
				});
			}
			contact.find('#send_allow').replaceWith(data.send_allow);
		}
		else {
			contact.find('#send_allow').remove();
		}
		return contact;
	}

	function add_requisite(data) {
		var requisite = $('#template_requisite').clone().attr({'id': 'requisite_' + data.id});
		console.log(requisite);
		requisite.find('#full_name').replaceWith(data.full_name);
		requisite.find('#address_juridical').replaceWith(data.address_juridical);
		requisite.find('#address_fact').replaceWith(data.address_fact);
		requisite.find('#inn').replaceWith(data.inn);
		requisite.find('#kpp').replaceWith(data.kpp);
		requisite.find('#ogrn').replaceWith(data.ogrn);
		requisite.find('#bank').replaceWith(data.bank);
		requisite.find('#bik').replaceWith(data.bik);
		requisite.find('#r_schet').replaceWith(data.r_schet);
		requisite.find('#kor_schet').replaceWith(data.kor_schet);
		return requisite;
	}
</script>

