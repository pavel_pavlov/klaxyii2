<?php

class RefsController extends Controller
{
	public $defaultAction = 'index';

	public function actionIndex()
	{

		$this->render('index');
	}

	public function actionCreate()
	{
		$formName = 'sss';
		$data['formName'] = $formName;

		//Yii::app()->getModule('groups');
		$criteria = new CDbCriteria();
		$criteria->condition = 't.acl_user_ref = ' . Yii::app()->user->id;
		$criteria->addCondition('is_refs = true', $operator = 'AND');
		$criteria->order = 't.id ASC';
		$oUsers = AppClients::model()
			->with(
				array(
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findAll($criteria);
		$users = array();
		foreach ($oUsers as $key_user => $user) {
			$users[$key_user] = $user->attributes;
		}
		//M::printr($users, '$users');
		$data['users'] = $users;
		if (!empty($_POST)) {
			$post = $_POST;
			//M::printr($post, '$post');
			$JS['errors'] = false;

			$transaction = Yii::app()->db->beginTransaction();
			try {
				$oPromo = new AppPromos();
				//Сгенерировать нужное количество промо-кодов
				if ((int)$post['numbers'] === 0) {
					$oPromo->addError('numbers', 'Неверно указано количество промо-кодов.');
					throw new Exception('Неверно указано количество промо-кодов.');
				}

				$symbols = "0123456789ABCDEF";
				for ($n = 0; $n < (int)$post['numbers']; $n++) {
					$x = 0;
					//Сгенерировать уникальный промо-код
					do {
						$promo = array();
						for ($j = 0; $j < 3; $j++) {
							$promo[$j] = "";
							for ($i = 0; $i < 6; $i++) {
								$rand = rand(0, strlen($symbols) - 1);
								$promo[$j] .= $symbols[$rand];
							}
						}
						$promo = implode('-', $promo);

						$criteria = new CDbCriteria();
						$criteria->condition = "promo = :promo";
						$criteria->params = array(':promo' => $promo);
						$oPromo = AppPromos::model()->findAll($criteria);
						$x++;
						if ($x > 1000) {
							throw new Exception('Достигнуто максимальное количество промо-кодов.');
						}
					} while (!empty($oPromo));

					//создать объект и записать его в БД
					$oPromo = new AppPromos();
					$oPromo->promo = $promo;
					$oPromo->acl_user_ref = Yii::app()->user->id;
					$oPromo->dt_create = new CDbExpression('now()');
					$oPromo->dt_update = new CDbExpression('now()');
					$oPromo->dt_start = date("Y-m-d H:i:s",strtotime($post['dt_start']));
					if ($post['no_date_finish'] == 'false') {
						$oPromo->dt_finish = date("Y-m-d H:i:s",strtotime($post['dt_finish']));
					}
					$oPromo->referrer = $post['referrer'];
					$oPromo->referrer_condition = $post['referrer_condition'];
					$oPromo->referrer_reward = $post['referrer_reward'];
					$oPromo->referrer_reward_type = $post['referrer_reward_type'];
					$oPromo->referal_condition = $post['referal_condition'];
					$oPromo->referal_reward = $post['referal_reward'];
					$oPromo->referal_reward_type = $post['referal_reward_type'];

					if (!$oPromo->save()) {
						$JS['errors'] = true;
						$JS['error'] = $oPromo->getErrors();
						throw new Exception('Not valid datas...');
					}
				} //$n = 0; $n < (int)$post['numbers']

				$JS['errors'] = false;
				$transaction->commit();
			} catch (Exception $e) {
				M::printr($JS, '$JS3');
				$transaction->rollBack();
				$JS['errors'] = true;
				$JS['error'] = $oPromo->getErrors();
				M::printr($e->getMessage(), '$e->getMessage()');
				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
			} //try

			if (Yii::app()->request->isAjaxRequest) {
				print CJSON::encode($JS);
				exit;
			}


			$date_start = $post['date_start'];
			M::printr($date_start, '$date_start');

			$x = date('d.m.Y', strtotime($date_start));
			M::printr(strtotime($date_start), 'strtotime($date_start)');

			$date_finish = $post['date_finish'];
			M::printr($date_finish, '$date_finish');

			$x = date('d.m.Y', strtotime($date_finish));
			M::printr(strtotime($date_finish), 'strtotime($date_finish)');

			exit;
		}

		$this->render('create', $data);
	}

	public function actionList()
	{
		$this->render('list');
	}

	public function actionGenerate()
	{
		$this->render('index');
	}

	public function actionRating($id = 0)
	{
		$id = (int)$id;
		$data = array();

		$rating = API::getRatingQuestionsReferals();
		$data['rating'] = $rating;

		$this->render('rating', $data);
	}


}