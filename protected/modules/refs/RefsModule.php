<?php

class RefsModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'refs.models.*',
			'refs.models.forms.*',
			'refs.models.base.*',
			'refs.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			/*
			if (Yii::app()->user->isGuest) {
				CController::redirect(CController::createUrl('/auth/login/login'));
				return false;
			}
			*/
			return true;
		}
		else
			return false;
	}
}
