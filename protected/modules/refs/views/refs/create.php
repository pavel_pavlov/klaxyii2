<?php
/* @var $this Controller */

$this->breadcrumbs = array(
	$this->module->id,
);
?>

<div class="form">
	<?php
	$form = new dxForm($formName);
	//$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
	//$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));
	?>
	<!--script type="text/javascript">
		var userData;
		var saveUrl = '<?= $form->saveUrl ?>';
		var redirectUrl = ' <?= $form->redirectUrl ?>';
	</script-->

	<?php //$form->dx ?>
	<div class="row">
		<div style="width: 600px;">
			<div class="row">
				<div>Выберите клиента, для которого будут созданы промо-коды:</div>
				<?php $form->dxSelectBox(
					'referrer',
					array(
						'items' => $users,
						'displayExpr' => 'header',
						'valueExpr' => 'id',
						'value' => isset($client['status']) ? $client['status'] : null,
						'searchEnabled' => true,
					)
				); ?>
			</div>

			<div class='row'>
				<div>Дата начала действия промо-кода:</div>
				<?php $form->dxDateBox(
					'dt_start',
					array(
						'formatString' => "dd.MM.yyyy",
						'format' => 'date',
						'fieldEditEnabled' => false,
						'applyValueMode' => 'useButtons',
						'value' => date('d.m.Y', time()),
					)
				); ?>
			</div>

			<div class='row'>
				<div>Дата окончания действия промо-кода:</div>
				<?php $form->dxDateBox(
					'dt_finish',
					array(
						'formatString' => "dd.MM.yyyy",
						'format' => 'date',
						'disabled' => true,
						'fieldEditEnabled' => false,
						'applyValueMode' => 'useButtons',
						'pickerType' => 'calendar',
						'value' => date('d.m.Y', strtotime("+1 year", time())),
					)
				); ?>
			</div>

			<div class='row'>
				<?php $form->dxCheckBox(
					'no_date_finish',
					array(
						'text' => 'время действия промо-кода не ограничено.',
						'onValueChanged' => new CDbExpression("function(data){
							{$form->formName}['date_finish'].option('disabled', data.value);
						}"),
						'value' => 'true',
					)
				); ?>
			</div>

			<div class='row'>
				<div>Количество промо-кодов:</div>
				<?php $form->dxTextBox(
					'numbers',
					array(
						'value' => '1',
					)
				); ?>
			</div>

		</div>
		<hr>
		<div style='width: 300px; float: left;'>
			<div class='row'>
				<div>Условие вознаграждения реферрера:</div>
				<?php $conditions = array(
					array(
						'id' => '1',
						'condition' => 'reg',
						'text' => 'За регистрацию'
					),
					array(
						'id' => '2',
						'condition' => 'alwayspay',
						'text' => 'С каждой покупки реферала'
					),
					array(
						'id' => '3',
						'condition' => 'onepay',
						'text' => 'С одной покупки реферала'
					),
				); ?>
				<?php $form->dxSelectBox(
					'referrer_condition',
					array(
						'items' => $conditions,
						'displayExpr' => 'text',
						'valueExpr' => 'condition',
						'value' => 'onepay',
					)
				); ?>
			</div>

			<div class='row'>
				<div>Величина вознаграждения реферрера:</div>
				<?php $form->dxTextBox(
					'referrer_reward',
					array(
						'value' => '10',
					)
				); ?>
			</div>

			<div class='row'>
				<div>Тип вознаграждения реферрера:</div>
				<?php $form->dxSelectBox(
					'referrer_reward_type',
					array(
						'items' => new CDbExpression('[{"id":1, "value": "%"},{"id":2, "value": "RUR"}]'),
						'displayExpr' => 'value',
						'valueExpr' => 'id',
						'value' => 1,
					)
				); ?>
			</div>
		</div>

		<div style='width: 300px; float: left;'>
			<div class='row'>
				<div>Условие вознаграждения реферала:</div>
				<?php $conditions = array(
					array(
						'id' => '1',
						'condition' => 'reg',
						'text' => 'За регистрацию'
					),
					array(
						'id' => '2',
						'condition' => 'allquests',
						'text' => 'на все квесты'
					),
				); ?>
				<?php $form->dxSelectBox(
					'referal_condition',
					array(
						'items' => $conditions,
						'displayExpr' => 'text',
						'valueExpr' => 'condition',
						'value' => 'allquests',
					)
				); ?>
			</div>

			<div class='row'>
				<div>Величина вознаграждения реферала:</div>
				<?php $form->dxTextBox(
					'referal_reward',
					array(
						'value' => '10',
					)
				); ?>
			</div>

			<div class='row'>
				<div>Тип вознаграждения реферала:</div>
				<?php $form->dxSelectBox(
					'referal_reward_type',
					array(
						'items' => new CDbExpression('[{"id":1, "value": "%"},{"id":2, "value": "RUR"}]'),
						'displayExpr' => 'value',
						'valueExpr' => 'id',
						'value' => 2,
					)
				); ?>
			</div>
		</div>

	</div>

	<div style='clear: both;'>
		<div class='row'>
			<?php $form->dxSubmitButton(
				'submit',
				array(
					'text' => 'Создать',
					'type' => 'success'
				),
				array(
					'url' => CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id)),
					'type' => 'POST',
					'dataType' => 'json',
				),
				false
			); ?>
		</div>
	</div>

</div>

