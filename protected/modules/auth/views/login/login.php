<?php
/* @var $this AclUsersController */
/* @var $model aclUsers */
/* @var $form CActiveForm */
?>

<div id="menu">
	<?php
	$controller = Yii::app()->controller->id;
	$this->widget('zii.widgets.CMenu',array(
		'items'=>array(
			array('label'=>'Index', 'url'=>array("{$controller}/index")),
			array('label'=>'Login', 'url'=>array("{$controller}/login")),
		),
	)); ?>
</div><!-- menu -->

<div class="form">

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'users-forms-login',
			'enableAjaxValidation' => false,
		)
	); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password'); ?>
		<?php echo $form->error($model, 'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->checkBox($model, 'rememberMe'); ?>
		<?php echo $form->labelEx($model, 'rememberMe', ['class' => 'myclass']); ?>
		<?php echo $form->error($model, 'rememberMe'); ?>
	</div>
	<br>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->