<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<div id="menu">
	<?php
	$controller = Yii::app()->controller->id;
	$this->widget('zii.widgets.CMenu',array(
		'items'=>array(
			array('label'=>'Index', 'url'=>array("{$controller}/index")),
			array('label'=>'Reg', 'url'=>array("{$controller}/reg")),
		),
	)); ?>
</div><!-- menu -->

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-forms-reg',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cpassword'); ?>
		<?php echo $form->passwordField($model,'cpassword'); ?>
		<?php echo $form->error($model,'cpassword'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->