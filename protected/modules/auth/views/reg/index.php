<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
	$this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<?php
if (Yii::app()->user->hasFlash('contact')) {
	print Yii::app()->user->getFlash('contact');
}
?>

<div id="menu">
	<?php
	$controller = Yii::app()->controller->id;
	$this->widget('zii.widgets.CMenu',array(
		'items'=>array(
			array('label'=>'Index', 'url'=>array("{$controller}/index")),
			array('label'=>'Reg', 'url'=>array("{$controller}/reg")),
		),
	)); ?>
</div><!-- menu -->

<p>
	This is the view content for action "<?php echo $this->action->id; ?>". The action belongs to the controller
	"<?php echo get_class($this); ?>" in the "<?php echo $this->module->id; ?>" module.
</p>
<p>
	You may customize this page by editing <tt><?php echo __FILE__; ?></tt>
</p>