<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<div id="menu">
	<?php
	$controller = Yii::app()->controller->id;
	$this->widget(
		'zii.widgets.CMenu', array(
			'items' => array(
				array('label' => 'Index', 'url' => array("{$controller}/index")),
				array('label' => 'List', 'url' => array("{$controller}/list")),
				array('label' => 'View', 'url' => array("{$controller}/view")),
				array('label' => 'New', 'url' => array("{$controller}/new")),
			)
		)
	); ?>
</div><!-- menu -->
<?php

?>



<div class="form">

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'app-client-groups-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// See class documentation of CActiveForm for details on this,
			// you need to use the performAjaxValidation()-method described there.
			'enableAjaxValidation' => false,
		)
	); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($mForm); ?>

	<style type="text/css">
		#demo-body .custom-item {
			padding: 0 10px;
		}

		.custom-item > img {
			height: 30px;
			width: 40px;
			float: left;
			margin-top: 2px;
		}

		.custom-item > div.product-name {
			margin-left: 40px;
			line-height: 34px;
			font-size: 14px;
		}

		#demo-body .custom-item input {
			background-color: transparent;
		}

		#demo-body .dx-popup-content .custom-item {
			padding-top: 7px;
			padding-bottom: 8px;
		}

		.dx-popup-content .custom-item > div {
			padding-left: 8px;
		}
	</style>
	<div class="row">
		<?php echo $form->labelEx($mForm, 'sms_text'); ?>
		<?php echo $form->textArea($mForm, 'sms_text', array('value' => $mForm->sms_text)); ?>
		<?php echo $form->error($mForm, 'sms_text'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($mForm, 'groups'); ?>
		<div id="productsDataSource"></div>
		<?php echo $form->hiddenField($mForm, 'groups'); ?>
		<?php echo $form->error($mForm, 'groups'); ?>
	</div>
	<script language="JavaScript">
		$("#productsDataSource").ready(function () {
			var
				values = <?= $mForm->groups ?>,
				groups = <?= $jsonGroups ?>,
				dataSource = new DevExpress.data.ArrayStore({
					data: groups,
					key: "id"
				}),
				productsWidget = $("#productsDataSource")
				.dxTagBox({
					dataSource: dataSource,
					displayExpr: "group",
					valueExpr: "id",
					values: values
				})
				.dxTagBox("instance")
				.on('valueChanged', function (data) {
					$("#<?= CHtml::activeId($mForm, 'groups'); ?>").attr('value', JSON.stringify(data.values));
				});
		});
	</script>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->

<script language="JavaScript">
	$(document).ready(function () {

	});
</script>