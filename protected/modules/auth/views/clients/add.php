<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style type="text/css">
	.hide_tab {
		display: none;
	}

	.show_tab {
		display: block;
	}

	#client_responsibles {
		border: 1px solid #000000;
	}

	.border {
		border: 1px solid #000000;
	}

	.border_red {
		border: 1px solid #FF0000;
	}

	.border_green {
		border: 1px solid #00FF00;
	}

	.row {
		background-color: #ffffad;
	}

	.dx-button-text {
		color: #ffffff;
		font-family: Arial;
		font-size: 13px;
	}
</style>

<!-- Шаблоны -->
<div style="display: none;">
	<!-- Шаблон блока Ответственных лиц -->
	<div id="template_responsible_" style="width: 600px;">
		<div class="row" style="background-color: #adffad; border: #88aa88">
			<div style="width: 200px;">
				Фамилия:
				<div id="second_name"></div>
			</div>
			<div style="width: 200px;">
				Имя:
				<div id="first_name"></div>
			</div>
			<div style="width: 200px;">
				Отчество:
				<div id="middle_name"></div>
			</div>
			<div style="width: 200px;">
				<button id="to_header" class="dx-button">В заголовок</button>
			</div>
			<div style="width: 200px;">
				Должность:
				<div id="position"></div>
			</div>
			<div style="width: 200px;">
				Пароль:
				<div id="password"></div>
			</div>
			<div style="width: 200px;">
				<div id="is_responsible"></div>
			</div>
			<div style="width: 100%;">
				<div id="contacts_">
					<!-- место для контактных данных -->
				</div>
				<!--На элементы с id="add_contact" и id="del_responsible" будут подвешены события onClick-->
				<button id="add_contact" class="dx-button dx-button-success dx-button-text">Добавить контакт</button>
				<button id="del_responsible" class="dx-button dx-button-danger dx-button-text">Удалить доверенное лицо</button>
			</div>
		</div>
	</div>

	<!-- Шаблон блока дополнительного контакта -->
	<div id="template_contact_" style="width: 400px;">
		<div class="row" style="background-color: #adffad; border: #88aa88">
			<div style="width: 200px;">
				Тип контакта:
				<div id="type"></div>
			</div>
			<div style="width: 400px;">
				Текст:
				<div id="text"></div>
			</div>
			<div style="width: 400px;">
				<div id="send_allow"></div>
			</div>
			<div style="width: 100%;">
				<!--На элемент с id="add_contact" будет подвешено событие onClick-->
				<button id="del_contact" class="dx-button dx-button-danger dx-button-text">Удалить контакт</button>
			</div>
		</div>
	</div>

	<!-- Шаблон блока реквизитов -->
	<div id="template_requisite_" style="width: 600px;">
		<div class="row">
			<div style="width: 200px;">
				<span>Полное наименование:</span>
				<div id="full_name"></div>
			</div>
			<div style="width: 200px;">
				Юридический адрес:
				<div id="address_juridical"></div>
			</div>
			<div style="width: 200px;">
				Фактический адрес:
				<div id="address_fact"></div>
			</div>
			<div style="width: 200px;">
				ИНН:
				<div id="inn"></div>
			</div>
			<div style="width: 200px;">
				КПП:
				<div id="kpp"></div>
			</div>
			<div style="width: 200px;">
				ОГРН:
				<div id="ogrn"></div>
			</div>
			<div style="width: 300px;">
				Банк:
				<div id="bank"></div>
			</div>
			<div style="width: 300px;">
				БИК:
				<div id="bik"></div>
			</div>
			<div style="width: 300px;">
				Расчетный счет:
				<div id="r_schet"></div>
			</div>
			<div style="width: 300px;">
				Корреспондентский счет:
				<div id="kor_schet"></div>
			</div>
			<div style="width: 100%;">
				<button class="dx-button dx-button-danger dx-button-text" id="del_requisite">
					X<!--На эту кнопку будет подвешено событи-->
				</button>
			</div>
		</div>
	</div>
</div>

<span><a href="javascript: show('main')">Основные свойства</a></span>
<span><a href="javascript: show('contacts')">Контактные данные</a></span>
<span><a href="javascript: show('banks')">Реквизиты</a></span>

<div class="form">
	<?php
	$form = new dxForm('AddNewClient');
	$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
	$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));
	?>

	<div class="tab tab_contacts">
		<div class="row">
			<?php echo CHtml::label('Телефоны', 'id_phones'); ?>
			<?php $form->dxTextField(
				'phones',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Email', 'id_email'); ?>
			<?php $form->dxTextField(
				'email',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('URL', 'id_url'); ?>
			<?php $form->dxTextField(
				'url',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<div class="row" id="client_responsibles">
			</div>

			<div class="row">
				<?php $form->dxButton(
					'AddResponsible',
					array(
						'text' => 'Добавить ответственное лицо',
						'type' => 'success',
						'onClick' => new Expression('function(data) {add_responsible();}')
					)
				); ?>
			</div>
		</div>
	</div>
	<!-- tab_contacts -->

	<div class="tab tab_banks hide_tab">
		<div class="row">
			<div id="client_requisites">
			</div>
			<div class="row">
				<?php $form->dxButton(
					'requisites',
					array(
						'text' => 'Добавить реквизиты',
						'type' => 'success',
						'onClick' => new Expression('function(data) {add_requisite();}')
					)
				); ?>
			</div>
		</div>
	</div>
	<!-- tab_banks -->

	<div class="tab tab_main hide_tab">
		<div class="row">
			<?php echo CHtml::label('Type of responsible:', 'id_type'); ?>
			<?php $form->dxRadioGroup(
				'type',
				array(
					'items' => array(
						array(
							'id' => 1,
							'type' => 'Type One'
						),
						array(
							'id' => 2,
							'type' => 'Type Two'
						),
					),
					'displayExpr' => 'type',
					'valueExpr' => 'id',
					'value' => 1,
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Password', 'id_password'); ?>
			<?php $form->dxTextField(
				'password',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Заголовок', 'header'); ?>
			<?php $form->dxTextField(
				'header',
				array(
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Менеджер', 'manager'); ?>
			<?php $form->dxTextField(
				'manager',
				array(
					'placeholder' => '...',
					'value' => ''
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Статус', 'status'); ?>
			<?php $form->dxSelectBox(
				'status',
				array(
					//'dataSource' => $groups,//new Expression("SelDataSrc"),
					'items' => $groups,
					'displayExpr' => 'group',
					'valueExpr' => 'id',
					'value' => 4,
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Группы', 'groups'); ?>
			<?php $form->dxTagBox(
				'groups',
				array(
					//'dataSource' => $groups,//new Expression("SelDataSrc"),
					'dataSource' => $groups,
					'displayExpr' => 'group',
					'valueExpr' => 'id',
					'values' => array()
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Комментарии', 'description'); ?>
			<?php $form->dxTextArea(
				'description',
				array(
					'value' => '',
					'height' => 200,
					//'width' => 500,
				)
			);
			?>
		</div>

	</div>
	<!-- tab_main -->
	<button class="dx-button dx-button-default" onClick="save();">
		save
	</button>
	<button class="dx-button dx-button-success" onClick="apply();">
		apply
	</button>
	<button class="dx-button dx-button-danger" onClick="cancel();">
		cancel
	</button>
</div>
<!-- form -->

<script type="text/javascript">
	var userData;
	var saveUrl = '<?php $form->saveUrl; ?>';
	var redirectUrl = ' <?php $form->redirectUrl; ?>';
	<?= $form->formName; ?>.responsible_count = 1; //номер следующего ответственного лица
	<?= $form->formName; ?>.responsibles = {}; //ответственные лица
	<?= $form->formName; ?>.requisite_count = 1; //номер следующего блока реквизитов
	<?= $form->formName; ?>.requisites = {}; //реквизиты

	function show(name) {
		$('.tab').addClass('hide_tab');
		$('.tab_' + name).removeClass('hide_tab');
	}

	//добавление ответственного лица
	function add_responsible(data, bool) {
		i = <?= $form->formName; ?>.responsible_count;
		<?= $form->formName; ?>.responsibles[i] = {};
		var responsible = <?= $form->formName; ?>.responsibles[i];
		responsible.div = {};
		responsible.dx = {};
		responsible.contact_count = 1;
		responsible.contacts = {};

		var contacts = responsible.contacts;

		var delButton = $('<input>')
			.attr({
				'type': 'button',
				'onClick': 'del_responsible(' + i + ')',
				'value': 'Удалить ' + i
			})
			.addClass('dx-button dx-button-danger');

		if (bool !== undefined) {
			delButton = null;
		}

		responsible.div.first_name = $('<div>').dxTextBox({showClearButton: true});
		responsible.dx.first_name = responsible.div.first_name.dxTextBox('instance');

		responsible.div.second_name = $('<div>').dxTextBox({showClearButton: true});
		responsible.dx.second_name = responsible.div.second_name.dxTextBox('instance');

		responsible.div.middle_name = $('<div>').dxTextBox({showClearButton: true});
		responsible.dx.middle_name = responsible.div.middle_name.dxTextBox('instance');

		responsible.div.position = $('<div>').dxTextBox({showClearButton: true});
		responsible.dx.position = responsible.div.position.dxTextBox('instance');

		responsible.div.password = $('<div>').dxTextBox({showClearButton: true, mode: 'password'});
		responsible.dx.password = responsible.div.password.dxTextBox('instance');

		responsible.div.is_responsible = $('<div>').dxCheckBox({
			text: 'Ответственное лицо',
			value: false
		});
		responsible.dx.is_responsible = responsible.div.is_responsible.dxCheckBox('instance');

		//берем шаблон и дополняем его данные
		var Tresponsible = $('#template_responsible_').clone().attr({'id': 'responsible_' + i});
		Tresponsible.find('#first_name').append(responsible.div.first_name);
		Tresponsible.find('#second_name').append(responsible.div.second_name);
		Tresponsible.find('#middle_name').append(responsible.div.middle_name);
		Tresponsible.find('#to_header').attr({'onClick': 'to_header(' + i + ')'});
		Tresponsible.find('#position').append(responsible.div.position);
		Tresponsible.find('#password').append(responsible.div.password);
		Tresponsible.find('#is_responsible').append(responsible.div.is_responsible);
		Tresponsible.find('#contacts_').attr({'id': 'contacts_' + i});
		Tresponsible.find('#add_contact').attr({'onClick': 'add_contact(' + i + ')'});
		if (bool !== undefined) {
			Tresponsible.find('#del_responsible').remove();
		}
		Tresponsible.find('#del_responsible').attr({'onClick': 'del_responsible(' + i + ')'});
		$('#client_responsibles').append(Tresponsible);

		//добавление видимого блока в DOM
		//<?= $form->formName; ?>.responsible_div.append(responsible_div);
		<?= $form->formName; ?>.responsible_count++;
	}

	//удаление ответственного лица
	function del_responsible(i) {
		delete
		<?= $form->formName; ?>.
		responsibles[i];
		$('#responsible_' + i).remove();
	}

	//добавление контактного поля в responsible[n].contacts
	function add_contact(i) {
		j = <?= $form->formName; ?>.responsibles[i].contact_count;
		<?= $form->formName; ?>.responsibles[i].contacts[j] = {};
		var contact = <?= $form->formName; ?>.responsibles[i].contacts[j];
		var contacts_div = $('#contacts_' + i);

		contact.div = {};
		contact.dx = {};

		//создать блок контакта
		//тип контакта
		contact.div.type = $('<div>').dxSelectBox({
			dataSource: <?= $jsFields ?>,
			displayExpr: 'field_label',
			valueExpr: 'id'
		});
		contact.dx.type = contact.div.type.dxSelectBox('instance');

		//текст контакта
		contact.div.text = $('<div>').dxTextBox({});
		contact.dx.text = contact.div.text.dxTextBox('instance');

		contact.div.send_allow = $('<div>').dxCheckBox({
			text: 'Получать уведомления и рассылки ',
			value: false
		});
		contact.dx.send_allow = contact.div.send_allow.dxCheckBox('instance');

		contact.div.delButton = $('<div>').append($('<input>')
			.attr({
				'type': 'button',
				'onClick': 'del_contact(' + i + ',' + j + ')',
				'value': 'X'
			})
			.addClass('dx-button dx-button-danger'));

		var Tcontact = $('#template_contact_').clone().attr({'id': 'contact_' + i + '_' + j});
		Tcontact.find('#type').append(contact.div.type);
		Tcontact.find('#text').append(contact.div.text);
		Tcontact.find('#send_allow').append(contact.div.send_allow);
		Tcontact.find('#del_contact').attr({'onClick': 'del_contact(' + i + ',' + j + ')'});
		$('#contacts_' + i).append(Tcontact);

		<?= $form->formName; ?>.responsibles[i].contact_count++;
	}

	//удаление контакта
	function del_contact(i, j) {
		delete
		<?= $form->formName; ?>.
		responsibles[i].contacts[j];
		$('#contact_' + i + '_' + j).remove();
	}

	//добавление реквизитов
	function add_requisite() {
		i = <?= $form->formName; ?>.requisite_count;
		<?= $form->formName; ?>.requisites[i] = {};
		var requisite = <?= $form->formName; ?>.requisites[i];
		requisite.div = {};
		requisite.dx = {};

		//создать поля реквизитов
		requisite.div.full_name = $('<div>').dxTextBox({});
		requisite.dx.full_name = requisite.div.full_name.dxTextBox('instance');

		requisite.div.address_juridical = $('<div>').dxTextBox({});
		requisite.dx.address_juridical = requisite.div.address_juridical.dxTextBox('instance');

		requisite.div.address_fact = $('<div>').dxTextBox({});
		requisite.dx.address_fact = requisite.div.address_fact.dxTextBox('instance');

		requisite.div.inn = $('<div>').dxTextBox({});
		requisite.dx.inn = requisite.div.inn.dxTextBox('instance');

		requisite.div.kpp = $('<div>').dxTextBox({});
		requisite.dx.kpp = requisite.div.kpp.dxTextBox('instance');

		requisite.div.ogrn = $('<div>').dxTextBox({});
		requisite.dx.ogrn = requisite.div.ogrn.dxTextBox('instance');

		requisite.div.bank = $('<div>').dxTextBox({});
		requisite.dx.bank = requisite.div.bank.dxTextBox('instance');

		requisite.div.bik = $('<div>').dxTextBox({});
		requisite.dx.bik = requisite.div.bik.dxTextBox('instance');

		requisite.div.r_schet = $('<div>').dxTextBox({});
		requisite.dx.r_schet = requisite.div.r_schet.dxTextBox('instance');

		requisite.div.kor_schet = $('<div>').dxTextBox({});
		requisite.dx.kor_schet = requisite.div.kor_schet.dxTextBox('instance');

		requisite.div.delButton = $('<div>')
			.append($('<input>')
				.attr({
					'type': 'button',
					'onClick': 'del_requisite(' + i + ')',
					'value': 'X'
				})
				.addClass('dx-button dx-button-danger'));

		var Trequisite = $('#template_requisite_').clone().attr({'id': 'requisite_' + i});
		Trequisite.find('#full_name').append(requisite.div.full_name);
		Trequisite.find('#address_juridical').append(requisite.div.address_juridical);
		Trequisite.find('#address_fact').append(requisite.div.address_fact);
		Trequisite.find('#inn').append(requisite.div.inn);
		Trequisite.find('#kpp').append(requisite.div.kpp);
		Trequisite.find('#ogrn').append(requisite.div.ogrn);
		Trequisite.find('#bank').append(requisite.div.bank);
		Trequisite.find('#bik').append(requisite.div.bik);
		Trequisite.find('#r_schet').append(requisite.div.r_schet);
		Trequisite.find('#kor_schet').append(requisite.div.kor_schet);
		Trequisite.find('#del_requisite').attr({'onClick': 'del_requisite(' + i + ')'});
		$('#client_requisites').append(Trequisite);
		<?= $form->formName; ?>.
		requisite_count++;
	}

	//удаление реквизитов
	function del_requisite(i) {
		alert(1);
		delete <?= $form->formName; ?>.requisites[i];
		$('#requisite_' + i).remove();
	}

	//заполнение заголовка
	function to_header(i) {
		var data = <?= $form->formName; ?>.responsibles[i];
		var header = data.dx.second_name.option('value') + ' ' + data.dx.first_name.option('value') + ' ' + data.dx.middle_name.option('value');
		alert(header);
		<?= $form->formName; ?>.header.option('value', header);
	}

	//сбор данных со всепй формы
	function collect_data() {
		var dataForm = {};
		console.log(<?= $form->formName ?>);
		//сбор основных данных клиента
		dataForm.type = <?= $form->formName ?>.type.option('value');
		dataForm.password = <?= $form->formName ?>.password.option('value');
		dataForm.header = <?= $form->formName ?>.header.option('value');
		dataForm.manager = <?= $form->formName ?>.manager.option('value');
		dataForm.status = <?= $form->formName ?>.status.option('value');
		dataForm.groups = <?= $form->formName ?>.groups.option('values');
		dataForm.description = <?= $form->formName ?>.description.option('value');
		dataForm.phones = <?= $form->formName ?>.phones.option('value');
		dataForm.email = <?= $form->formName ?>.email.option('value');
		dataForm.url = <?= $form->formName ?>.url.option('value');
		console.log('[dataForm]',dataForm);
		//сбор ответственных лиц
		var responsibles = <?= $form->formName ?>.responsibles;
		dataForm.responsibles = {};
		for(var resp in responsibles) {
			dataForm.responsibles[resp] = {};
			dataForm.responsibles[resp].first_name = responsibles[resp].dx.first_name.option('value');
			dataForm.responsibles[resp].second_name = responsibles[resp].dx.second_name.option('value');
			dataForm.responsibles[resp].middle_name = responsibles[resp].dx.middle_name.option('value');
			dataForm.responsibles[resp].position = responsibles[resp].dx.position.option('value');
			dataForm.responsibles[resp].password = responsibles[resp].dx.password.option('value');
			dataForm.responsibles[resp].is_responsible = responsibles[resp].dx.is_responsible.option('value');

			//сбор контактов
			var contacts = responsibles[resp].contacts;
			dataForm.responsibles[resp].contacts = {};
			for (var contact in contacts) {
				dataForm.responsibles[resp].contacts[contact] = {};
				dataForm.responsibles[resp].contacts[contact].type = contacts[contact].dx.type.option('value');
				dataForm.responsibles[resp].contacts[contact].text = contacts[contact].dx.text.option('value');
				dataForm.responsibles[resp].contacts[contact].send_allow = contacts[contact].dx.send_allow.option('value');
			}
		}

		//сбор реквизитов
		var requisites = <?= $form->formName ?>.requisites;
		dataForm.requisites = {};
		for(var req in requisites) {
			dataForm.requisites[req] = {};
			dataForm.requisites[req].full_name = requisites[req].dx.full_name.option('value');
			dataForm.requisites[req].address_juridical = requisites[req].dx.address_juridical.option('value');
			dataForm.requisites[req].address_fact = requisites[req].dx.address_fact.option('value');
			dataForm.requisites[req].inn = requisites[req].dx.inn.option('value');
			dataForm.requisites[req].kpp = requisites[req].dx.kpp.option('value');
			dataForm.requisites[req].ogrn = requisites[req].dx.ogrn.option('value');
			dataForm.requisites[req].bank = requisites[req].dx.bank.option('value');
			dataForm.requisites[req].bik = requisites[req].dx.bik.option('value');
			dataForm.requisites[req].r_schet = requisites[req].dx.r_schet.option('value');
			dataForm.requisites[req].kor_schet = requisites[req].dx.kor_schet.option('value');
		}
		return dataForm;
	}

	//редирект
	function redirect(url) {
		window.location = '<?php $form->redirectUrl ?>';
	}

	//отправка данных на сервер
	function send_data(url, data) {
		$.ajax({
			url: '<?= CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id)) ?>',
			type: 'POST',
			dataType: 'json',
			data: data
		}).success(function (response) {
			console.log(response);
			return true;
		}).error(function (data, key, value) {
			//alert('error: ' + data + ' : ' + key + ' : ' + value);
			return false;
		});
	}

	//сохранить и закрыть (+ редирект)
	function save() {
		var data = collect_data();
		var send_url = '<?= CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id)) ?>';
		console.log(send_url);
		var send_result = send_data(send_url, data);
		if (send_result == true) {

		}
	}

	//только сохранить
	function apply() {

	}

	//редирект без сохранения
	function cancel() {

	}

	add_responsible(userData, true);
</script>
