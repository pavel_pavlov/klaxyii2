<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<style type="text/css">
	#gridContainer {
		height: auto;
		width: 100%;
		margin: 50px 0;
	}

	.dx-texteditor-container {
		height: inherit;
	}
</style>

<div id="gridContainer"></div>
<script type="text/javascript">
	$(document).ready(function () {
		var clients = <?= CJSON::encode($clients) ?>;
		//console.log('[clients]',clients);
		$("#gridContainer").dxDataGrid({
			dataSource: clients,
			paging: {
				pageSize: 20
			},
			pager: {
				showPageSizeSelector: true,
				allowedPageSizes: [5, 10, 20]
			},
			columns: [
				{
					dataField: 'id',
					caption: "ID",
					width: 50
				}, {
					caption: 'Name',
					dataField: 'name',
					cellTemplate: function (container, options) {
						$('<span/>')
							.html('<a href="<?= $this->createUrl('/auth/clients/view', array('client_id' => '')); ?>' + options.data.id + '">' + options.value + '</a>')
							.appendTo(container);
					}
				},
				'email', 'phone', 'description',
				{
					dataField: 'dt_create',
					caption: 'Date create'
				},
				{
					dataField: 'groups',
					caption: 'Groups',
					cellTemplate: function (container, options) {
						$('<span/>')
							.html(function () {
								//console.log('[OPTIONS]',options);
								var str = [];
								for (var index in options.data.groups) {
									var x = options.data.groups[index];
									//console.log('[X]', x.group);
									str[index] = x.group;
								}
								return '<a href="<?= $this->createUrl('/auth/groups/list'); ?>">'+str.join(', ')+'</a>';
							})
							.appendTo(container);
						//options.data.id + '">' + options.value + '</a>')
						//.appendTo(container);
					}
				}
			]
		});
		console.log('');
	});
</script>

<!--div id="gridContainerGroup"></div>
<script type="text/javascript">
	$(document).ready(function () {
		var clients_groups = ;

		var dataGrid = $("#gridContainerGroup").dxDataGrid({
			dataSource: clients_groups,
			allowColumnReordering: true,
			grouping: {
				autoExpandAll: true
			},
			searchPanel: {
				visible: true
			},
			paging: {
				pageSize: 20
			},
			groupPanel: {
				visible: true
			},
			columns: [
				{
					dataField: 'id',
					caption: 'ID',
					width: 50
				},
				'name',
				'email',
				'phone',
				'description',
				'dt_create',
				{
					dataField: 'group_name',
					groupIndex: 0
				}
			]
		}).dxDataGrid("instance");

		$("#autoExpand").dxCheckBox({
			value: true,
			onValueChanged: function (data) {
				dataGrid.option('grouping.autoExpandAll', data.value);
			}
		});
	});
</script-->
