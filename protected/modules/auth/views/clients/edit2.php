<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<div class="form">
	<?php $form = new dxForm('xxx'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //Yii::app()->user->getFlash('error'); ?>
	<?php //echo $form->errorSummary($model); ?>
	<?php $form->dxHiddenField('client_id', $client->id); ?>
	<div class="row">
		<?php echo CHtml::label('Name', "{$form->formName}_name"); ?>
		<?php $form->dxTextField(
			'name',
			array(
				'height' => '22',
				//'mode' => 'password',
				'placeholder' => 'Enter full name here...',
				'value' => $client->name
			)
		); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Password mode', "{$form->formName}_password"); ?>
		<?php $form->dxTextField(
			'password',
			array(
				'height' => '22',
				'placeholder' => 'Enter full name here...',
				'value' => $client->password
			)
		); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Description of client', "{$form->formName}_description"); ?>
		<?php $form->dxTextArea(
			'description',
			array(
				'value' => $client->description,
				'height' => 200,
				//'width' => 500,
			)
		);
		?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Groups', "{$form->formName}_groups"); ?>
		<?php $form->dxTagBox(
			'groups',
			array(
				'dataSource' => $groups,
				'values' => $clientGroups,
				'displayExpr' => 'group',
				'valueExpr' => 'id'
			)
		);
		?>
	</div>

	<!--div class="row">
		<?php $form->dxSelectBox(
		'selectGroups',
		array(
			'dataSource' => $groups,//new Expression("SelDataSrc"),
			'displayExpr' => 'group',
			'valueExpr' => 'id',
			'value' => 4,
		)
	); ?>
	</div-->

	<!--div class="row">
		<?php $form->dxRadioGroup(
		'radioGroups',
		array(
			'dataSource' => $groups,//new Expression("SelDataSrc"),
			//'items' => $groups,
			'displayExpr' => 'group',
			'valueExpr' => 'id',
			'value' => 4,
		)
	); ?>
	</div-->

	<!--div class="row">
		<?php $form->dxCheckBox(
		'checkGroups',
		array(
			'text' => 'myText',
			'value' => true
		)
	); ?>
	</div-->

	<!--div class="row">
		<?php $form->dxSwitch(
		'switchGroups',
		array(
			'text' => 'myText',
			'value' => false
		)
	); ?>
	</div-->

	<!--div class="row">
		<?php if (isset($client)) {
		M::printr($client, '$client');
		echo CHtml::hiddenField($form->formName . 'client_id', $client->id);
	} ?>
	</div-->

	<div class="row">
		<div id="AddClientField"></div>
		<?php $form->dxButton(
			'MyButtonNew',
			array('text' => 'Add new contact', 'type' => 'default')
		); ?>
	</div>

	<div class="row">
		<div id="ClientFields"></div>
	</div>
	<script type="text/javascript">
		var additionalFields = {};
		additionalFields['<?= $form->formName ?>'] = {};
		var fields = <?= $jsFields ?>;
		var fieldValues = <?= $jsFieldValues; ?>;
		var AddClientField = $('#AddClientField').dxSelectBox({
			dataSource: fields,
			displayExpr: 'field_label',
			valueExpr: 'id'
		}).dxSelectBox('instance');
	</script>

	<div class="row">
		<?php $form->dxButton(
			'MyButtonNew',
			array('text' => 'Add new contact')
		); ?>
		<script type="text/javascript">
			var i = 1;
			var tag;
			var newFieldData = {};
			var dataBlock = {};
			dataBlock.dx = {};
			newFieldData.dx = {};


			function delBlock(i) {
				delete additionalFields['<?= $form->formName ?>'][i];
				//console.log(additionalFields);
				$('#ClientBlock_' + i).remove();
			}

			//Проверить, есть ли уже какие-то поля
			if (fieldValues) {
				for (var f in fieldValues) {
					dataBlock = createBlock(fields, fieldValues[f]);
					var insertField = $('<div>')
						.attr('id', 'ClientBlock_' + i)
						.css({'border': '1px solid #f00', 'border-radius': '3px'})
						.addClass('AdditionalClientField');
					//добавление визуальных частей
					insertField.append(dataBlock.label);
					insertField.append(dataBlock.textBox);
					if (dataBlock.send_allow == true) {
						insertField.append(dataBlock.checkBox);
					}
					insertField.append(dataBlock.delButton);
					//вывод блока в браузер
					$('#ClientFields').append(insertField);
					additionalFields['<?= $form->formName ?>'][i] = dataBlock;
					i++;
				}
			}

			$('#MyButtonNew').off('click').on('click', function (e) {
				e.preventDefault();
				//берем номер поля и его данные
				var selectedValue = AddClientField.option('value');
				console.log('selectedValue: ', selectedValue);
				if (selectedValue == null) {
					return;
				}
				dataBlock = createBlock(fields, selectedValue);
				console.log('dataBlock: ', dataBlock);
				insertField = $('<div>')
					.attr('id', 'ClientBlock_' + i)
					.css({'border': '1px solid #f00', 'border-radius': '3px'})
					.addClass('AdditionalClientField');
				//добавление визуальных частей
				//метка
				insertField.append(dataBlock.label);

				//текстовое поле
				insertField.append(dataBlock.textBox);

				//чекбокс
				if (dataBlock.send_allow == true) {
					insertField.append(dataBlock.checkBox);
				}
				//кнопка удаления
				insertField.append(dataBlock.delButton);

				//вывод блока в браузер
				$('#ClientFields').append(insertField);
				additionalFields['<?= $form->formName ?>'][i] = dataBlock;
				i++;
			});
		</script>
	</div>

	<div class="row">
		<?php $form->dxButton(
			'SaveContacts',
			array(
				'text' => 'Save contacts',
				'type' => 'success'
			)
		); ?>

		<script type="text/javascript">
			$('#SaveContacts').off('click').on('click', function () {
				//console.log('<?= $form->formName?>: ', <?= $form->formName?>);
				var i = 0;
				var dataForm = {};
				//собрать данные с основной формы
				for (i in <?= $form->formName?>) {
					if (typeof(<?= $form->formName?>[i]) !== 'object') {
						dataForm[i] = <?= $form->formName?>[i];
						//console.log('TypeOf (' + i + '): ', typeof(<?= $form->formName?>[i]));
						continue;
					}
					if (<?= $form->formName?>[i].option('values') !== undefined) {
						dataForm[i] = <?= $form->formName?>[i].option('values');
						continue;
					}
					if (<?= $form->formName?>[i].option('value') !== undefined) {
						dataForm[i] = <?= $form->formName?>[i].option('value');
						continue;
					}
					//dataForm['<?= $form->formName?>'][i] = <?= $form->formName?>[i];
				}
				//собрать данные с контактных полей
				var x = $('#ClientFields').children('div');
				dataForm['additionalFields'] = {};
				for (i in additionalFields['<?= $form->formName ?>']) {
					var block = {};
					block.clientFieldRef = additionalFields['<?= $form->formName ?>'][i]['clientFieldRef'];
					block.field_ref = additionalFields['<?= $form->formName ?>'][i]['field_ref'];
					block.field_type = additionalFields['<?= $form->formName ?>'][i]['field_type'];
					block.is_checked = additionalFields['<?= $form->formName ?>'][i].dx.checkBox.option('value');
					block.field_value = additionalFields['<?= $form->formName ?>'][i].dx.textBox.option('value');
					//if (block.sendAllow === true) {
					//	block.checkbox = additionalFields['<?= $form->formName ?>'][i].checkBox.option('value');
					//}
					dataForm['additionalFields'][i] = block;
				}
				//console.log('dataForm: ', dataForm);
				//отправить данные
				$.ajax({
					url: '<?= CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id)) ?>',
					type: 'POST',
					dataType: 'html',
					data: dataForm
				}).success(function (response) {
					console.log(response);
				}).error(function (data, key, value) {
					alert('error: ' + data + ' : ' + key + ' : ' + value);
				});
			});
		</script>
	</div>

	<div class="row">
		<?php
		//M::printr($fieldValues, '$fieldValues');
		?>
	</div>

</div><!-- form -->