<?php

/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 22.07.2015
 * Time: 14:46
 */
class Sms// extends CActiveRecord
{
	private $id = 16831;
	private $key = "860778A38F5A6B1A";
	private $from = "NAYS";
	public $errors = array();


	public function addError($attribute, $text) {
		$this->errors[$attribute] = $text;
	}

	public function send_many($to_phones, $sms_text = '', $from = false) {
		if (!is_array($to_phones)) {
			$this->addError('to_phones', '$to_phones must be array!');
			return false;
		}

		if (empty($sms_text)) {
			$this->addError('sms_text', '$sms_text must be not empty!!');
			return false;
		}

		if (!$from) {
			$from = $this->from;
		}
		$sms_text = urlencode($sms_text);
		$sms_text = str_replace("\n", '%0D', $sms_text);
		foreach ($to_phones as $phone) {
			$to[] = array(
				'to' => $phone,
				'from' => $from,
				'text' => $sms_text
			);
		}

		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type: application/json',
				'content' => json_encode($to)
				/*
				'
				[
					{"to":"79161234567", "from":"SMS-INFO", "text":"Hello world 1!"},
					{"to":"7916765321", "from":"SMS-INFO", "text":"Hello world 2!"}
				],
				'
				*/
			)
		);
		M::printr($options, '$options');
		$context = stream_context_create($options);
		$result = file_get_contents("http://bytehand.com:3800/send_multi?id={$this->id}&key={$this->key}", false, $context);
		var_dump($result);
		M::printr($result, '$result');
		return $result;
	}

	public function send_one($to_phone = '', $sms_text = '', $from = false) {
		$bytehandId = 0000;
		$bytehandKey = "XXXXXXXXXXXXXXXX";
		$bytehandFrom = "VILIOT";

		function sendSMS($to, $text) {
			global $bytehandId;
			global $bytehandKey;
			global $bytehandFrom;

			$result = @file_get_contents('http://bytehand.com:3800/send?id=' . $bytehandId . '&key=' . $bytehandKey . '&to=' . urlencode($to) . '&from=' . urlencode($bytehandFrom) . '&text=' . urlencode($text));
			if ($result === false)
				return false;
			else
				return true;
		}

		sendSMS("79161234567", "Test");
	}

	public function validate($phone = false) {
		return true;
	}
}