<?php

/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 27.07.2015
 * Time: 14:00
 */

/**
 * Class Expression
 * Класс, который выдает выражение, а не значение
 * Используется в классе dxForm для передачи имени переменной в качестве строки,
 * которая выведется без кавычек, в отличие от строки, которая будет экранирована кавычками.
 */
class Expression
{
	private $expression;

	function __construct($expression) {
		$this->expression = $expression;

	}

	function  __toString() {
		return $this->expression;
	}
}