<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_PAIR_USERNAME_PASSWORD_INVALID=200;

	private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function __construct($email, $password) {
		$this->username = $email;
		$this->password = $password;
	}

	public function authenticate() {
		$oUsers = new AclUsers();
		$user = $oUsers->findByAttributes(array('email' => $this->username, 'password' => $this->password));
		if (is_null($user)) {
			$this->errorCode = self::ERROR_PAIR_USERNAME_PASSWORD_INVALID;
		} else {
			$this->_id = $user->id;
			//добавляем данные о пользователе в сессию
			Yii::app()->session['id']=$user->id;
			Yii::app()->session['email']=$user->email;
			Yii::app()->session['dt_create']=$user->dt_create;
			Yii::app()->session['dt_update']=$user->dt_update;
			Yii::app()->session['is_active']=$user->is_active;
			Yii::app()->session['is_deleted']=$user->is_deleted;

			/*$this->setState('id', $user->id);
			$this->setState('email', $user->email);
			$this->setState('dt_create', $user->dt_create);
			$this->setState('dt_update', $user->dt_update);
			$this->setState('is_active', $user->is_active);
			$this->setState('is_deleted', $user->is_deleted);*/

			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	/**
	 * Returns the unique identifier for the identity.
	 * The default implementation simply returns {@link username}.
	 * This method is required by {@link IUserIdentity}.
	 * @return string the unique identifier for the identity.
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * Returns the display name for the identity.
	 * The default implementation simply returns {@link username}.
	 * This method is required by {@link IUserIdentity}.
	 * @return string the display name for the identity.
	 */
	public function getName() {
		return $this->username;
	}


}