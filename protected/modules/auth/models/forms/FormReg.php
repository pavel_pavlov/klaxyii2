<?php

/**
 * RegForm class.
 * RegForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class FormReg extends CFormModel
{
	public $email;
	public $password;
	public $cpassword;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('email, password, cpassword', 'required'),
			array('email', 'email'),
			array('email', 'unique', 'className' => 'AclUsers'),
			array('password', 'confirm'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'email' => 'EMail',
			'password' => 'Password',
			'cpassword' => 'Confirm password',
		);
	}

	/**
	 * Check password and confirm password..
	 */
	public function confirm($attribute, $params) {
		if ($this->password != $this->cpassword) {
			$this->addError('password', 'Wrong confirm password.');
		}
	}

}
