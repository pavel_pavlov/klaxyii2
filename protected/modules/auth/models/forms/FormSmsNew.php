<?php

/**
 * RegForm class.
 * RegForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class FormSmsNew extends CFormModel
{
	/**
	 * AppSmsMessages
	 * @property string $id
	 * @property string $sms_text
	 * @property string $dt_create
	 * @property string $acl_users_ref
	 * @property string $app_client_group_ref
	 *
	 *
	 * AppSmsQueues
	 * @property string $id
	 * @property string $app_sms_messages_ref
	 * @property string $app_client_ref
	 * @property boolean $is_sended
	 * @property boolean $is_delivered
	 * @property string $dt_sended
	 * @property string $dt_delivered
	 * @property string $message_id
	 * @property integer $status
	 *
	 */

	//FormFields
	public $sms_text;
	public $groups = '[]';

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('sms_text', 'required'),
			array('sms_text', 'length', 'max' => 800),
			array(
				'groups',
				'is_json',
				array(
					'string' => 'must be string.',
					'json' => ' must be json.',
					'count' => ' must be count > 0.'
				)
			),
			/*
			array('name, password, groups', 'required'),
			array('email', 'unique', 'className' => 'AppClients'),
			array('name, email, phone', 'length', 'max'=>255),
			array('password', 'length', 'max' => 255),
			array('description, is_deleted, is_active, groups', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phone, description, dt_create, dt_update, is_deleted, is_active, acl_user_ref, groups', 'safe', 'on'=>'search')
			*/
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'sms_text' => 'SMS Text',
			'groups' => 'Groups',
		);
	}

	//проверка параметра на json и количество элементов > 0
	public function is_json($par1, $par2) {
		if (!is_string($this->$par1)) {
			return $this->addError($par1, ucfirst($par1) . $par2[0]['string']);
		}

		$res = json_decode($this->$par1);
		if (!(is_object($res) || is_array($res))) {
			return $this->addError($par1, ucfirst($par1) . $par2[0]['json']);
		}

		if (!(count($res) > 0)) {
			return $this->addError($par1, ucfirst($par1) . $par2[0]['count']);
		}
		return true;
	}
}
