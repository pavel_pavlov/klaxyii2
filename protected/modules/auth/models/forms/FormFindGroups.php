<?php

/**
 * RegForm class.
 * RegForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class FormFindGroups extends CFormModel
{
	//FormFields
	public $sms_text;
	public $acl_users_ref;
	public $app_client_group_ref;
	public $app_sms_messages_ref;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
/*
			array('name, password, groups', 'required'),
			array('email', 'unique', 'className' => 'AppClients'),
			array('name, email, phone', 'length', 'max'=>255),
			array('password', 'length', 'max' => 255),
			array('description, is_deleted, is_active, groups', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phone, description, dt_create, dt_update, is_deleted, is_active, acl_user_ref, groups', 'safe', 'on'=>'search')
*/
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'password' => 'Password',
			'email' => 'Email',
			'phone' => 'Phone',
			'description' => 'Description',
			'dt_create' => 'Date create',
		);
	}



}
