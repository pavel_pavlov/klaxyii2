<?php

class AuthModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'auth.models.*',
			'auth.models.forms.*',
			'auth.models.base.*',
			'auth.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$cs = Yii::app()->getClientScript();
			//$cs->registerCssFile($baseUrl.'/dx.light.compact.css');
			$cs->registerScriptFile('/js/jquery-2.1.3.js', CClientScript::POS_HEAD);
			$cs->registerScriptFile('/js/functions.js', CClientScript::POS_HEAD);
			//$cs->registerScriptFile('/js/notify.min.js', CClientScript::POS_HEAD);

			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
