<?php

class ClientsController extends Controller
{
	public $defaultAction = 'List';

	public function filters()
	{
		//return array( 'accessControl' );
	}

	public function filterAccessControl()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'actions' => array('index'),
				'users' => array('@')
			),
			array('deny',
				'users' => array('*')
			),
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAdd()
	{
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}

		$data = array();
		$model = new FormAddClient();

		//взять группы пользователя
		$oClientGroups = new AppClientGroups();
		$groups = $oClientGroups->findAllByAttributes(
			array('acl_user_ref' => (int)Yii::app()->user->id),
			array('order' => 'id ASC')
		);
		$jsGroups = CJSON::encode($groups);
		$data['groups'] = $groups;
		$data['jsGroups'] = $jsGroups;

		//взять типы доп. полей
		$oFields = new AppClientFields();
		$fields = $oFields->findAll(array('order' => 'field_label ASC'));
		$data['fields'] = $fields;
		$data['jsFields'] = CJSON::encode($fields);

		//M::printr($groups, '$groups');
		//M::printr($jsGroups, '$jsGroups');

		//проверяем, не передавалось ли что-нибудь уже...
		if (!empty($_POST)) {
			//M::printr($_POST, '$_POST');
			$post = $_POST;
			M::printr($post, '$post');
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$oClient = new AppClients();
				$oClient->type = $post['type'];
				$oClient->password = $post['password'];
				$oClient->header = $post['header'];
				$oClient->manager = $post['manager'];
				$oClient->status = $post['status'];
				$oClient->description = $post['description'];
				$oClient->phones = $post['phones'];
				$oClient->email = $post['email'];
				$oClient->url = $post['url'];
				$oClient->acl_user_ref = Yii::app()->user->id;
				$oClient->dt_create = new CDbExpression('now()');
				$oClient->dt_update = new CDbExpression('now()');
				if (!$oClient->save()) {
					throw new Exception('Exception: error save data in table AppClients');
				}
				M::printr($oClient->id, '$oClient->id');

				//сохраняем ответственных лиц
				foreach ($post['responsibles'] as $key_resp => $responsible) {
					$oResps = new AppResponsibles();
					$oResps->first_name = $responsible['first_name'];
					$oResps->second_name = $responsible['second_name'];
					$oResps->middle_name = $responsible['middle_name'];
					$oResps->position = $responsible['position'];
					$oResps->password = $responsible['password'];
					$oResps->is_responsible = $responsible['is_responsible'] == 'true' ? true : false;
					$oResps->app_client_ref = $oClient->id;
					if (!$oResps->save()) {
						throw new Exception('Exception: error save data in table AppResponsibles');
					}
					M::printr($oResps->id, '$oResps->id');
					M::printr('DONE $oResponsibles', 'DONE $oResponsibles');

					//сохранение контактных данных
					//если контактные данные не указаны, то идем дальше
					if (!isset($responsible['contacts'])) {
						//если контактные данные не указаны, то идем дальше
						continue;
					}
					foreach ($responsible['contacts'] as $kek_contact => $contact) {
						$oContactFieldValues = new AppContactFieldValues();
						$oContactFieldValues->field_ref = $contact['type'];
						$oContactFieldValues->field_value = $contact['text'];
						$oContactFieldValues->send_allow = ($contact['type'] == 2 || $contact['type'] == 6) ? $contact['send_allow'] : false;
						$oContactFieldValues->app_resp_ref = $oResps->id;
						if (!$oContactFieldValues->save()) {
							throw new Exception('Exception: error save data in table AppContactFieldValues');
						}

					} //each $responsible['contacts']

				} //each $post['responsibles']
				if (isset($post['requisites'])) {
					foreach ($post['requisites'] as $key_requisites => $requisite) {
						//$oRequisites = new
					} //each $post['requisites']
				}
				M::printr('DONE $oClient', 'DONE $oClient');

				$transaction->commit();
			} catch (Exception $e) {
				M::printr($e->getMessage(), 'EXCEPTION!!!');
				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				$transaction->rollBack();
			} //try
		} //if (!empty($_POST))

		$data['model'] = $model;
		$this->render('add', $data);
	}

	public function actionList()
	{
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('login/login'));
		}

		$data = array();
		$oClients = new AppClients();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$clients_obj = $oClients
			->with(
				array(
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findAll($criteria);
		//$clients_obj = $oClients->findAll($criteria);
		//M::printr($clients_obj, '$clients_obj');

		$clients = array();
		foreach ($clients_obj as $key_client => $client_obj) {
			//M::printr($client_obj, '$client_obj');
			$clients[$key_client] = $client_obj->attributes;
			$groups_obj = $client_obj->appClientHasGroups;
			$clients[$key_client]['groups'] = array();
			$arrGroup = array();
			foreach ($groups_obj as $key_group => $group_obj) {
				//M::printr($group_obj, '$group_obj');
				$group = $group_obj->attributes;
				$clients[$key_client]['groups'][$key_group] = $group;
				$userGroup = $group_obj->appUserGroupRef;
				//M::printr($userGroup, '$userGroup');
				$clients[$key_client]['groups'][$key_group]['group'] = $userGroup->group;
			}
			//$clients[$key_client]['str_groups'] = implode(',', $clients[$key_client]['str_groups']);
		}
		//M::printr($clients, '$clients');
		//M::printr($groups, '$groups');
		$data['clients'] = $clients;

		$this->render('list', $data);
	}

	public function actionEdit($client_id = 0)
	{
		$data = array();
		$formName = 'sss';
		$data['formName'] = $formName;
		if (isset($_POST['client_id'])) {
			$client_id = $_POST['client_id'];
		}

		//взять группы пользователя
		$oClientGroups = new AppUserGroups();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = $oClientGroups->findAll($criteria);
		$arr = array();
		foreach ($groups as $key_groups => $group) {
			$arr[$key_groups] = $group->attributes;
		}
		$data['user_groups'] = $arr;

		//берем все данные о клиенте
		$oClients = new AppClients();
		$clients = $oClients
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
					'appRequisites',
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findByPk($client_id);

		//берем официальные лица в массив
		$responsibles = array();
		foreach ($clients->appResponsibles as $key_responsible => $responsible) {
			$responsibles[$key_responsible] = $responsible->attributes;
			//берем контакты официальных лиц
			$responsibles[$key_responsible]['contacts'] = array();
			foreach ($responsible->appContactFieldValues as $key_contact => $contact) {
				$responsibles[$key_responsible]['contacts'][$key_contact] = $contact->attributes;
			}
		}

		//берем реквизиты
		$requisites = array();
		foreach ($clients->appRequisites as $key_requisite => $requisite) {
			$requisites[$key_requisite] = $requisite->attributes;
		}

		//берем группы клиента
		$client_groups = array();
		foreach ($clients->appClientHasGroups as $key_client_group => $client_group) {
			$client_groups[$key_client_group] = $client_group->app_user_group_ref;
		}
		//берем данные клиента в массив
		$client = $clients->attributes;
		$data['client'] = $client;
		$data['responsibles'] = $responsibles;
		$data['requisites'] = $requisites;
		$data['client_groups'] = $client_groups;

		//взять группы клиента
		$oClientGroups = new AppClientHasGroups();
		$clientGroups = $oClientGroups->findAllByAttributes(
			array('app_client_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);

		//взять типы доп. полей
		$oFields = new AppContactFields();
		$fields = $oFields->findAll(array('order' => 'field_label ASC'));
		$data['fields'] = $fields;
		$data['jsFields'] = CJSON::encode($fields);

		//взять доп. поля клиента
		$oFieldValues = new AppContactFieldValues();
		$fieldValues = $oFieldValues->findAllByAttributes(
			array('app_resp_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);
		$data['fieldValues'] = $fieldValues;
		$data['jsFieldValues'] = CJSON::encode($fieldValues);

		//если сохранение данных
		if (!empty($_POST)) {
			//M::printr($_POST, '$_POST');
			$post = $_POST;
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$oClient = AppClients::model()->findByPk((int)$client_id);
				if (empty($oClient)) {
					throw new Exception('Exception: index `id` = ' . (int)$client_id . ' not found in table AppClients');
				}
				$oClient->attributes = $post;
				$oClient->dt_update = new CDbExpression('NOW()');
				if (!$oClient->save()) {
					//M::printr($oCli);
					throw new Exception('Exception: error save data in table AppClients');
				}

				//сохраняем ответственных лиц
				if (isset($post['responsibles'])) {
					$id_responsibles = array();
					foreach ($post['responsibles'] as $key_resp => $responsible) {
						//проверяем, есть ли такой id в базе
						if ((int)$responsible['id'] !== 0) {
							//если id != 0, значит есть, взять его из базы
							$oResponsible = AppResponsibles::model()->findByPk((int)$responsible['id']);
							if (empty($oResponsible)) {
								throw new Exception('Exception: index `id` = ' . (int)$responsible['id'] . ' not found in table AppResponsibles');
							}
						} else {
							//если id = 0, значит новый, создать объект
							$oResponsible = new AppResponsibles();
						}
						$oResponsible->attributes = $responsible;
						$oResponsible->is_responsible = $responsible['is_responsible'] == 'true' ? true : false;
						$oResponsible->app_client_ref = $oClient->id;
						if (!$oResponsible->save()) {
							throw new Exception('Exception: error save data in table AppResponsibles');
						}
						M::printr($oResponsible, '$oResponsible');
						//сохранение контактных данных
						if (isset($responsible['contacts'])) {
							$id_contacts = array();
							foreach ($responsible['contacts'] as $key_contact => $contact) {
								if ((int)$contact['id'] !== 0) {
									$oContact = AppContactFieldValues::model()->findByPk((int)$contact['id']);
									if (empty($oContact)) {
										throw new Exception('Exception: index `id` = ' . (int)$contact['id'] . ' not found in table AppContactFieldValues');
									}
								} else {
									$oContact = new AppContactFieldValues();
								}
								$oContact->attributes = $contact;
								$oContact->send_allow = !in_array($contact['field_ref'], array(2, 6)) ? $contact['send_allow'] : false;
								$oContact->app_resp_ref = $oResponsible->id;
								if (!$oContact->save()) {
									throw new Exception('Exception: error save data in table AppContactFieldValues');
								}
								//получить список тех, которые будут сохранены (отличных от нуля)
								M::printr($oContact->id, '$oContact->id');
								$id_contacts[] = $oContact->id;
							} //each $responsible['contacts']

							//удалить контакты, отсутствующие в списке сохраненных
							$criteria = new CDbCriteria();
							$criteria->addNotInCondition('id', $id_contacts);
							$criteria->addCondition('app_resp_ref = ' . $oResponsible->id);
							M::printr($id_contacts, '$id_contacts');
							AppContactFieldValues::model()->deleteAll($criteria);

						}
						//получить список тех, которые будут сохранены (отличных от нуля)
						M::printr($oResponsible->id, '$oResponsible->id');
						$id_responsibles[] = $oResponsible->id;
					}//each $post['responsibles']

					//удалить контакты, отсутствующие в списке сохраненных
					$criteria = new CDbCriteria();
					$criteria->addNotInCondition('id', $id_responsibles);
					$criteria->addCondition('app_client_ref = ' . $oClient->id);
					M::printr($id_responsibles, '$id_responsibles');
					AppResponsibles::model()->deleteAll($criteria);
				}

				//сохраняем реквизиты
				if (isset($post['requisites'])) {
					$id_requisites = array();
					foreach ($post['requisites'] as $key_requisites => $requisite) {
						if ((int)$requisite['id'] !== 0) {
							$oRequisite = AppRequisites::model()->findByPk((int)$requisite['id']);
							if (empty($oRequisite)) {
								throw new Exception('Exception: index `id` = ' . (int)$requisite['id'] . ' not found in table AppRequisites');
							}
						} else {
							$oRequisite = new AppRequisites();
						}
						$oRequisite->attributes = $requisite;
						$oRequisite->app_client_ref = $oClient->id;
						if (!$oRequisite->save()) {
							throw new Exception('Exception: error save data in table AppRequisites');
						}
						//получить список тех, которые будут сохранены (отличных от нуля)
						M::printr($oRequisite->id, '$oRequisite->id');
						$id_requisites[] = $oRequisite->id;
					} //each $post['requisites']
					//удалить контакты, отсутствующие в списке сохраненных
					$criteria = new CDbCriteria();
					$criteria->addNotInCondition('id', $id_requisites);
					$criteria->addCondition('app_client_ref = ' . $oClient->id);
					M::printr($id_requisites, '$id_requisites');
					AppRequisites::model()->deleteAll($criteria);
				}

				$transaction->commit();


				exit;
			} catch (Exception $e) {
				M::printr($e->getMessage(), 'EXCEPTION!!!');
				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				$transaction->rollBack();
			} //try
		} //if (!empty($_POST))

		$this->render('edit', $data);
	}

	public function actionView($client_id = 0)
	{
		$data = array();
		$formName = 'sss';
		$data['formName'] = $formName;
		if (isset($_POST['client_id'])) {
			$client_id = $_POST['client_id'];
		}

		//взять группы пользователя
		$oClientGroups = new AppUserGroups();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = $oClientGroups->findAll($criteria);
		$arr = array();
		foreach ($groups as $key_groups => $group) {
			$arr[$key_groups] = $group->attributes;
		}
		$data['user_groups'] = $arr;

		//берем все данные о клиенте
		$oClients = new AppClients();
		$clients = $oClients
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
					'appRequisites',
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findByPk($client_id);

		//берем официальные лица в массив
		$responsibles = array();
		foreach ($clients->appResponsibles as $key_responsible => $responsible) {
			$responsibles[$key_responsible] = $responsible->attributes;
			//берем контакты официальных лиц
			$responsibles[$key_responsible]['contacts'] = array();
			foreach ($responsible->appContactFieldValues as $key_contact => $contact) {
				$responsibles[$key_responsible]['contacts'][$key_contact] = $contact->attributes;
			}
		}

		//берем реквизиты
		$requisites = array();
		foreach ($clients->appRequisites as $key_requisite => $requisite) {
			$requisites[$key_requisite] = $requisite->attributes;
		}

		//берем группы клиента
		$client_groups = array();
		foreach ($clients->appClientHasGroups as $key_client_group => $client_group) {
			$client_groups[$key_client_group] = $client_group->app_user_group_ref;
		}
		//берем данные клиента в массив
		$client = $clients->attributes;
		$data['client'] = $client;
		$data['responsibles'] = $responsibles;
		$data['requisites'] = $requisites;
		$data['client_groups'] = $client_groups;

		//взять группы клиента
		$oClientGroups = new AppClientHasGroups();
		$clientGroups = $oClientGroups->findAllByAttributes(
			array('app_client_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);

		//взять типы доп. полей
		$oFields = new AppContactFields();
		$fields = $oFields->findAll(array('order' => 'field_label ASC'));
		$data['fields'] = $fields;
		$data['jsFields'] = CJSON::encode($fields);

		//взять доп. поля клиента
		$oFieldValues = new AppContactFieldValues();
		$fieldValues = $oFieldValues->findAllByAttributes(
			array('app_resp_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);
		$data['fieldValues'] = $fieldValues;
		$data['jsFieldValues'] = CJSON::encode($fieldValues);

		$this->render('edit', $data);
	}

}