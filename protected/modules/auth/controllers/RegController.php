<?php

class RegController extends Controller
{
	public $defaultAction = 'reg';

	public function actionIndex() {
		$this->render('index');
	}

	//регистрация пользователя
	public function actionReg() {
		$model = new FormReg;
		if (isset($_POST['FormReg'])) {
			$model->attributes = $_POST['FormReg'];
			if ($model->validate()) {
				//все верно
				//добавить данные в БД
				print "Добавить пользователя в БД...";
				$oUsers = new aclUsers;
				$oUsers->email = $model->email;
				$oUsers->password = md5($model->password);
				if ($oUsers->save()) {
					$this->redirect(array('/auth/login/login'));
				}
				//register successfully
			}
		}
		$this->render('reg', array('model' => $model));
	}
}