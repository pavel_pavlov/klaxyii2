<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	public function actionIndex() {

		/*
		print "<br><br>";
		echo 'NAME: ' . Yii::app()->user->name . "<br>\n";
		echo 'ID: ' . Yii::app()->user->id . "<br>\n";
		*/


		/*
		$_SESSION['var_name'] = 'var_value';
		//добавление переменной в сессию
		Yii::app()->session['dt_update']='asdfasdf';
		$x = Yii::app()->session['dt_update'];
		M::printr($x, '$x');

		$url = $this->createUrl('post/read', array('id' => 100, '#' => 'title'));
		M::printr($url, '$url');
		M::printr(Yii::app()->user, 'Yii::app()->user');
		Yii::app()->user->setState('dt_create', 'dt_createdt_createdt_create');
		$Y = Yii::app()->user->getState('dt_create');
		M::printr($Y, '$Y');

		M::printr(Yii::app()->user, 'Yii::app()->user');
		*/
		//M::printr($_SESSION, '$_SESSION');
		//M::printr($_COOKIE, '$_COOKIE');
		$this->render('index');
	}

	//Авторизация пользователя
	public function actionLogin() {
		//достаем форму логина
		$model = new FormLogin;

		//Если вызвана ajax-валидация
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		//проверяем, не передавалось ли что-нибудь уже...
		if (isset($_POST['FormLogin'])) {
			$model->attributes = $_POST['FormLogin'];
			//проверяем валидность введенных данных
			if ($model->validate() && $model->login()) {
				//редиректим на страницу
				//$this->redirect(array('site/author','id'=>$model->id, 'title'=>$model->title));
				$this->redirect(array('/clients/clients/index'));
			}
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}