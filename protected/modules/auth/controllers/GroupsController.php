<?php

class GroupsController extends Controller
{
	public $defaultAction = 'List';

	public function actionIndex() {
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('index');
	}

	public function actionAdd() {
		//M::printr($_POST, '$_POST');
		//M::printr(Yii::app()->session, 'Yii::app()->session');
		//M::printr(Yii::app()->user->id, 'Yii::app()->user');

		if(Yii::app()->user->isGuest){
			$this->redirect(array('login/login'));
		}

		$data = array();
		$model = new AppClientGroups;

		//проверяем, не передавалось ли что-нибудь уже...
		if (isset($_POST['AppUserGroups'])) {
			$model->attributes = $_POST['AppUserGroups'];
			//M::printr($model->attributes, '$model->attributes');
			//M::printr(Yii::app()->user->id, 'Yii::app()->user->id');
			//exit;;
			//проверяем валидность введенных данных
			$model->acl_user_ref = Yii::app()->user->id;
			if ($model->validate() && $model->save()) {
				//редиректим на страницу
				//$this->redirect(array('site/author','id'=>$model->id, 'title'=>$model->title));
				$this->redirect(array('groups/list'));
			}
		}
		$data['model'] = $model;
		$this->render('add', $data);
	}

	public function actionList() {
		if(Yii::app()->user->isGuest){
			$this->redirect(array('login/login'));
		}
		$data[] = array();


		$oClients = new AppClients();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$clients_obj = $oClients->findAll($criteria);

		$oGroups = new AppUserGroups;
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = $oGroups->findAll($criteria);

		$str = CJSON::encode($groups);

		$data['str'] = $str;
		$this->render('list', $data);
	}

}