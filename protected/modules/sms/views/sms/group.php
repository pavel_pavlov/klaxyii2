<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<div id="menu">
	<?php
	$controller = Yii::app()->controller->id;
	$this->widget(
		'zii.widgets.CMenu', array(
			'items' => array(
				array('label' => 'Index', 'url' => array("{$controller}/index")),
				array('label' => 'List', 'url' => array("{$controller}/list")),
				array('label' => 'View', 'url' => array("{$controller}/view")),
				array('label' => 'NewGroups', 'url' => array("{$controller}/newGroups")),
				array('label' => 'NewPersonal', 'url' => array("{$controller}/newPersonal")),
			)
		)
	); ?>
</div><!-- menu -->

<div class="form">

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'app-client-groups-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// See class documentation of CActiveForm for details on this,
			// you need to use the performAjaxValidation()-method described there.
			'enableAjaxValidation' => false,
		)
	); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'group'); ?>
		<?php echo $form->textField($model, 'group'); ?>
		<?php echo $form->error($model, 'group'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->