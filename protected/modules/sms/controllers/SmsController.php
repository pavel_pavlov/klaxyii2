<?php

class SmsController extends Controller
{
	public function actionIndex() {

		$x = '[{"status":0,"description":"112198003321999379"},{"status":11,"description":"Invalid message receiver."},{"status":0,"description":"112198003338701279"}]';

		$this->render('index', array('x' => $x));
	}

	public function actionNew() {
		$render = array();
		//форма
		$mForm = new FormSmsNew();
		$name = get_class($mForm);
		$POST = Yii::app()->request->getPost(get_class($mForm));

		//приняли данные формы
		if (!empty($POST)) {
			$mForm->attributes = $POST;
			//проверить данные формы валидацией
			if ($mForm->validate()) {
				$groups = json_decode($POST['groups']);
				//M::printr($groups, '$groups');

				//взять пользователей полученных групп
				$mClientGroups = new AppClientHasGroups();
				$clients = $mClientGroups->getByGroupIds($groups);

				foreach ($clients as $client) {
					$phones[] = $client['phone'];
				}
				M::printr($phones, '$phones');
				$mSms = new Sms();
				$send_many = $mSms->send_many($phones, $mForm->sms_text);
				M::printr($send_many, '$send_many');
			}
		}


		$render['mForm'] = $mForm;

		$mGroups = new AppClientGroups();
		$groups = $mGroups->findAllByAttributes(array('acl_user_ref' => Yii::app()->user->id), array('order' => 'id ASC'));
		$jsonGroups = CJSON::encode($groups);
		$render['jsonGroups'] = $jsonGroups;

		$this->render('new', $render);
	}

	public function actionGroups() {
		$render = array();


		$this->renderPartial('', $render);
	}


}