<?php

class QuestionsController extends Controller
{
	//public $layout = '//layouts/friday13';
	public $defaultAction = "index";

	public function actionIndex() {
		$this->render('index');
	}

	public function actionList() {
		$data = array();

		$questions_text = array(
			'0' => array(
				'question' => 'Участвовали ли ранее в каких-либо квестах?',
				'ans' => array(
					'1' => 'Да',
					'2' => 'Нет',
					'3' => 'Не понял',
				),
			),
			'1' => array(
				'question' => 'На квест по мотивам какого жанра кино, литературного произведения Вы бы пошли с друзьями?',
				'ans' => array(
					'1' => 'Ужас',
					'2' => 'Комедия',
					'3' => 'Приключения',
					'4' => 'Криминал',
					'5' => 'Фантастика',
					'6' => 'Эротика',
				),
			),
			'2' => array(
				'question' => 'Что произвело бы на Вас впечатление при прохождении квеста в жанре хоррор?',
				'ans' => array(
					'1' => 'Мистика, сверхъестественные создания',
					'2' => 'Изуродованные тела, оторванные конечности',
				),
			),
			'3' => array(
				'question' => 'Каких спецэффектов Вы бы хотели видеть больше в реалити квестах?',
				'ans' => array(
					'1' => 'Световые',
					'2' => 'Звуковые',
					'3' => 'Механические',
					'4' => 'Электрические',
					'5' => 'Сенсорные',
				),
			),
			'4' => array(
				'question' => 'Какие задачи Вы бы с удовольствием решали при прохождении квеста?',
				'ans' => array(
					'1' => 'Математические',
					'2' => 'Логические (например решаемые с конца)',
					'3' => 'Пазлы',
					'4' => 'Химические',
					'5' => 'Физические (применение физической силы)',
				),
			),
			'5' => array(
				'question' => 'Какой стиль игры Вы предпочитаете?',
				'ans' => array(
					'1' => 'Командная работа (слаженные действия, к победе приходит вся команда)',
					'2' => 'Соревноваться с участниками своей команды (победитель один)',
				),
			),
			'6' => array(
				'question' => 'Вы бы пожертвовали своей одеждой (например испачкать, порвать, намочить и т.п.) во время прохождения квеста?',
				'ans' => array(
					'1' => 'Да',
					'2' => 'Нет',
				),
			),
			'7' => array(
				'question' => 'Квест с каким финалом Вам был бы более интересен?',
				'ans' => array(
					'1' => 'Выйти из комнаты',
					'2' => 'Освободить друга',
					'3' => 'Набрать большее кол-во баллов',
					'4' => 'Выиграть приз',
				),
			),
		);


		$answers = array();
		for ($i = 0; $i <= 7; $i++) {
			$field = "req" . $i;
			$result = Yii::app()->db->createCommand()
				->select("{$field} AS \"variant\", COUNT({$field}) AS \"counter\"")
				->from("app_questions")
				->where("{$field} > 0")
				->group("variant")
				->order("counter DESC, variant ASC")
				->queryAll();
			//M::printr($result, '$result');
			$arr = array();
			foreach ($result as $k => $v) {
				$arr[$v['variant']] = $v;
			}
			//M::printr($arr, '$arr');
			$answers[$i] = $arr;
		}
		//M::printr($answers, '$answers');

		$arr = array();
		//привести массив в вид {вопрос=>ответ}
		foreach ($questions_text as $num_question => $question) {
			//M::printr($question, '$question');
			$arr[$question['question']] = array();
			foreach ($question['ans'] as $num_variant => $variant) {
				//M::printr($variant, '$variant');
				if(!isset($answers[$num_question][$num_variant])) {
					$answers[$num_question][$num_variant]['counter'] = 0;
				}
				$arr[$question['question']][$question['ans'][$num_variant]] = $answers[$num_question][$num_variant]['counter'];
				//$arr[$question['question']][$answer[$num_question]['variant']] = $answer[$num_question]['counter'];
			}
			//ksort($arr[$question['question']]);
			arsort($arr[$question['question']]);
		}
		//M::printr($arr, '$arr');
//exit;

		$data['answers'] = $answers;
		$data['questions_text'] = $questions_text;
		$data['arr'] = $arr;
		$this->render('list', $data);
	}

	public function actionFriday13Client() {
		//принять данные от сайта и передать эти данные на регистрацию пользователя
		$post = $_POST;
		//M::printr($post, '$post');
		if (!isset($post['ref'])) {
			exit;
		}
		$ref = $post['ref'];

		//создать образ клиента
		$user['phones'] = isset($post['phone']) ? $post['phone'] : null;
		$user['email'] = isset($post['email']) ? $post['email'] : null;
		$user['app_client_ref'] = $post['ref'];
		$user['responsibles'] = array(
			array(
				'first_name' => isset($post['name']) ? $post['name'] : null,
				'second_name' => isset($post['sname']) ? $post['sname'] : null,
				'is_responsible' => true,
				'contacts' => array(
					array(
						'field_ref' => 2,
						'field_value' => isset($post['email']) ? $post['email'] : null,
						'send_allow' => true,
					),
					array(
						'field_ref' => 6,
						'field_value' => isset($post['phone']) ? $post['phone'] : null,
						'send_allow' => true,
					),
				),
			),
		);

		//M::printr($user, '$user');
		//exit;
		API::reg_client($user);
	}

	public function actionFriday13Questions($ref = 1) {
		//принять данные от сайта и передать эти данные на регистрацию пользователя
		$post = $_POST;
		//M::printr($post, '$post');
		$client_id = API::addQuestions($post);
	}

}