<h1 align="center">Статистика по опросу.</h1>
<style>
	table, tr, td {
		font-family: Arial, Helvetica, Courier, Times;
		color: #000000;
	}

	/*th {
		color: #00FFAB;
	}*/

	.answers {
		font-size: 14px;
		color: #fff;
	}

	.ques {
		width: 300px;
	}

	.ans {
		width: 300px;
		text-align: left;
	}

</style>

<table class="answers" border='0' width='600px' align='center'>
	<?php
	foreach ($arr as $k => $v) {
		print "<th colspan='2'>{$k}</th>\n";
		foreach ($v as $k_ans => $ans) {
			print "<tr><td>{$k_ans}</td><td>{$ans}</td></tr>\n";
		}
		print "<tr><td colspan='2'><span style='font-size: 1em;'>&nbsp;</span></td></tr>\n";
	}
	?>
</table>
