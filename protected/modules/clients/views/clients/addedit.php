<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style type="text/css">
	.hide_tab {
		display: none;
	}

	.show_tab {
		display: block;
	}

	.border {
		border: 1px solid #000000;
	}

	.border_red {
		border: 1px solid #FF0000;
	}

	.border_green {
		border: 1px solid #00FF00;
	}

	.row {
		background-color: #eeeeee;
		margin: 5px;
	}

	.dx-button-text {
		color: #ffffff;
		font-family: Arial;
		font-size: 13px;
	}

	.dx-button {
		margin: 5px 5px;
	}

	.notify {
		position: absolute;
		right: 30px;
		top: 10px;
		border-radius: 10px;
	}

	.inner-notify {
		border-radius: 10px;
		position: relative;
		width: 100%;
		height: 100%;
		padding: 10px;
		font-family: Arial, Helvetica, Courier, Times;
	}

</style>

<!-- Шаблоны -->
<div style="display: none;">
	<!-- Шаблон notify -->
	<div class="notify-template">
		<div class="inner-notify">
		</div>
	</div>

	<!-- Шаблон блока Ответственных лиц -->
	<div id="template_responsible_" style="width: 600px; margin: 5px;">
		<div class="row" style="padding: 5px; background-color: #cccccc; border: 1px solid #000000; border-radius: 5px;">
			<div style="width: 200px;">
				Фамилия:
				<div id="second_name"></div>
			</div>
			<div style="width: 200px;">
				Имя:
				<div id="first_name"></div>
			</div>
			<div style="width: 200px;">
				Отчество:
				<div id="middle_name"></div>
			</div>
			<div style="width: 200px;">
				<button id="to_header" class="dx-button">В заголовок</button>
			</div>
			<div style="width: 200px;">
				Должность:
				<div id="position"></div>
			</div>
			<div style="width: 200px;">
				Пароль:
				<div id="password"></div>
			</div>
			<div style="width: 200px;">
				<div id="is_responsible"></div>
			</div>
			<div style="width: 100%;">
				<div id="contacts_">
					<!-- место для контактных данных -->
				</div>
				<!--На элементы с id="add_contact" и id="del_responsible" будут подвешены события onClick-->
				<button id="add_contact" class="dx-button dx-button-success dx-button-text">+</button>
				<button id="del_responsible" class="dx-button dx-button-danger dx-button-text">X</button>
			</div>
		</div>
	</div>

	<!-- Шаблон блока дополнительного контакта -->
	<div id="template_contact_" style="width: 400px; margin: 5px;">
		<div class="row" style="padding: 5px; background-color: #aaaaaa; border: 1px solid #000000; border-radius: 5px;">
			<div style="width: 200px;">
				Тип контакта:
				<div id="field_ref"></div>
			</div>
			<div style="width: 100%;">
				Текст:
				<div id="field_value"></div>
			</div>
			<div style="width: 400px;">
				<div id="send_allow"></div>
			</div>
			<div style="width: 100%;">
				<!--На элемент с id="add_contact" будет подвешено событие onClick-->
				<button id="del_contact" class="dx-button dx-button-danger dx-button-text">X</button>
			</div>
		</div>
	</div>

	<!-- Шаблон блока реквизитов -->
	<div id="template_requisite_" style="width: 600px; margin: 5px;">
		<div class="row" style="padding: 5px; background-color: #bbbbbb; border: 1px solid #000000; border-radius: 5px;">
			<div style="width: 200px;">
				<span>Полное наименование:</span>
				<div id="full_name"></div>
			</div>
			<div style="width: 200px;">
				Юридический адрес:
				<div id="address_juridical"></div>
			</div>
			<div style="width: 200px;">
				Фактический адрес:
				<div id="address_fact"></div>
			</div>
			<div style="width: 200px;">
				ИНН:
				<div id="inn"></div>
			</div>
			<div style="width: 200px;">
				КПП:
				<div id="kpp"></div>
			</div>
			<div style="width: 200px;">
				ОГРН:
				<div id="ogrn"></div>
			</div>
			<div style="width: 300px;">
				Банк:
				<div id="bank"></div>
			</div>
			<div style="width: 300px;">
				БИК:
				<div id="bik"></div>
			</div>
			<div style="width: 300px;">
				Расчетный счет:
				<div id="r_schet"></div>
			</div>
			<div style="width: 300px;">
				Корреспондентский счет:
				<div id="kor_schet"></div>
			</div>
			<div style="width: 100%;">
				<button class="dx-button dx-button-danger dx-button-text" id="del_requisite">
					X<!--На эту кнопку будет подвешено событи-->
				</button>
			</div>
		</div>
	</div>

	<!-- Шаблон блока свойство -->
	<div id="template_property_" style="width: 1222px; margin: 5px;">
		<div class="row" style="padding: 5px; background-color: #bbbbbb; border: 1px solid #000000; border-radius: 5px;">
			<div style="width: 1200px;">
				<!-- наименование свойства -->
				<div id="name"></div>

				<!-- значение -->
				<div id="value" style="width: 300px;"></div>

				<!-- само ед.изм. -->
				<div id="measure" style="width: 100px;"></div>

			</div>
		</div>
	</div>
</div>

<span><a href="javascript: show('main')">Основные свойства</a></span>
<span><a href="javascript: show('contacts')">Контактные данные</a></span>
<span><a href="javascript: show('banks')">Реквизиты</a></span>
<span><a href="javascript: show('properties')">Дополнительные свойства</a></span>

<div class="form">
	<?php
	$form = new dxForm('EditClient');
	if(!empty($client)){
		$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id, 'client_id' => $client['id']));
	} else{
		$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
	}
	$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));
	$form->init();
	?>
	<script type="text/javascript">
		var user_groups = <?= CJSON::encode($user_groups) ?>;
		var props = <?= CJSON::encode($props) ?>;
		console.log('[props]', props);
		var userData;
		var groupList = [];
		<?= $form->formName; ?>.responsible_count = 1; //номер следующего ответственного лица
		<?= $form->formName; ?>.responsibles = {}; //ответственные лица
		<?= $form->formName; ?>.requisite_count = 1; //номер следующего блока реквизитов
		<?= $form->formName; ?>.requisites = {}; //реквизиты
		<?= $form->formName; ?>.properties = {}; //свойства


		//добавление свойств группы
		/**
		 * добавляет свойства группы "group_id" шаблона "Template" в контейнер "Target"
		 *
		 * @param Template
		 * @param Target
		 * @param group_id
		 */
		function add_property(Template, Target, group_id) {
			$(document).ready(function () {
				console.log('[Template]', Template);
				console.log('[Target]', Target);
				console.log(props.groups[group_id].properties);

				console.log('[TARGET]', $('#' + Target));

				//$('#' + Target).text(group_id);
				for (index in props.groups[group_id].properties) {
					var property_id = props.groups[group_id].properties[index];
					if ($('#property_' + property_id).length == 0) {
						//взять шаблон
						var Tproperty = $('#' + Template).clone().attr({'id': 'property_' + property_id});
						$('#' + Target).append(Tproperty);
						//заменить в нем значения
						Tproperty.find('#name').text(props.properties[property_id].name);
						console.log('[Tproperty]', Tproperty);

						Tproperty.find('#measure').dxTextBox({
							showClearButton: false,
							value: props.properties[property_id].measure,
							disabled: true,
							placeholder: 'ед. изм.'
						});
						//<?= $form->formName; ?>.properties[property_id] = Tproperty.find('#measure').dxTextBox('instance');

						var type_id = props.properties[property_id].app_property_type_ref;
						var type = props.types[type_id].table_type;

						if (props.types[type_id].is_multiple === false) {
							Tproperty.find('#value').dxTextBox({
								showClearButton: false,
								value: props.values[property_id].number,
								placeholder: 'значение'
							});
							<?= $form->formName; ?>.properties[property_id] = Tproperty.find('#value').dxTextBox('instance');
						} else {
							//console.log('[DATA]', props.properties[property_id].data);
							Tproperty.find('#value').dxSelectBox({
								items: JSON.parse(props.properties[property_id].data),
								value: props.properties[property_id][type],
								placeholder: 'значение',
								displayExpr: "value",
								valueExpr: "id"
							});
							<?= $form->formName; ?>.properties[property_id] = Tproperty.find('#value').dxSelectBox('instance');
						}
					}
				}
				console.log(<?= $form->formName; ?>);
				//console.log(Tproperty, 'Tproperty');
			});
		}

		//удаление свойств группы
		function del_property(group_id) {
			//проверить, есть ли свойство в оставшихся группах
			//если нет, то удалить
			//есть есть, то оставить.
		}

		//добаление/удаление группы
		function onValueChanged(values) {
			if (values.length == groupList.length) {
				return true;
			}
			var arr = diffArrays(values, groupList);

			var element, index;
			if (arr[0].length > arr[1].length) {
				//добавление
				for (index in arr[0]) {
					element = arr[0][index];
					add_property('template_property_', 'client_properties', element);
				}
			} else {
				//удаление
				for (index in arr[1]) {
					element = arr[1][index];
					del_property(element);
				}
			}
			groupList = [];
			groupList = copy(values);
		}

		function show(name) {
			$('.tab').addClass('hide_tab');
			$('.tab_' + name).removeClass('hide_tab');
		}

		//добавление ответственного лица
		function add_responsible(data) {
			//console.log('[responsible.data]', data);
			if (data === undefined) {
				data = {};
				data.id = 0;
				data.first_name = '';
				data.second_name = '';
				data.middle_name = '';
				data.position = '';
				data.password = '';
				data.is_responsible = false;
				data.contacts = {};
			}
			i = <?= $form->formName; ?>.responsible_count;
			<?= $form->formName; ?>.responsibles[i] = {};
			var responsible = <?= $form->formName; ?>.responsibles[i];
			responsible.div = {};
			responsible.dx = {};
			responsible.contact_count = 1;
			responsible.contacts = {};

			var contacts = responsible.contacts;

			var delButton = $('<input>')
				.attr({
					'type': 'button',
					'onClick': 'del_responsible(' + i + ')',
					'value': 'Удалить ' + i
				})
				.addClass('dx-button dx-button-danger');

			if (i === 1) {
				delButton = null;
			}

			responsible.id = data.id;

			responsible.div.first_name = $('<div>').dxTextBox({
				showClearButton: true,
				value: data.first_name
			});
			responsible.dx.first_name = responsible.div.first_name.dxTextBox('instance');

			responsible.div.second_name = $('<div>').dxTextBox({
				showClearButton: true,
				value: data.second_name
			});
			responsible.dx.second_name = responsible.div.second_name.dxTextBox('instance');

			responsible.div.middle_name = $('<div>').dxTextBox({
				showClearButton: true,
				value: data.middle_name

			});
			responsible.dx.middle_name = responsible.div.middle_name.dxTextBox('instance');

			responsible.div.position = $('<div>').dxTextBox({
				showClearButton: true,
				value: data.position
			});
			responsible.dx.position = responsible.div.position.dxTextBox('instance');

			responsible.div.password = $('<div>').dxTextBox({
				showClearButton: true, mode: 'password',
				value: data.password
			});
			responsible.dx.password = responsible.div.password.dxTextBox('instance');

			responsible.div.is_responsible = $('<div>').dxCheckBox({
				text: 'Ответственное лицо',
				value: false
			});
			responsible.dx.is_responsible = responsible.div.is_responsible.dxCheckBox('instance');

			//берем шаблон и дополняем его данные
			var Tresponsible = $('#template_responsible_').clone().attr({'id': 'responsible_' + i});
			Tresponsible.find('#first_name').replaceWith(responsible.div.first_name);
			Tresponsible.find('#second_name').replaceWith(responsible.div.second_name);
			Tresponsible.find('#middle_name').replaceWith(responsible.div.middle_name);
			Tresponsible.find('#to_header').attr({'onClick': 'to_header(' + i + ')'});
			Tresponsible.find('#position').replaceWith(responsible.div.position);
			Tresponsible.find('#password').replaceWith(responsible.div.password);
			Tresponsible.find('#is_responsible').replaceWith(responsible.div.is_responsible);
			Tresponsible.find('#contacts_').attr({'id': 'contacts_' + i});
			Tresponsible.find('#add_contact').attr({'onClick': 'add_contact(' + i + ')'});
			if (i === 1) {
				Tresponsible.find('#del_responsible').remove();
			}
			Tresponsible.find('#del_responsible').attr({'onClick': 'del_responsible(' + i + ')'});
			$('#client_responsibles').append(Tresponsible);

			//добавление видимого блока в DOM
			//<?= $form->formName; ?>.responsible_div.append(responsible_div);
			if(data.contacts.length > 0){
				for(var index in data.contacts){
					add_contact(i, data.contacts[index]);
				}
			}
			<?= $form->formName; ?>.responsible_count++;
		}

		//удаление ответственного лица
		function del_responsible(i) {
			delete <?= $form->formName; ?>.responsibles[i];
			$('#responsible_' + i).remove();
		}

		//добавление контактного поля в responsible[n].contacts
		function add_contact(i, data) {
			j = <?= $form->formName; ?>.responsibles[i].contact_count;
			<?= $form->formName; ?>.responsibles[i].contacts[j] = {};
			var contact = <?= $form->formName; ?>.responsibles[i].contacts[j];
			var contacts_div = $('#contacts_' + i);

			//console.log('[contact.data]', data);
			if (data === undefined || data.length === 0) {
				data = {};
				data.id = 0;
				data.field_ref = null;
				data.field_value = '';
				data.send_allow = false;
			}

			contact.div = {};
			contact.dx = {};

			//создать блок контакта
			//тип контакта
			contact.id = data.id;
			contact.div.field_ref = $('<div>').dxSelectBox({
				dataSource: <?= $jsFields ?>,
				displayExpr: 'field_label',
				valueExpr: 'id',
				value: data.field_ref
			});
			contact.dx.field_ref = contact.div.field_ref.dxSelectBox('instance');

			//текст контакта
			contact.div.field_value = $('<div>').dxTextBox({
				value: data.field_value
			});
			contact.dx.field_value = contact.div.field_value.dxTextBox('instance');

			contact.div.send_allow = $('<div>').dxCheckBox({
				text: 'Получать уведомления и рассылки ',
				value: data.send_allow
			});
			contact.dx.send_allow = contact.div.send_allow.dxCheckBox('instance');

			contact.div.delButton = $('<div>').append($('<input>')
				.attr({
					'type': 'button',
					'onClick': 'del_contact(' + i + ',' + j + ')',
					'value': 'X'
				})
				.addClass('dx-button dx-button-danger'));

			var Tcontact = $('#template_contact_').clone().attr({'id': 'contact_' + i + '_' + j});
			Tcontact.find('#field_ref').append(contact.div.field_ref);
			Tcontact.find('#field_value').append(contact.div.field_value);
			Tcontact.find('#send_allow').append(contact.div.send_allow);
			Tcontact.find('#del_contact').attr({'onClick': 'del_contact(' + i + ',' + j + ')'});
			$('#contacts_' + i).append(Tcontact);

			<?= $form->formName; ?>.responsibles[i].contact_count++;
		}

		//удаление контакта
		function del_contact(i, j) {
			delete
			<?= $form->formName; ?>.
			responsibles[i].contacts[j];
			$('#contact_' + i + '_' + j).remove();
		}

		//добавление реквизитов
		function add_requisite(data) {
			if (data === undefined || data.length === 0) {
				data = {};
				data.id = 0;
				data.full_name = '';
				data.address_juridical = '';
				data.address_fact = '';
				data.inn = '';
				data.kpp = '';
				data.ogrn = '';
				data.bank = '';
				data.bik = '';
				data.r_schet = '';
				data.kor_schet = '';
			}

			i = <?= $form->formName; ?>.requisite_count;
			<?= $form->formName; ?>.requisites[i] = {};
			var requisite = <?= $form->formName; ?>.requisites[i];
			requisite.div = {};
			requisite.dx = {};

			//создать поля реквизитов
			requisite.id = data.id;

			requisite.div.full_name = $('<div>').dxTextBox({value: data.full_name});
			requisite.dx.full_name = requisite.div.full_name.dxTextBox('instance');

			requisite.div.address_juridical = $('<div>').dxTextBox({value: data.address_juridical});
			requisite.dx.address_juridical = requisite.div.address_juridical.dxTextBox('instance');

			requisite.div.address_fact = $('<div>').dxTextBox({value: data.address_fact});
			requisite.dx.address_fact = requisite.div.address_fact.dxTextBox('instance');

			requisite.div.inn = $('<div>').dxTextBox({value: data.inn});
			requisite.dx.inn = requisite.div.inn.dxTextBox('instance');

			requisite.div.kpp = $('<div>').dxTextBox({value: data.kpp});
			requisite.dx.kpp = requisite.div.kpp.dxTextBox('instance');

			requisite.div.ogrn = $('<div>').dxTextBox({value: data.ogrn});
			requisite.dx.ogrn = requisite.div.ogrn.dxTextBox('instance');

			requisite.div.bank = $('<div>').dxTextBox({value: data.bank});
			requisite.dx.bank = requisite.div.bank.dxTextBox('instance');

			requisite.div.bik = $('<div>').dxTextBox({value: data.bik});
			requisite.dx.bik = requisite.div.bik.dxTextBox('instance');

			requisite.div.r_schet = $('<div>').dxTextBox({value: data.r_schet});
			requisite.dx.r_schet = requisite.div.r_schet.dxTextBox('instance');

			requisite.div.kor_schet = $('<div>').dxTextBox({value: data.kor_schet});
			requisite.dx.kor_schet = requisite.div.kor_schet.dxTextBox('instance');

			requisite.div.delButton = $('<div>')
				.append($('<input>')
					.attr({
						'type': 'button',
						'onClick': 'del_requisite(' + i + ')',
						'value': 'X'
					})
					.addClass('dx-button dx-button-danger'));

			var Trequisite = $('#template_requisite_').clone().attr({'id': 'requisite_' + i});
			Trequisite.find('#full_name').append(requisite.div.full_name);
			Trequisite.find('#address_juridical').append(requisite.div.address_juridical);
			Trequisite.find('#address_fact').append(requisite.div.address_fact);
			Trequisite.find('#inn').append(requisite.div.inn);
			Trequisite.find('#kpp').append(requisite.div.kpp);
			Trequisite.find('#ogrn').append(requisite.div.ogrn);
			Trequisite.find('#bank').append(requisite.div.bank);
			Trequisite.find('#bik').append(requisite.div.bik);
			Trequisite.find('#r_schet').append(requisite.div.r_schet);
			Trequisite.find('#kor_schet').append(requisite.div.kor_schet);
			Trequisite.find('#del_requisite').attr({'onClick': 'del_requisite(' + i + ')'});
			$('#client_requisites').append(Trequisite);
			<?= $form->formName; ?>.
			requisite_count++;
		}

		//удаление реквизитов
		function del_requisite(i) {
			delete <?= $form->formName; ?>.requisites[i];
			$('#requisite_' + i).remove();
		}

		//заполнение заголовка
		function to_header(i) {
			var data = <?= $form->formName; ?>.responsibles[i];
			var header = data.dx.second_name.option('value') + ' ' + data.dx.first_name.option('value') + ' ' + data.dx.middle_name.option('value');
			alert(header);
			<?= $form->formName; ?>.header.option('value', header);
		}

		//сбор данных со всей формы
		function collect_data() {
			var dataForm = {};
			console.log(<?= $form->formName ?>);
			//сбор основных данных клиента
			dataForm.type = <?= $form->formName ?>.type.option('value');
			dataForm.password = <?= $form->formName ?>.password.option('value');
			dataForm.header = <?= $form->formName ?>.header.option('value');
			dataForm.manager = <?= $form->formName ?>.manager.option('value');
			dataForm.status = <?= $form->formName ?>.status.option('value');
			dataForm.groups = <?= $form->formName ?>.groups.option('values');
			dataForm.description = <?= $form->formName ?>.description.option('value');
			dataForm.is_hide_in_rating = <?= $form->formName ?>.is_hide_in_rating.option('value');
			dataForm.phones = <?= $form->formName ?>.phones.option('value');
			dataForm.email = <?= $form->formName ?>.email.option('value');
			dataForm.url = <?= $form->formName ?>.url.option('value');
			console.log('[dataForm]',dataForm);
			//сбор ответственных лиц
			var responsibles = <?= $form->formName ?>.responsibles;
			dataForm.responsibles = {};
			for(var resp in responsibles) {
				dataForm.responsibles[resp] = {};
				dataForm.responsibles[resp].id = responsibles[resp].id;
				dataForm.responsibles[resp].first_name = responsibles[resp].dx.first_name.option('value');
				dataForm.responsibles[resp].second_name = responsibles[resp].dx.second_name.option('value');
				dataForm.responsibles[resp].middle_name = responsibles[resp].dx.middle_name.option('value');
				dataForm.responsibles[resp].position = responsibles[resp].dx.position.option('value');
				dataForm.responsibles[resp].password = responsibles[resp].dx.password.option('value');
				dataForm.responsibles[resp].is_responsible = responsibles[resp].dx.is_responsible.option('value');
				console.log('[dataForm.responsibles[resp]]', dataForm.responsibles[resp]);

				//сбор контактов
				var contacts = responsibles[resp].contacts;
				dataForm.responsibles[resp].contacts = {};
				for (var contact in contacts) {
					dataForm.responsibles[resp].contacts[contact] = {};
					dataForm.responsibles[resp].contacts[contact].id = contacts[contact].id;
					dataForm.responsibles[resp].contacts[contact].field_ref = contacts[contact].dx.field_ref.option('value');
					dataForm.responsibles[resp].contacts[contact].field_value = contacts[contact].dx.field_value.option('value');
					dataForm.responsibles[resp].contacts[contact].send_allow = contacts[contact].dx.send_allow.option('value');
					console.log('[dataForm.responsibles[resp].contacts[contact]]', dataForm.responsibles[resp].contacts[contact]);
				}
			}

			//сбор реквизитов
			var requisites = <?= $form->formName ?>.requisites;
			dataForm.requisites = {};
			for(var req in requisites) {
				dataForm.requisites[req] = {};
				dataForm.requisites[req].id = requisites[req].id;
				dataForm.requisites[req].full_name = requisites[req].dx.full_name.option('value');
				dataForm.requisites[req].address_juridical = requisites[req].dx.address_juridical.option('value');
				dataForm.requisites[req].address_fact = requisites[req].dx.address_fact.option('value');
				dataForm.requisites[req].inn = requisites[req].dx.inn.option('value');
				dataForm.requisites[req].kpp = requisites[req].dx.kpp.option('value');
				dataForm.requisites[req].ogrn = requisites[req].dx.ogrn.option('value');
				dataForm.requisites[req].bank = requisites[req].dx.bank.option('value');
				dataForm.requisites[req].bik = requisites[req].dx.bik.option('value');
				dataForm.requisites[req].r_schet = requisites[req].dx.r_schet.option('value');
				dataForm.requisites[req].kor_schet = requisites[req].dx.kor_schet.option('value');
			}
			console.log('[COLLECT DATAFORM]>',dataForm);
			return dataForm;
		}

		//редирект
		function redirect(url) {
			window.location = url;
		}

		//уведомление
		function notify(header, text, type) {
			var head = $('<span>')
				.css({
					'color':'#000000',
					'font-size': '20px'
				})
				.text(header)
				.append('<br>');
			var color = '#ff0000';
			if (type == "success") {
				color = '#bbffbb';
			} else if (type == "warning") {
				color = '#ffbbbb';
			}
			$('.notify').remove();
			var noti = $('.notify-template').clone().removeClass('notify-template').addClass('notify');
			noti.find('.inner-notify').css({'background-color': color}).html(head).append(text);
			$('body').prepend(noti);
		}

		//выполнится после отправки данных
		function after_send(response){
			console.log(response);
			if(response.errors == false){
				notify('Save success','redirect to "List Clients"','success');
				setTimeout(function(){
					if(is_redirect){
						redirect(redirectUrl);
					}
				},3000);
			} else {
				notify('Save error','Some errors...','warning');
			}
		}

		//отправка данных на сервер
		function send_data(url, data) {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: data
			}).success(function (response) {
				console.log('[send_data > response]',response);
				after_send(response);
			}).error(function (data, key, value) {

				return false;
				//after_send(data);
			});
		}

		//сохранить и закрыть (+ редирект)
		function save() {
			var data = collect_data();
			var send_url = saveUrl;
			is_redirect = true;
			send_data(saveUrl, data);
		}

		//только сохранить
		function apply() {
			var data = collect_data();
			var send_url = saveUrl;
			console.log('[SAVEURL]', saveUrl);
			var send_result = send_data(send_url, data);
		}

		//редирект без сохранения
		function cancel() {
			notify('OK','ok','success');
			//DevExpress.ui.notify('text', 'type', 1000);
			//redirect('<?php $form->redirectUrl ?>');
		}

		$(document).ready(function(){
			var is_redirect = false;

			var responsibles = <?= CJSON::encode($responsibles) ?>;
			if (responsibles.length == 0) {
				add_responsible();
			}
			var index;
			for (index in responsibles) {
				var responsible = responsibles[index];
				add_responsible(responsible);
			}

			var requisites = <?= CJSON::encode($requisites) ?>;
			for (index in requisites) {
				var requisite = requisites[index];
				add_requisite(requisite);
			}

			var properties = <?= CJSON::encode($props['properties']) ?>;
			for (index in property) {
				var property = properties[index];
				//add_properties(property);
			}
		});
	</script>

	<div class="tab tab_main hide_tab">
		<div class="row">
			<?php echo CHtml::label('Форма собственности:', 'id_type'); ?>
			<?php $form->dxRadioGroup(
				'type',
				array(
					'items' => array(
						array(
							'id' => 1,
							'type' => 'Type One'
						),
						array(
							'id' => 2,
							'type' => 'Type Two'
						),
					),
					'displayExpr' => 'type',
					'valueExpr' => 'id',
					'value' => isset($client['type']) ? $client['type'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Password', 'id_password'); ?>
			<?php $form->dxTextField(
				'password',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'value' => isset($client['password']) ? $client['password'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Отображаемое имя', 'header'); ?>
			<?php $form->dxTextField(
				'header',
				array(
					'value' => isset($client['header']) ? $client['header'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Менеджер', 'manager'); ?>
			<?php $form->dxTextField(
				'manager',
				array(
					'value' => isset($client['manager']) ? $client['manager'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Статус', 'status'); ?>
			<?php $form->dxSelectBox(
				'status',
				array(
					'items' => new CDbExpression('[{"id": 1,"status":"status1"},{"id": 2,"status":"status2"}]'),
					'displayExpr' => 'status',
					'valueExpr' => 'id',
					'value' => isset($client['status']) ? $client['status'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Группы', 'groups'); ?>
			<?php $form->dxTagBox(
				'groups',
				array(
					'items' => new CDbExpression('user_groups'),
					'displayExpr' => 'group',
					'valueExpr' => 'id',
					'values' => $client_groups,
					'onValueChanged' => new CDbExpression("function(data){
						onValueChanged(this.option('values'));
					}"),
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Комментарии', 'description'); ?>
			<?php $form->dxTextArea(
				'description',
				array(
					'height' => 200,
					//'width' => 500,
					'value' => isset($client['description']) ? $client['description'] : null
				)
			);
			?>
		</div>

		<div class="row">
			<?php //echo CHtml::label('Не показывать в рейтинге', 'is_hide_in_rating'); ?>
			<?php $form->dxCheckBox(
				'is_hide_in_rating',
				array(
					'text' => 'Не показывать в рейтинге',
					'value' => isset($client['is_hide_in_rating']) ? $client['is_hide_in_rating'] : false,
					//'height' => 200,
					//'width' => 500,
					//'value' => isset($client['is_hide_in_rating']) ? $client['is_hide_in_rating'] : null
				)
			);
			?>
		</div>

	</div>
	<!-- tab_main -->

	<div class="tab tab_contacts hide_tab">
		<div class="row">
			<?php echo CHtml::label('Телефоны', 'phones'); ?>
			<?php $form->dxTextField(
				'phones',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => isset($client['phones']) ? $client['phones'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('Email', 'id_email'); ?>
			<?php $form->dxTextField(
				'email',
				array(
					//'mode' => 'password',
					//'height' => '22',
					'placeholder' => '...',
					'value' => isset($client['email']) ? $client['email'] : null
				)
			); ?>
		</div>

		<div class="row">
			<?php echo CHtml::label('URL', 'id_url'); ?>
			<?php $form->dxTextField(
				'url',
				array(
					'value' => isset($client['url']) ? $client['url'] : null
				)
			); ?>
		</div>

		<div class="row">
			<div id="client_responsibles">
			</div>

			<div class="row">
				<?php $form->dxButton(
					'AddResponsible',
					array(
						'text' => 'Добавить ответственное лицо',
						'type' => 'success',
						'onClick' => new CDbExpression('function(data) {add_responsible();}')
					)
				); ?>
			</div>
		</div>
	</div>
	<!-- tab_contacts -->

	<div class="tab tab_banks hide_tab">
		<div class="row">
			<div id="client_requisites">
			</div>
			<div class="row">
				<?php $form->dxButton(
					'requisites',
					array(
						'text' => 'Добавить реквизиты',
						'type' => 'success',
						'onClick' => new CDbExpression('function(data) {add_requisite();}')
					)
				); ?>
			</div>
		</div>
	</div>
	<!-- tab_banks -->

	<div class="tab tab_properties hide_tab">
		<div class="row">
			Свойства появляются автоматически при выборе группы.
			<div id="client_properties"></div>
		</div>
	</div>
	<!-- tab_banks -->
	<div class="row">
		<button class="dx-button dx-button-default" onClick="save();">
			save
		</button>
		<button class="dx-button dx-button-success" onClick="apply();">
			apply
		</button>
		<button class="dx-button dx-button-danger" onClick="cancel();">
			cancel
		</button>
	</div>
</div>
<!-- form -->

