<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>

<style type="text/css">
	#gridContainer {
		height: auto;
		width: 100%;
		margin: 50px 0;
	}

	.dx-texteditor-container {
		height: inherit;
	}

	.hide_tab {
		display: none;
	}

	a {
		color: #0000ff;
		text-decoration: underline;
	}
</style>

<!-- Шаблоны -->
<div style="display: none;">
	<!-- Шаблон блока "Клиент" -->
	<div id="template_client" style="width: 600px;">
		<div style="background-color: #eeeeee; border-radius: 5px;">
			<!--div style="display: none;"></div-->
			<span><a onclick="show(this, 'main');">Основные свойства</a></span> <span><a
					onclick="show(this, 'contacts');">Контактные данные</a></span> <span><a
					onclick="show(this, 'requisites');">Реквизиты</a></span>

			<div class="tab tab_main">
				<div class="row">
					<span style="width: 100px;">type:</span> <span id="type"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">password:</span> <span id="password"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">header:</span> <span id="header"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">manager:</span> <span id="manager"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">status:</span> <span id="status"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">groups:</span> <span id="groups"></span>
				</div>

				<div class="row">
					<span style="width: 100px;">description:</span> <span id="description"></span>
				</div>

				<div class="row">
					<!--span style="width: 100px;">Показывать ли в рейтинге:</span-->
					<span id="is_hide_in_rating"></span>
				</div>

			</div>
			<!-- tab_main -->

			<div class="tab tab_contacts hide_tab">
				<div class="row">
					phones
					<div id="phones"></div>
				</div>

				<div class="row">
					email
					<div id="email"></div>
				</div>

				<div class="row">
					url
					<div id="url"></div>
				</div>

				<div class="row">
					<div id="responsibles">
					</div>
				</div>
			</div>
			<!-- tab_contacts -->

			<div class="tab tab_requisites hide_tab">
				<div class="row">
					<div id="requisites">
					</div>
				</div>
			</div>
			<!-- tab_requisites -->
		</div>
	</div>

	<!-- Шаблон блока "Ответственные лица" -->
	<div id="template_responsible" style="width: 600px;">
		<div class="row" style="background-color: #cccccc; padding: 2px; border-radius: 5px;">
			<div style="width: 200px;">
				Фамилия:
				<div id="second_name"></div>
			</div>
			<div style="width: 200px;">
				Имя:
				<div id="first_name"></div>
			</div>
			<div style="width: 200px;">
				Отчество:
				<div id="middle_name"></div>
			</div>
			<div style="width: 200px;">
				Должность:
				<div id="position"></div>
			</div>
			<div style="width: 200px;">
				Пароль:
				<div id="password"></div>
			</div>
			<div style="width: 200px;">
				<div id="is_responsible"></div>
			</div>
			<div class="row">
				<div id="contacts">
				</div>
			</div>
		</div>
	</div>

	<!-- Шаблон блока "Контакты" -->
	<div id="template_contact" style="width: 400px;">
		<div class="row" style="background-color: #aaaaaa; border-radius: 5px; padding: 2px; margin-top: 2px;">
			<div style="width: 220px;">
				<span id="field_label">field_label</span>: <span id="field_value">field_value</span> <br><span
					id="send_allow">send_allow</span>
			</div>
		</div>
	</div>

	<!-- Шаблон блока "Реквизиты" -->
	<div id="template_requisite" style="width: 600px;">
		<div class="row" style="background-color: #eeeeee; border-radius: 5px;">
			<div style="width: 200px;">
				<span>Полное наименование:</span>

				<div id="full_name"></div>
			</div>
			<div style="width: 200px;">
				Юридический адрес:
				<div id="address_juridical"></div>
			</div>
			<div style="width: 200px;">
				Фактический адрес:
				<div id="address_fact"></div>
			</div>
			<div style="width: 200px;">
				ИНН:
				<div id="inn"></div>
			</div>
			<div style="width: 200px;">
				КПП:
				<div id="kpp"></div>
			</div>
			<div style="width: 200px;">
				ОГРН:
				<div id="ogrn"></div>
			</div>
			<div style="width: 300px;">
				Банк:
				<div id="bank"></div>
			</div>
			<div style="width: 300px;">
				БИК:
				<div id="bik"></div>
			</div>
			<div style="width: 300px;">
				Расчетный счет:
				<div id="r_schet"></div>
			</div>
			<div style="width: 300px;">
				Корреспондентский счет:
				<div id="kor_schet"></div>
			</div>
		</div>
	</div>
</div>

<div id="gridContainer"></div>
<script type="text/javascript">
	$(document).ready(function () {
		var clients = <?= CJSON::encode($clients) ?>;
		//console.log('[clients]',clients);
		$("#gridContainer").dxDataGrid({
			dataSource: clients,
			paging: {
				pageSize: 20
			},
			pager: {
				showPageSizeSelector: true,
				allowedPageSizes: [5, 10, 20]
			},
			columns: [
				{
					dataField: 'id',
					caption: "ID",
					width: 50
				}, {
					caption: 'Name',
					dataField: 'header',
					cellTemplate: function (container, options) {
						$('<span/>')
							.html('<a href="<?= $this->createUrl('/clients/clients/addedit', array('client_id' => '')); ?>' + options.data.id + '">' + options.value + '</a>')
							.appendTo(container);
					}
				},
				'email',
				'phones',
				'description',
				{
					dataField: 'groups',
					caption: 'Groups',
					cellTemplate: function (container, options) {
						$('<span/>')
							.html(function () {
								var str = [];
								for (var index in options.data.groups) {
									var x = options.data.groups[index];
									str[index] = x.group;
								}
								return '<a href="<?= $this->createUrl('/groups/groups/list'); ?>">' + str.join(', ') + '</a>';
							})
							.appendTo(container);
					}
				},
				{
					dataField: 'dt_create',
					caption: 'Date create'
				},
			],
			masterDetail: {
				enabled: true,
				template: function (container, options) {
					container.addClass("internal-grid-container");
					var z = add_client(options.data);
					var tag = $('<div>').addClass("internal-grid");
					tag.append(z);
					container.append(z);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	function show(obj, name) {
		var x = $(obj).closest('[id^=client_]');
		x.find('.tab').addClass('hide_tab');
		x.find('.tab_' + name).removeClass('hide_tab');
	}

	function add_client(data) {
		console.log('[CLIENT] >', data);
		var
			client = $('#template_client').clone().attr({'id': 'client_' + data.id}),
			index;

		client.find('#type').replaceWith(data.type);
		client.find('#password').replaceWith(data.password);
		client.find('#header').replaceWith(data.header);
		client.find('#manager').replaceWith(data.manager);
		client.find('#status').replaceWith(data.status);
		client.find('#groups').replaceWith(data.groups);
		client.find('#description').replaceWith(data.description);
		if (data.is_hide_in_rating === false) {
			data.is_hide_in_rating = 'Показывать';
			data.is_hide_in_rating = $('<div>').dxCheckBox({text: 'Скрыть в рейтинге', value: false, disabled: true});
		} else {
			data.is_hide_in_rating = 'Не показывать';
			data.is_hide_in_rating = $('<div>').dxCheckBox({text: 'Скрыть в рейтинге', value: true, disabled: true});
		}
		client.find('#is_hide_in_rating').replaceWith(data.is_hide_in_rating);
		client.find('#phones').replaceWith(data.phones);
		client.find('#email').replaceWith(data.email);
		client.find('#url').replaceWith(data.url);

		for (index in data.responsibles) {
			client.find("#responsibles").append(add_responsible(data.responsibles[index]));
		}

		for (index in data.requisites) {
			client.find("#requisites").append(add_requisite(data.requisites[index]));
		}
		return client;
	}

	function add_responsible(data) {
		console.log('[RESPONSIBLE] >', data);
		var
			responsible = $('#template_responsible').clone().attr({'id': 'responsible_' + data.id}),
			index;
		responsible.find('#first_name').replaceWith(data.first_name);
		responsible.find('#middle_name').replaceWith(data.middle_name);
		responsible.find('#second_name').replaceWith(data.second_name);
		responsible.find('#password').replaceWith(data.password);
		responsible.find('#position').replaceWith(data.position);
		if (data.is_responsible === false) {
			data.is_responsible = $('<div>').dxCheckBox({text: 'Ответственное лицо', value: false, disabled: true});
		} else {
			data.is_responsible = $('<div>').dxCheckBox({text: 'Ответственное лицо', value: true, disabled: true});
		}
		responsible.find('#is_responsible').replaceWith(data.is_responsible);

		for (index in data.contacts) {
			responsible.find("#contacts").append(add_contact(data.contacts[index]));
		}

		return responsible;
	}

	function add_contact(data) {
		var contact = $('#template_contact').clone().attr({'id': 'contact_' + data.id});
		console.log('[CONTACT]', data);
		contact.find('#field_label').replaceWith(data.field_label);
		contact.find('#field_value').replaceWith(data.field_value);
		if (data.field_ref === 2 || data.field_ref === 6) {
			if (data.send_allow === false) {
				data.send_allow = $('<div>').dxCheckBox({
					text: 'Принимать рассылки и уведомления',
					value: false,
					disabled: true
				});
			} else {
				data.send_allow = $('<div>').dxCheckBox({
					text: 'Принимать рассылки и уведомления',
					value: true,
					disabled: true
				});
			}
			contact.find('#send_allow').replaceWith(data.send_allow);
		}
		else{
			contact.find('#send_allow').remove();
		}
		return contact;
	}

	function add_requisite(data) {
		var requisite = $('#template_requisite').clone().attr({'id': 'requisite_' + data.id});
		console.log(requisite);
		requisite.find('#full_name').replaceWith(data.full_name);
		requisite.find('#address_juridical').replaceWith(data.address_juridical);
		requisite.find('#address_fact').replaceWith(data.address_fact);
		requisite.find('#inn').replaceWith(data.inn);
		requisite.find('#kpp').replaceWith(data.kpp);
		requisite.find('#ogrn').replaceWith(data.ogrn);
		requisite.find('#bank').replaceWith(data.bank);
		requisite.find('#bik').replaceWith(data.bik);
		requisite.find('#r_schet').replaceWith(data.r_schet);
		requisite.find('#kor_schet').replaceWith(data.kor_schet);
		return requisite;
	}
</script>

<!--div id="gridContainerGroup"></div>
<script type="text/javascript">
	$(document).ready(function () {
		var clients_groups = ;

		var dataGrid = $("#gridContainerGroup").dxDataGrid({
			dataSource: clients_groups,
			allowColumnReordering: true,
			grouping: {
				autoExpandAll: true
			},
			searchPanel: {
				visible: true
			},
			paging: {
				pageSize: 20
			},
			groupPanel: {
				visible: true
			},
			columns: [
				{
					dataField: 'id',
					caption: 'ID',
					width: 50
				},
				'name',
				'email',
				'phone',
				'description',
				'dt_create',
				{
					dataField: 'group_name',
					groupIndex: 0
				}
			]
		}).dxDataGrid("instance");

		$("#autoExpand").dxCheckBox({
			value: true,
			onValueChanged: function (data) {
				dataGrid.option('grouping.autoExpandAll', data.value);
			}
		});
	});
</script-->
