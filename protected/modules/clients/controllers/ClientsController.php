<?php

class ClientsController extends Controller
{
	public $defaultAction = 'list';

	public function filters() {
		//return array( 'accessControl' );
	}

	public function filterAccessControl() {
		return array(
			array('allow', // allow authenticated users to access all actions
				'actions' => array('index'),
				'users' => array('@')
			),
			array('deny',
				'users' => array('*')
			),
		);
	}

	public function actionIndex() {
		/*
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}
		*/

		$this->render('index');
	}

	public function actionAddEdit($client_id = 0) {
		/*
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}
		*/
		$data = array();
		$formName = 'sss';
		$data['formName'] = $formName;
		$client_id = (int)$client_id;

		//взять группы пользователя
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = AppUserGroups::model()->findAll($criteria);
		$arr = array();
		foreach ($groups as $key_groups => $group) {
			$arr[$key_groups] = $group->attributes;
		}
		$data['user_groups'] = $arr;

		//берем все данные о клиенте
		$oClient = AppClients::model()
			->with(
				array(
					'aclUserRef.appUserGroups.appGroupHasProperties.appPropertyRef.appPropertyTypeRef',
					'aclUserRef.appUserGroups.appGroupHasProperties.appPropertyRef.appClientPropertyValues',
					'appResponsibles.appContactFieldValues',
					'appRequisites',
				)
			)
			->findByPk($client_id);
		//M::printr($oClient, '$oClient');

		$props = array(
			'groups' => array(),
			'properties' => array(),
			'types' => array(),
			'values' => array(),
		);

		if (isset($oClient->aclUserRef)) {
			if (isset($oClient->aclUserRef->appUserGroups) && !empty($oClient->aclUserRef->appUserGroups)) {
				foreach ($oClient->aclUserRef->appUserGroups as $key_group => $group) {
					//M::printr($group, '$group');
					$props['groups'][$group->id] = $group->attributes;
					foreach ($group->appGroupHasProperties as $key_propertyGroup => $property) {
						//M::printr($property->appPropertyRef, '$property->appPropertyRef');
						$props['groups'][$group->id]['properties'][] = $property->app_property_ref;
						//$property->appPropertyRef->attributes;
						$props['properties'][$property->app_property_ref] = $property->appPropertyRef->attributes;
						$props['types'][$property->appPropertyRef->app_property_type_ref] = $property->appPropertyRef->appPropertyTypeRef->attributes;
						if (!empty($property->appPropertyRef->appClientPropertyValues)) {
							$props['values'][$property->app_property_ref] = $property->appPropertyRef->appClientPropertyValues[0]->attributes;
						}else{
							$props['values'][$property->app_property_ref] = array();
						}
					}
				}
			}
		}
		//M::printr($props, '$props');

		$data['props'] = $props;
		//M::printr($data, '$data');

		if (!empty($oClient)) {
			//берем официальные лица в массив
			$responsibles = array();
			foreach ($oClient->appResponsibles as $key_responsible => $responsible) {
				$responsibles[$key_responsible] = $responsible->attributes;
				//берем контакты официальных лиц
				$responsibles[$key_responsible]['contacts'] = array();
				foreach ($responsible->appContactFieldValues as $key_contact => $contact) {
					$responsibles[$key_responsible]['contacts'][$key_contact] = $contact->attributes;
				}
			}
			//берем реквизиты
			$requisites = array();
			foreach ($oClient->appRequisites as $key_requisite => $requisite) {
				$requisites[$key_requisite] = $requisite->attributes;
			}
			//берем группы клиента
			$client_groups = array();
			foreach ($oClient->appClientHasGroups as $key_client_group => $client_group) {
				$client_groups[$key_client_group] = $client_group->app_user_group_ref;
			}
			//берем данные клиента в массив
			$client = $oClient->attributes;
		} else {
			$client = array();
			$responsibles = array();
			$requisites = array();
			$client_groups = array();
		}
		$data['client'] = $client;
		$data['responsibles'] = $responsibles;
		$data['requisites'] = $requisites;
		$data['client_groups'] = $client_groups;

		//взять группы клиента
		$oClientGroups = new AppClientHasGroups();
		$clientGroups = $oClientGroups->findAllByAttributes(
			array('app_client_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);

		//взять типы доп. полей
		$oFields = new AppContactFields();
		$fields = $oFields->findAll(array('order' => 'field_label ASC'));
		$data['fields'] = $fields;
		$data['jsFields'] = CJSON::encode($fields);

		//взять доп. поля клиента
		$oFieldValues = new AppContactFieldValues();
		$fieldValues = $oFieldValues->findAllByAttributes(
			array('app_resp_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);
		$data['fieldValues'] = $fieldValues;
		$data['jsFieldValues'] = CJSON::encode($fieldValues);
		//если сохранение данных
		if (!empty($_POST)) {
			$post = $_POST;
			//M::printr($post, '$post');
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$oClient = AppClients::model()->findByPk((int)$client_id);
				//если такой не найден ($client_id = 0)
				if (empty($oClient)) {
					//новый клиент
					$oClient = new AppClients();
					$oClient->dt_create = new CDbExpression('NOW()');
					$oClient->acl_user_ref = Yii::app()->user->id;
					//throw new Exception('Exception: index `id` = ' . (int)$client_id . ' not found in table AppClients');
				}
				$oClient->attributes = $post;
				$oClient->is_hide_in_rating = $post['is_hide_in_rating'] === 'true' ? true : false;
				$oClient->dt_update = new CDbExpression('NOW()');
				if (!$oClient->save()) {
					//M::printr($oCli);
					throw new Exception('Exception: error save data in table AppClients');
				}

				//сохраняем группы
				if (isset($post['groups']) || !empty($post['groups'])) {
					$criteria = new CDbCriteria();
					$criteria->addCondition('app_client_ref = ' . $oClient->id);
					AppClientHasGroups::model()->deleteAll($criteria);
					foreach ($post['groups'] as $key_group => $group) {
						$oGroup = new AppClientHasGroups();
						$oGroup->app_client_ref = $oClient->id;
						$oGroup->app_user_group_ref = $group;
						if (!$oGroup->save()) {
							throw new Exception('Exception: error save data in table AppClientHasGroups');
						}
					}
				} else {
					throw new Exception('Exception: error save data in table AppClientHasGroups');
				}

				//сохраняем ответственных лиц
				if (isset($post['responsibles'])) {
					$id_responsibles = array();
					foreach ($post['responsibles'] as $key_resp => $responsible) {
						//проверяем, есть ли такой id в базе
						if ((int)$responsible['id'] !== 0) {
							//если id != 0, значит есть, взять его из базы
							$oResponsible = AppResponsibles::model()->findByPk((int)$responsible['id']);
							if (empty($oResponsible)) {
								throw new Exception('Exception: index `id` = ' . (int)$responsible['id'] . ' not found in table AppResponsibles');
							}
						} else {
							//если id = 0, значит новый, создать объект
							$oResponsible = new AppResponsibles();
						}
						$oResponsible->attributes = $responsible;
						$oResponsible->is_responsible = $responsible['is_responsible'] === 'true' ? true : false;
						$oResponsible->app_client_ref = $oClient->id;
						if (!$oResponsible->save()) {
							throw new Exception('Exception: error save data in table AppResponsibles');
						}
						//сохранение контактных данных
						if (isset($responsible['contacts'])) {
							$id_contacts = array();
							foreach ($responsible['contacts'] as $key_contact => $contact) {
								if ((int)$contact['id'] !== 0) {
									$oContact = AppContactFieldValues::model()->findByPk((int)$contact['id']);
									if (empty($oContact)) {
										throw new Exception('Exception: index `id` = ' . (int)$contact['id'] . ' not found in table AppContactFieldValues');
									}
								} else {
									$oContact = new AppContactFieldValues();
								}
								$oContact->attributes = $contact;
								$oContact->send_allow = in_array($contact['field_ref'], array(2, 6)) ? ($contact['send_allow'] === 'true' ? true : false) : false;
								$oContact->app_resp_ref = $oResponsible->id;
								if (!$oContact->save()) {
									throw new Exception('Exception: error save data in table AppContactFieldValues');
								}
								//получить список тех, которые будут сохранены (отличных от нуля)
								$id_contacts[] = $oContact->id;
							} //each $responsible['contacts']

							//удалить контакты, отсутствующие в списке сохраненных
							$criteria = new CDbCriteria();
							$criteria->addNotInCondition('id', $id_contacts);
							$criteria->addCondition('app_resp_ref = ' . $oResponsible->id);
							AppContactFieldValues::model()->deleteAll($criteria);

						} //контактные данные
						//получить список тех, которые будут сохранены (отличных от нуля)
						$id_responsibles[] = $oResponsible->id;
					}//each $post['responsibles']

					//удалить контакты, отсутствующие в списке сохраненных
					$criteria = new CDbCriteria();
					$criteria->addNotInCondition('id', $id_responsibles);
					$criteria->addCondition('app_client_ref = ' . $oClient->id);
					AppResponsibles::model()->deleteAll($criteria);
				} //ответственные лица

				//сохраняем реквизиты
				if (isset($post['requisites'])) {
					$id_requisites = array();
					foreach ($post['requisites'] as $key_requisites => $requisite) {
						if ((int)$requisite['id'] !== 0) {
							$oRequisite = AppRequisites::model()->findByPk((int)$requisite['id']);
							if (empty($oRequisite)) {
								throw new Exception('Exception: index `id` = ' . (int)$requisite['id'] . ' not found in table AppRequisites');
							}
						} else {
							$oRequisite = new AppRequisites();
						}
						$oRequisite->attributes = $requisite;
						$oRequisite->app_client_ref = $oClient->id;
						if (!$oRequisite->save()) {
							throw new Exception('Exception: error save data in table AppRequisites');
						}
						//получить список тех, которые будут сохранены (отличных от нуля)
						$id_requisites[] = $oRequisite->id;
					} //each $post['requisites']
					//удалить контакты, отсутствующие в списке сохраненных
					$criteria = new CDbCriteria();
					$criteria->addNotInCondition('id', $id_requisites);
					$criteria->addCondition('app_client_ref = ' . $oClient->id);
					AppRequisites::model()->deleteAll($criteria);
				} //реквизиты

				//проверить, есть ли у пользователя ссылка
				//добавить линку пользователя
				$criteria = new CDbCriteria();
				$criteria->addCondition('app_client_ref = ' . $oClient->id);
				$oLink = AppLinks::model()->find($criteria);
				if (empty($oLink)) {
					$oLink = new AppLinks();
					$oLink->link = "{$_SERVER['SERVER_NAME']}/?ref={$oClient->id}";
					$oLink->referer_condition_ref = 1;
					$oLink->referal_condition_ref = 1;
					$oLink->app_client_ref = $oClient->id;
					if (!$oLink->save()) {
						throw new Exception('Exception: error save data in table AppLinks');
					}
				}

				$JS['errors'] = false;
				$errors['errors'] = false;

				$transaction->commit();
			} catch (Exception $e) {
				$JS['errors'] = true;
				$JS['error'] = $e->getMessage();

				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				$transaction->rollBack();
			} //try

			//вывод сообщения об ошибке в ajax
			if (Yii::app()->request->isAjaxRequest) {
				print CJSON::encode($JS);
				exit;
			}
			M::printr($e->getMessage(), 'EXCEPTION');
		} //if (!empty($_POST))

		$this->render('addedit', $data);
	}

	public function actionGetProperties() {

	}

	public function actionList() {
		/*
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}
		*/

		$data = array();

		//взять группы пользователя
		$oClientGroups = new AppUserGroups();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$oClientGroups = AppUserGroups::model()->findAll($criteria);
		if (empty($oClientGroups)) {
			$oClientGroups = array();
		}
		$groups = array();
		foreach ($oClientGroups as $key => $group) {
			$groups[$group['id']] = $group->attributes;
		}
		//M::printr($groups, '$groups');
		if (empty($groups)) {
			//перекинуть на страницу создания группы
			$this->redirect(Yii::app()->createUrl('/groups/groups/add'));
		}
		$data['groups'] = $groups;
		//берем все данные о клиенте
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$oClients = AppClients::model()
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
					'appResponsibles.appContactFieldValues.fieldRef',
					'appRequisites',
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findAll($criteria);
		//M::printr($oClients, '$oClients');

		//берем данные клиента
		$clients = array();
		if (!empty($oClients)) {
			foreach ($oClients as $key_client => $client) {
				$clients[$key_client] = $client->attributes;

				//берем ответственные лица
				$clients[$key_client]['responsibles'] = array();
				foreach ($client->appResponsibles as $key_responsible => $responsible) {
					$clients[$key_client]['responsibles'][$key_responsible] = $responsible->attributes;

					//берем контакты ответственных лиц
					$clients[$key_client]['responsibles'][$key_responsible]['contacts'] = array();
					foreach ($responsible->appContactFieldValues as $key_contact => $contact) {
						$clients[$key_client]['responsibles'][$key_responsible]['contacts'][$key_contact] = $contact->attributes;
						$clients[$key_client]['responsibles'][$key_responsible]['contacts'][$key_contact]['field_label'] = $contact->fieldRef['field_label'];
					}
				}

				//берем реквизиты
				$clients[$key_client]['requisites'] = array();
				foreach ($client->appRequisites as $key_requisite => $requisite) {
					$clients[$key_client]['requisites'][$key_requisite] = $requisite->attributes;
				}

				//берем группы клиента
				$clients[$key_client]['groups'] = array();
				foreach ($client->appClientHasGroups as $key_group => $group) {
					//$clients[$key_client]['groups'][$key_group] = $group->app_user_group_ref;
					$clients[$key_client]['groups'][$key_group] = $group->attributes;
					$clients[$key_client]['groups'][$key_group]['group'] = $group->appUserGroupRef['group'];
				}
			}
		}
		//M::printr($clients, '$clients');
		$data['clients'] = $clients;
		$this->render('list', $data);

		exit;;
	}

	public function actionView($client_id = 0) {
		$data = array();
		$formName = 'sss';

		//взять группы пользователя
		$oClientGroups = new AppUserGroups();
		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . (int)Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = $oClientGroups->findAll($criteria);
		$arr = array();
		foreach ($groups as $key_groups => $group) {
			$arr[$key_groups] = $group->attributes;
		}
		$data['user_groups'] = $arr;

		//берем все данные о клиенте
		$oClients = new AppClients();
		$clients = $oClients
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
					'appRequisites',
					'appClientHasGroups',
					'appClientHasGroups.appUserGroupRef'
				)
			)
			->findByPk($client_id);

		//берем официальные лица в массив
		$responsibles = array();
		foreach ($clients->appResponsibles as $key_responsible => $responsible) {
			$responsibles[$key_responsible] = $responsible->attributes;
			//берем контакты официальных лиц
			$responsibles[$key_responsible]['contacts'] = array();
			foreach ($responsible->appContactFieldValues as $key_contact => $contact) {
				$responsibles[$key_responsible]['contacts'][$key_contact] = $contact->attributes;
			}
		}

		//берем реквизиты
		$requisites = array();
		foreach ($clients->appRequisites as $key_requisite => $requisite) {
			$requisites[$key_requisite] = $requisite->attributes;
		}

		//берем группы клиента
		$client_groups = array();
		foreach ($clients->appClientHasGroups as $key_client_group => $client_group) {
			$client_groups[$key_client_group] = $client_group->app_user_group_ref;
		}
		//берем данные клиента в массив
		$client = $clients->attributes;
		$client['responsibles'] = $responsibles;
		$client['requisites'] = $requisites;
		$client['client_groups'] = $client_groups;
		$data['client'] = $client;

		$data['responsibles'] = $responsibles;
		$data['requisites'] = $requisites;
		$data['client_groups'] = $client_groups;

		//взять группы клиента
		$oClientGroups = new AppClientHasGroups();
		$clientGroups = $oClientGroups->findAllByAttributes(
			array('app_client_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);

		//взять типы доп. полей
		$oFields = new AppContactFields();
		$fields = $oFields->findAll(array('order' => 'field_label ASC'));
		$data['fields'] = $fields;
		$data['jsFields'] = CJSON::encode($fields);

		//взять доп. поля клиента
		$oFieldValues = new AppContactFieldValues();
		$fieldValues = $oFieldValues->findAllByAttributes(
			array('app_resp_ref' => (int)$client_id),
			array('order' => 'id ASC')
		);
		$data['fieldValues'] = $fieldValues;
		$data['jsFieldValues'] = CJSON::encode($fieldValues);

		$this->render('view', $data);
	}

}