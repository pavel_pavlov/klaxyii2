<?php

/**
 * This is the model class for table "app_referers".
 *
 * The followings are the available columns in table 'app_referers':
 * @property string $id
 * @property string $app_promo_ref
 * @property string $app_client_ref
 *
 * The followings are the available model relations:
 * @property AppClients $appClientRef
 * @property AppPromos $appPromoRef
 */
class BaseAppReferers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_referers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_promo_ref, app_client_ref', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_promo_ref, app_client_ref', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appClientRef' => array(self::BELONGS_TO, 'AppClients', 'app_client_ref'),
			'appPromoRef' => array(self::BELONGS_TO, 'AppPromos', 'app_promo_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_promo_ref' => 'App Promo Ref',
			'app_client_ref' => 'App Client Ref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('app_promo_ref',$this->app_promo_ref,true);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppReferers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
