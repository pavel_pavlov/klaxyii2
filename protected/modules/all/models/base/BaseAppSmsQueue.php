<?php

/**
 * This is the model class for table "app_sms_queue".
 *
 * The followings are the available columns in table 'app_sms_queue':
 * @property string $id
 * @property string $app_sms_messages_ref
 * @property boolean $is_sended
 * @property boolean $is_delivered
 * @property string $app_client_ref
 * @property string $dt_sended
 * @property string $dt_delivered
 * @property string $message_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property AppSmsMessages $appSmsMessagesRef
 */
class BaseAppSmsQueue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_sms_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_sms_messages_ref, app_client_ref', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('message_id', 'length', 'max'=>255),
			array('is_sended, is_delivered, dt_sended, dt_delivered', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_sms_messages_ref, is_sended, is_delivered, app_client_ref, dt_sended, dt_delivered, message_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appSmsMessagesRef' => array(self::BELONGS_TO, 'AppSmsMessages', 'app_sms_messages_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_sms_messages_ref' => 'App Sms Messages Ref',
			'is_sended' => 'Is Sended',
			'is_delivered' => 'Is Delivered',
			'app_client_ref' => 'App Client Ref',
			'dt_sended' => 'Dt Sended',
			'dt_delivered' => 'Dt Delivered',
			'message_id' => 'Message',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('app_sms_messages_ref',$this->app_sms_messages_ref,true);
		$criteria->compare('is_sended',$this->is_sended);
		$criteria->compare('is_delivered',$this->is_delivered);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);
		$criteria->compare('dt_sended',$this->dt_sended,true);
		$criteria->compare('dt_delivered',$this->dt_delivered,true);
		$criteria->compare('message_id',$this->message_id,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppSmsQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
