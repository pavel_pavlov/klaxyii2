<?php

/**
 * This is the model class for table "app_questions".
 *
 * The followings are the available columns in table 'app_questions':
 * @property string $id
 * @property string $app_client_ref
 * @property integer $req1
 * @property integer $req2
 * @property integer $req3
 * @property integer $req4
 * @property integer $req5
 * @property integer $req6
 * @property integer $req7
 * @property integer $req0
 *
 * The followings are the available model relations:
 * @property AppClients $appClientRef
 */
class BaseAppQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('req1, req2, req3, req4, req5, req6, req7, req0', 'numerical', 'integerOnly'=>true),
			array('app_client_ref', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_client_ref, req1, req2, req3, req4, req5, req6, req7, req0', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appClientRef' => array(self::BELONGS_TO, 'AppClients', 'app_client_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_client_ref' => 'App Client Ref',
			'req1' => 'Req1',
			'req2' => 'Req2',
			'req3' => 'Req3',
			'req4' => 'Req4',
			'req5' => 'Req5',
			'req6' => 'Req6',
			'req7' => 'Req7',
			'req0' => 'Req0',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);
		$criteria->compare('req1',$this->req1);
		$criteria->compare('req2',$this->req2);
		$criteria->compare('req3',$this->req3);
		$criteria->compare('req4',$this->req4);
		$criteria->compare('req5',$this->req5);
		$criteria->compare('req6',$this->req6);
		$criteria->compare('req7',$this->req7);
		$criteria->compare('req0',$this->req0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
