<?php

/**
 * This is the model class for table "app_promos".
 *
 * The followings are the available columns in table 'app_promos':
 * @property string $id
 * @property string $promo
 * @property string $acl_user_ref
 * @property string $dt_create
 * @property string $dt_start
 * @property string $dt_finish
 * @property string $dt_activate
 * @property string $referer_condition_ref
 * @property string $referal_condition_ref
 *
 * The followings are the available model relations:
 * @property AclUsers $aclUserRef
 * @property AppReferals[] $appReferals
 * @property AppReferers[] $appReferers
 */
class BaseAppPromos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_promos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('promo, acl_user_ref, dt_create, dt_start', 'required'),
			array('promo', 'length', 'max'=>255),
			array('dt_finish, dt_activate, referer_condition_ref, referal_condition_ref', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, promo, acl_user_ref, dt_create, dt_start, dt_finish, dt_activate, referer_condition_ref, referal_condition_ref', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aclUserRef' => array(self::BELONGS_TO, 'AclUsers', 'acl_user_ref'),
			'appReferals' => array(self::HAS_MANY, 'AppReferals', 'app_promo_ref'),
			'appReferers' => array(self::HAS_MANY, 'AppReferers', 'app_promo_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'promo' => 'Promo',
			'acl_user_ref' => 'Acl User Ref',
			'dt_create' => 'Dt Create',
			'dt_start' => 'Dt Start',
			'dt_finish' => 'Dt Finish',
			'dt_activate' => 'Dt Activate',
			'referer_condition_ref' => 'Referer Condition Ref',
			'referal_condition_ref' => 'Referal Condition Ref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('promo',$this->promo,true);
		$criteria->compare('acl_user_ref',$this->acl_user_ref,true);
		$criteria->compare('dt_create',$this->dt_create,true);
		$criteria->compare('dt_start',$this->dt_start,true);
		$criteria->compare('dt_finish',$this->dt_finish,true);
		$criteria->compare('dt_activate',$this->dt_activate,true);
		$criteria->compare('referer_condition_ref',$this->referer_condition_ref,true);
		$criteria->compare('referal_condition_ref',$this->referal_condition_ref,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppPromos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
