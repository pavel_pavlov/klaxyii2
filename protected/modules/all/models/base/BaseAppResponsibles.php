<?php

/**
 * This is the model class for table "app_responsibles".
 *
 * The followings are the available columns in table 'app_responsibles':
 * @property string $id
 * @property string $password
 * @property string $position
 * @property string $app_client_ref
 * @property boolean $is_responsible
 * @property string $first_name
 * @property string $second_name
 * @property string $middle_name
 *
 * The followings are the available model relations:
 * @property AppContactFieldValues[] $appContactFieldValues
 * @property AppClients $appClientRef
 */
class BaseAppResponsibles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_responsibles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_client_ref', 'required'),
			array('password, position, first_name, second_name, middle_name', 'length', 'max'=>255),
			array('is_responsible', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, password, position, app_client_ref, is_responsible, first_name, second_name, middle_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appContactFieldValues' => array(self::HAS_MANY, 'AppContactFieldValues', 'app_resp_ref'),
			'appClientRef' => array(self::BELONGS_TO, 'AppClients', 'app_client_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'password' => 'Password',
			'position' => 'Position',
			'app_client_ref' => 'App Client Ref',
			'is_responsible' => 'Is Responsible',
			'first_name' => 'First Name',
			'second_name' => 'Second Name',
			'middle_name' => 'Middle Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);
		$criteria->compare('is_responsible',$this->is_responsible);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('second_name',$this->second_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppResponsibles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
