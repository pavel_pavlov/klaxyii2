<?php

/**
 * This is the model class for table "acl_users".
 *
 * The followings are the available columns in table 'acl_users':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $dt_create
 * @property string $dt_update
 * @property boolean $is_deleted
 * @property boolean $is_active
 *
 * The followings are the available model relations:
 * @property AppPromos[] $appPromoses
 * @property AppSmsMessages[] $appSmsMessages
 * @property AppUserGroups[] $appUserGroups
 * @property AppProperties[] $appProperties
 * @property AppClients[] $appClients
 */
class BaseAclUsers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'acl_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password', 'length', 'max'=>255),
			array('dt_create, dt_update, is_deleted, is_active', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, dt_create, dt_update, is_deleted, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appPromoses' => array(self::HAS_MANY, 'AppPromos', 'acl_user_ref'),
			'appSmsMessages' => array(self::HAS_MANY, 'AppSmsMessages', 'acl_users_ref'),
			'appUserGroups' => array(self::HAS_MANY, 'AppUserGroups', 'acl_user_ref'),
			'appProperties' => array(self::HAS_MANY, 'AppProperties', 'acl_user_ref'),
			'appClients' => array(self::HAS_MANY, 'AppClients', 'acl_user_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'dt_create' => 'Dt Create',
			'dt_update' => 'Dt Update',
			'is_deleted' => 'Is Deleted',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('dt_create',$this->dt_create,true);
		$criteria->compare('dt_update',$this->dt_update,true);
		$criteria->compare('is_deleted',$this->is_deleted);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AclUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
