<?php

/**
 * This is the model class for table "app_conditions".
 *
 * The followings are the available columns in table 'app_conditions':
 * @property string $id
 * @property string $name
 * @property string $condition_type_ref
 * @property string $reward_type_ref
 * @property double $value
 *
 * The followings are the available model relations:
 * @property AppLinks[] $appLinks
 * @property AppLinks[] $appLinks1
 * @property AppConditionTypes $conditionTypeRef
 * @property AppRewardTypes $rewardTypeRef
 */
class BaseAppConditions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_conditions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('condition_type_ref, reward_type_ref', 'required'),
			array('value', 'numerical'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, condition_type_ref, reward_type_ref, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appLinks' => array(self::HAS_MANY, 'AppLinks', 'referal_condition_ref'),
			'appLinks1' => array(self::HAS_MANY, 'AppLinks', 'referer_condition_ref'),
			'conditionTypeRef' => array(self::BELONGS_TO, 'AppConditionTypes', 'condition_type_ref'),
			'rewardTypeRef' => array(self::BELONGS_TO, 'AppRewardTypes', 'reward_type_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'condition_type_ref' => 'Condition Type Ref',
			'reward_type_ref' => 'Reward Type Ref',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('condition_type_ref',$this->condition_type_ref,true);
		$criteria->compare('reward_type_ref',$this->reward_type_ref,true);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppConditions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
