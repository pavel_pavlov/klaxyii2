<?php

/**
 * This is the model class for table "app_contact_field_values".
 *
 * The followings are the available columns in table 'app_contact_field_values':
 * @property string $id
 * @property string $app_resp_ref
 * @property string $field_ref
 * @property string $field_value
 * @property boolean $send_allow
 *
 * The followings are the available model relations:
 * @property AppResponsibles $appRespRef
 * @property AppContactFields $fieldRef
 */
class BaseAppContactFieldValues extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_contact_field_values';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_resp_ref, field_ref', 'required'),
			array('field_value', 'length', 'max'=>255),
			array('send_allow', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_resp_ref, field_ref, field_value, send_allow', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appRespRef' => array(self::BELONGS_TO, 'AppResponsibles', 'app_resp_ref'),
			'fieldRef' => array(self::BELONGS_TO, 'AppContactFields', 'field_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_resp_ref' => 'App Resp Ref',
			'field_ref' => 'Field Ref',
			'field_value' => 'Field Value',
			'send_allow' => 'Send Allow',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('app_resp_ref',$this->app_resp_ref,true);
		$criteria->compare('field_ref',$this->field_ref,true);
		$criteria->compare('field_value',$this->field_value,true);
		$criteria->compare('send_allow',$this->send_allow);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppContactFieldValues the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
