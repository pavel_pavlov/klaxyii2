<?php

/**
 * This is the model class for table "app_clients".
 *
 * The followings are the available columns in table 'app_clients':
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $phones
 * @property string $description
 * @property string $dt_create
 * @property string $dt_update
 * @property boolean $is_deleted
 * @property boolean $is_active
 * @property string $acl_user_ref
 * @property string $password
 * @property boolean $sms_allow
 * @property string $first_name
 * @property string $second_name
 * @property string $middle_name
 * @property string $header
 * @property string $manager
 * @property integer $type
 * @property integer $status
 * @property string $url
 * @property string $app_client_ref
 * @property boolean $is_hide_in_rating
 *
 * The followings are the available model relations:
 * @property AppQuestions[] $appQuestions
 * @property AppLinks[] $appLinks
 * @property AppRequisites[] $appRequisites
 * @property AppReferals[] $appReferals
 * @property AppClientHasGroups[] $appClientHasGroups
 * @property AppReferers[] $appReferers
 * @property AppResponsibles[] $appResponsibles
 * @property AppRating[] $appRatings
 * @property AclUsers $aclUserRef
 * @property AppClients $appClientRef
 * @property AppClients[] $appClients
 * @property AppClientPropertyValues[] $appClientPropertyValues
 */
class BaseAppClients extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acl_user_ref', 'required'),
			array('type, status', 'numerical', 'integerOnly'=>true),
			array('name, email, phones, password, first_name, second_name, middle_name, header, manager, url', 'length', 'max'=>255),
			array('description, dt_create, dt_update, is_deleted, is_active, sms_allow, app_client_ref, is_hide_in_rating', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phones, description, dt_create, dt_update, is_deleted, is_active, acl_user_ref, password, sms_allow, first_name, second_name, middle_name, header, manager, type, status, url, app_client_ref, is_hide_in_rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appQuestions' => array(self::HAS_MANY, 'AppQuestions', 'app_client_ref'),
			'appLinks' => array(self::HAS_MANY, 'AppLinks', 'app_client_ref'),
			'appRequisites' => array(self::HAS_MANY, 'AppRequisites', 'app_client_ref'),
			'appReferals' => array(self::HAS_MANY, 'AppReferals', 'app_client_ref'),
			'appClientHasGroups' => array(self::HAS_MANY, 'AppClientHasGroups', 'app_client_ref'),
			'appReferers' => array(self::HAS_MANY, 'AppReferers', 'app_client_ref'),
			'appResponsibles' => array(self::HAS_MANY, 'AppResponsibles', 'app_client_ref'),
			'appRatings' => array(self::HAS_MANY, 'AppRating', 'app_clients_ref'),
			'aclUserRef' => array(self::BELONGS_TO, 'AclUsers', 'acl_user_ref'),
			'appClientRef' => array(self::BELONGS_TO, 'AppClients', 'app_client_ref'),
			'appClients' => array(self::HAS_MANY, 'AppClients', 'app_client_ref'),
			'appClientPropertyValues' => array(self::HAS_MANY, 'AppClientPropertyValues', 'app_client_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'phones' => 'Phones',
			'description' => 'Description',
			'dt_create' => 'Dt Create',
			'dt_update' => 'Dt Update',
			'is_deleted' => 'Is Deleted',
			'is_active' => 'Is Active',
			'acl_user_ref' => 'Acl User Ref',
			'password' => 'Password',
			'sms_allow' => 'Sms Allow',
			'first_name' => 'First Name',
			'second_name' => 'Second Name',
			'middle_name' => 'Middle Name',
			'header' => 'Header',
			'manager' => 'Manager',
			'type' => 'Type',
			'status' => 'Status',
			'url' => 'Url',
			'app_client_ref' => 'App Client Ref',
			'is_hide_in_rating' => 'Is Hide In Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('dt_create',$this->dt_create,true);
		$criteria->compare('dt_update',$this->dt_update,true);
		$criteria->compare('is_deleted',$this->is_deleted);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('acl_user_ref',$this->acl_user_ref,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('sms_allow',$this->sms_allow);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('second_name',$this->second_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('manager',$this->manager,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('status',$this->status);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);
		$criteria->compare('is_hide_in_rating',$this->is_hide_in_rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
