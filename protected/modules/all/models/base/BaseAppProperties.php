<?php

/**
 * This is the model class for table "app_properties".
 *
 * The followings are the available columns in table 'app_properties':
 * @property string $id
 * @property string $name
 * @property string $measure
 * @property string $data
 * @property string $acl_user_ref
 * @property string $app_property_type_ref
 *
 * The followings are the available model relations:
 * @property AclUsers $aclUserRef
 * @property AppPropertyTypes $appPropertyTypeRef
 * @property AppGroupHasProperty[] $appGroupHasProperties
 * @property AppClientPropertyValues[] $appClientPropertyValues
 */
class BaseAppProperties extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_properties';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, acl_user_ref, app_property_type_ref', 'required'),
			array('name, measure', 'length', 'max'=>255),
			array('data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, measure, data, acl_user_ref, app_property_type_ref', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aclUserRef' => array(self::BELONGS_TO, 'AclUsers', 'acl_user_ref'),
			'appPropertyTypeRef' => array(self::BELONGS_TO, 'AppPropertyTypes', 'app_property_type_ref'),
			'appGroupHasProperties' => array(self::HAS_MANY, 'AppGroupHasProperty', 'app_property_ref'),
			'appClientPropertyValues' => array(self::HAS_MANY, 'AppClientPropertyValues', 'app_property_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'measure' => 'Measure',
			'data' => 'Data',
			'acl_user_ref' => 'Acl User Ref',
			'app_property_type_ref' => 'App Property Type Ref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('measure',$this->measure,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('acl_user_ref',$this->acl_user_ref,true);
		$criteria->compare('app_property_type_ref',$this->app_property_type_ref,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppProperties the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
