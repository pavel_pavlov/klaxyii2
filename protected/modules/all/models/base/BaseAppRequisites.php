<?php

/**
 * This is the model class for table "app_requisites".
 *
 * The followings are the available columns in table 'app_requisites':
 * @property string $id
 * @property string $app_client_ref
 * @property string $full_name
 * @property string $address_juridical
 * @property string $address_fact
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $bank
 * @property string $bik
 * @property string $r_schet
 * @property string $kor_schet
 *
 * The followings are the available model relations:
 * @property AppClients $appClientRef
 */
class BaseAppRequisites extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_requisites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_client_ref', 'required'),
			array('full_name, address_juridical, address_fact, inn, kpp, ogrn, bank, bik, r_schet, kor_schet', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_client_ref, full_name, address_juridical, address_fact, inn, kpp, ogrn, bank, bik, r_schet, kor_schet', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appClientRef' => array(self::BELONGS_TO, 'AppClients', 'app_client_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_client_ref' => 'App Client Ref',
			'full_name' => 'Full Name',
			'address_juridical' => 'Address Juridical',
			'address_fact' => 'Address Fact',
			'inn' => 'Inn',
			'kpp' => 'Kpp',
			'ogrn' => 'Ogrn',
			'bank' => 'Bank',
			'bik' => 'Bik',
			'r_schet' => 'R Schet',
			'kor_schet' => 'Kor Schet',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('app_client_ref',$this->app_client_ref,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('address_juridical',$this->address_juridical,true);
		$criteria->compare('address_fact',$this->address_fact,true);
		$criteria->compare('inn',$this->inn,true);
		$criteria->compare('kpp',$this->kpp,true);
		$criteria->compare('ogrn',$this->ogrn,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('bik',$this->bik,true);
		$criteria->compare('r_schet',$this->r_schet,true);
		$criteria->compare('kor_schet',$this->kor_schet,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppRequisites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
