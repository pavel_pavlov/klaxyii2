<?php

/**
 * This is the model class for table "app_sms_messages".
 *
 * The followings are the available columns in table 'app_sms_messages':
 * @property string $id
 * @property string $sms_text
 * @property string $dt_create
 * @property string $acl_users_ref
 * @property string $app_user_group_ref
 * @property string $title
 *
 * The followings are the available model relations:
 * @property AclUsers $aclUsersRef
 * @property AppUserGroups $appUserGroupRef
 * @property AppSmsQueue[] $appSmsQueues
 */
class BaseAppSmsMessages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_sms_messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sms_text, acl_users_ref, title', 'required'),
			array('sms_text, title', 'length', 'max'=>255),
			array('dt_create, app_user_group_ref', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sms_text, dt_create, acl_users_ref, app_user_group_ref, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aclUsersRef' => array(self::BELONGS_TO, 'AclUsers', 'acl_users_ref'),
			'appUserGroupRef' => array(self::BELONGS_TO, 'AppUserGroups', 'app_user_group_ref'),
			'appSmsQueues' => array(self::HAS_MANY, 'AppSmsQueue', 'app_sms_messages_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sms_text' => 'Sms Text',
			'dt_create' => 'Dt Create',
			'acl_users_ref' => 'Acl Users Ref',
			'app_user_group_ref' => 'App User Group Ref',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sms_text',$this->sms_text,true);
		$criteria->compare('dt_create',$this->dt_create,true);
		$criteria->compare('acl_users_ref',$this->acl_users_ref,true);
		$criteria->compare('app_user_group_ref',$this->app_user_group_ref,true);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppSmsMessages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
