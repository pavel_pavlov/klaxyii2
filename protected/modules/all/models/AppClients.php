<?php

/**
 * Class AppClients
 * Класс, расширяемый от BaseAppClients
 */
class AppClients extends BaseAppClients
{
	/**
	 * возвращает
	 *
	 * @param array $id
	 */
	public function getClientsById($ids = array())
	{
		if (!is_array($ids)) {
			$ids = array($ids);
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition("id", $ids);;
		$criteria->order = 'id ASC';
		$result = $this->findAll($criteria);
		return $result;

	}


}