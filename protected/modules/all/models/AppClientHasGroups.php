<?php

/**
 * Class AppClientHasGroups
 * Расширение класса BaseAppClientHasGroups
 */
class AppClientHasGroups extends BaseAppClientHasGroups
{

	protected function _getSelect()
	{
		return Yii::app()->db->createCommand()
			->select(array(
				'app_clients.id client_id',
				'app_clients.phone',
				'app_client_groups.id group_id',
			))
			->from($this->tableName())
			->leftJoin('app_clients', 'app_client_has_groups.app_client_ref = app_clients.id')
			->leftJoin('app_client_groups', 'app_client_has_groups.app_client_group_ref = app_client_groups.id')
			->where('app_client_groups.acl_user_ref = :id', array('id' => Yii::app()->user->id))
			;
	}

	public function getByGroupIds($id = array()) {

		if (!is_array($id)) {
			$id = array($id);
		}

		// массив нужно привести к типу либо обработать.
		$ids = implode(',', array_map('intval', $id));

		$select = $this->_getSelect()
			->andWhere('app_clients.sms_allow IS TRUE')
			->andWhere('app_client_has_groups.app_client_group_ref IN ('. $ids .')')
			;

		$result = $select->queryAll();
		return $result;


// !!! Все внешние значения параметров запроса должны быть забиндены через {criteria} или ->bindParam

// Читаем документацию по QueryBuilder
// http://www.yiiframework.com/doc/guide/1.1/ru/database.query-builder

// Читаем документацию по ActiveRecord
// http://www.yiiframework.com/doc/guide/1.1/ru/database.ar

/// >> Вот такое говнище писать нельзя! >>>


		$query = "
SELECT
	app_clients.id client_id,
	app_clients.phone AS phone,
	app_client_groups.id group_id

FROM
	app_client_has_groups app_client_has_groups

LEFT JOIN
	app_clients app_clients ON app_client_has_groups.app_client_ref = app_clients.id
LEFT JOIN
	app_client_groups app_client_groups ON app_client_has_groups.app_client_group_ref = app_client_groups.id

WHERE
	app_client_has_groups.app_client_group_ref IN (" . implode(', ', $id) . ")
	AND
	app_clients.sms_allow = TRUE
	AND
	app_client_groups.acl_user_ref = " . (int)Yii::app()->user->id . ";
";
		//M::printr($query, '$query');
		$connection = Yii::app()->db;
		$result = $connection->createCommand($query)->query();


		// WTF??!! уже есть готовый метод ->queryAll
		$rows = array();
		while (($row = $result->read()) != false) {
			//M::printr($row, '$row');
			$rows[] = $row;
		}
		M::printr($rows, '$rows');

		//$groups = $this->findAllByAttributes(array('acl_user_ref' => Yii::app()->user->id), array('order' => 'id ASC'));
		return $rows;

	}
}
