<?php

class AppUserGroups extends BaseAppUserGroups
{
	public function asArray($groups = array()){
		$arr = array();
		foreach($groups as $item) {
			$arr[] = $item->attributes;
		}
		return $arr;
	}
}
