<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style type="text/css">
	#gridContainer {
		height: auto;
		width: 100%;
		margin: 50px 0;
	}
</style>
<?php //M::printr(Yii::app()->user, 'Yii::app()->user'); ?>
<div style="width: 60%;">
	<div id="gridContainer"></div>
</div>

<table border="1" style="height: 200px;">
	<tr valign="top">
		<td width="500px" align="left">
			<div id="gridCell"></div>
		</td>
		<td valign="middle" align="center" width="50px">
			<< >>
		</td>
		<td width="500px" align="left">
			<div id="gridCellNew"></div>
		</td>
	</tr>
</table>

<script>
	$("#gridContainer").dxDataGrid({
		dataSource: <?= $properties; ?>,
		filterRow: {
			visible: true,
			applyFilter: "auto"
		},
		searchPanel: {
			visible: true,
			width: 240,
			placeholder: 'Search...'
		},
		headerFilter: {
			visible: true
		},
		columns: [{
			caption: "ID",
			width: 60,
			dataField: "id"
		}, {
			caption: "Свойство",
			width: 160,
			dataField: 'name'
		}, {
			caption: "Ед. изм.",
			width: 90,
			alignment: 'right',
			dataField: "measure"
		}, {
			caption: 'Тип свойства',
			//cellTemplate: $('#cellTemplate'),
			dataField: "app_property_type_ref"
		}]
	}).dxDataGrid('instance');

	var allCells = <?= $properties; ?>;
	var newCells = [];

	var gridCell = $("#gridCell").dxDataGrid({
		dataSource: allCells,
		filterRow: {
			visible: true,
			applyFilter: "auto"
		},
		selection: {
			mode: 'single'
		},
		searchPanel: {
			visible: true,
			//width: 80,
			placeholder: 'Search...'
		},
		headerFilter: {
			visible: true
		},
		scrolling: {
			mode: 'virtual'
		},
		columns: [
			{
				caption: "Свойство",
				width: 400,
				dataField: 'name',
				sortOrder: 'asc'
			},
			{
				caption: "Ед. изм.",
				width: 100,
				alignment: 'right',
				dataField: "measure"
			}
		],
		onRowClick: function (data) {
			newCells.push(data.data);
			var element = findInObjects2(allCells, 'id', data.data.id);
			allCells.splice(element.index, 1);
			gridCellNew.refresh();
			gridCell.refresh();
		},
		height: 400
	}).dxDataGrid('instance');

	var gridCellNew = $("#gridCellNew").dxDataGrid({
		dataSource: newCells,
		filterRow: {
			visible: true,
			applyFilter: "auto"
		},
		selection: {
			mode: 'single'
		},
		searchPanel: {
			visible: true,
			//width: 200,
			placeholder: 'Search...'
		},
		headerFilter: {
			visible: true
		},
		scrolling: {
			mode: 'virtual'
		},
		columns: [
			{
				caption: "Свойство",
				width: 400,
				dataField: 'name',
				sortOrder: 'asc'
			},
			{
				caption: "Ед. изм.",
				width: 100,
				alignment: 'right',
				dataField: "measure"
			}
		],
		onRowClick: function (data) {
			allCells.push(data.data);
			var element = findInObjects2(newCells, 'id', data.data.id);
			newCells.splice(element.index, 1);
			gridCell.refresh();
			gridCellNew.refresh();
		},
		height: 400
	}).dxDataGrid('instance');
</script>

