<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style>
	.hide {
		display: none;
	}

	.col-md-1 {
		position: relative;
		width: 8.2%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-2 {
		position: relative;
		width: 16.5%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-3 {
		position: relative;
		width: 25%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-4 {
		position: relative;
		width: 33.2%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-5 {
		position: relative;
		width: 41.5%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-6 {
		position: relative;
		width: 50%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-7 {
		position: relative;
		width: 58.2%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-8 {
		position: relative;
		width: 66.5%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-9 {
		position: relative;
		width: 75%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-10 {
		position: relative;
		width: 83.2%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-11 {
		position: relative;
		width: 91.5%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.col-md-12 {
		position: relative;
		width: 99.8%;
		padding: 2px;
		margin: 0px;
		float: left;
	}

	.clearfix {
		clear: both;
	}
</style>

<!-- template -->
<div id="template" style="display: none;">
	<!-- Шаблон notify -->
	<div class="notify-template">
		<div class="inner-notify">
		</div>
	</div>

	<div id="template_item" class='row'>
		<div id="item_name" class="col-md-9"></div>
		<button id="remove_button" class="dx-button">Remove Item</button>
		<div class="clearfix"></div>
	</div>
</div>


<?php
$form = new dxForm($formName);
$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));

?>
<div class="col-md-4">
	<div class="row">
		<?php echo CHtml::label('Название свойства', 'name'); ?>
		<?php $form->dxTextBox(
			'name',
			array(
				'value' => '',
				'placeholder' => 'введите название свойства',
			)
		); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Единицы измерения', 'measure'); ?>
		<?php $form->dxTextBox(
			'measure',
			array(
				'value' => '',
				'placeholder' => 'введите единицы измерения',
			)
		); ?>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<?php echo CHtml::label('Тип свойства', 'app_property_type_ref'); ?>
		<?php $form->dxSelectBox(
			'app_property_type_ref',
			array(
				'placeholder' => 'Выберите тип свойства',
				'items' => $types,
				'displayExpr' => 'name',
				'valueExpr' => 'id',
				'value' => null,
				'onValueChanged' => new CDbExpression('function(data){change_select(data);}'),
			)
		); ?>
	</div>

	<div id="items_block" class="row hide">
		<div id="items" class="row"></div>
		<button id="add_item" class="dx-button" onclick="add_item()">Add Item</button>
	</div>

	<div class="row">
		<button class="dx-button" onClick="save();" style="width: 100px;">
			SAVE
		</button>
	</div>
</div>

<script type="application/javascript">
	var i = 1;
	var is_redirect = false;
	<?= $form->formName ?>.items = {};

	//реакция на изменение значения типа свойства
	function change_select(data) {
		//показать скрытую форму
		console.log('[data]', data);
		if (data.itemData['is_multiple'] === true) {
			$('#items_block').removeClass('hide');
		} else {
			$('#items_block').addClass('hide');
		}
	}

	//добавление нового item`а
	function add_item() {
		var item = {};
		//берем шаблон
		var x = $('#template_item').clone();
		x.attr({'id': 'item_' + i}); //меняем id
		item.div = $('<div>').dxTextBox({
			showClearButton: true,
			placeholder: 'Элемент'
		});
		item.dx = item.div.dxTextBox('instance');

		x.find('div#item_name').attr({'id': ''}).append(item.div);//добавляем поле ввода
		x.find('button#remove_button').attr({'id': ''}).attr({'onclick': 'remove_item(' + i + ')'}); //добавляем событие

		<?= $form->formName ?>.
		items[i] = item;
		console.log(x);
		$('#items').append(x); //выводим на страницу в нужное место
		i++;
	}

	//удаление item`а
	function remove_item(num) {
		delete
		<?= $form->formName; ?>.
		items[num];
		$('#item_' + num).remove();
	}

	//сбор данных для формы
	function collect_data() {
		var dataForm = {};
		console.log(<?= $form->formName ?>);
		//сбор основных данных клиента
		dataForm.name =
		<?= $form->formName ?>.
		name.option('value');
		dataForm.measure =
		<?= $form->formName ?>.
		measure.option('value');
		dataForm.app_property_type_ref =
		<?= $form->formName ?>.
		app_property_type_ref.option('value');
		dataForm.items = {};
		for (var item in <?= $form->formName ?>.
		items
	)
		{
			dataForm.items[item] =
			<?= $form->formName ?>.
			items[item].dx.option('value');
		}
		console.log('[dataForm]', dataForm);
		return dataForm;
	}

	//редирект
	function redirect(url) {
		window.location = url;
	}

	//вывод уведомления
	function notify(header, text, type) {
		var head = $('<span>')
			.css({
				'color': '#000000',
				'font-size': '20px'
			})
			.text(header)
			.append('<br>');
		var color = '#ff0000';
		if (type == "success") {
			color = '#bbffbb';
		} else if (type == "warning") {
			color = '#ffbbbb';
		}
		$('.notify').remove();
		var noti = $('.notify-template').clone().removeClass('notify-template').addClass('notify');
		noti.find('.inner-notify').css({'background-color': color}).html(head).append(text);
		$('body').prepend(noti);
	}

	//что делать после отправки
	function after_send(response) {
		console.log(response);
		if (response.errors == false) {
			notify('Save success', 'redirect to "List Properties"', 'success');
			setTimeout(function () {
				if (is_redirect) {
					redirect(redirectUrl);
				}
			}, 3000);
		} else {
			console.log();
			if (typeof response.error == 'object') {
				var error = '';
				for (var i in response.error) {
					error = error + response.error[i] + '<br/> ';
				}
			}
			else {
				error = response.error;
			}
			notify('Save error', error, 'warning');
		}
	}

	//отправка данных на сервер
	function send_data(url, data) {
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data
		}).success(function (response) {
			console.log('[send_data > response]', response);
			after_send(response);
		}).error(function (data, key, value) {

			return false;
			//after_send(data);
		});
	}

	//сохранить и закрыть (+ редирект)
	function save() {
		var data = collect_data();
		console.log('[data]', data);
		var send_url = saveUrl;
		is_redirect = true;
		send_data(saveUrl, data);
	}

	//только сохранить
	function apply() {
		var data = collect_data();
		var send_url = saveUrl;
		console.log('[SAVEURL]', saveUrl);
		var send_result = send_data(send_url, data);
	}

	//редирект без сохранения
	function cancel() {
		notify('OK', 'ok', 'success');
		//DevExpress.ui.notify('text', 'type', 1000);
		//redirect('<?php $form->redirectUrl ?>');
	}

</script>

