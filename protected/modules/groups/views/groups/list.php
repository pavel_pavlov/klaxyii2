<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style type="text/css">
	#gridContainer {
		height: auto;
		width: 100%;
		margin: 50px 0;
	}
</style>

<div id="gridContainer"></div>
<script>
	$(document).ready(function () {
		var customers = <?= $str ?>;

		$("#gridContainer").dxDataGrid({
			dataSource: customers,
			paging: {
				pageSize: 10
			},
			editing: {
				editMode: 'row',
				editEnabled: true,
				removeEnabled: false,
				insertEnabled: false
			},
			onRowUpdating: function (e) {
				alert('RowUpdating');
			},
			pager: {
				showPageSizeSelector: true,
				allowedPageSizes: [5, 10, 20]
			},
			columns: [
				{
					dataField: 'id',
					caption: "ID",
					width: 30
				},
				{
					//dataField: 'group',
					caption: "Наименование группы",
					cellTemplate: function (container, options) {
						console.log(options.data);
						container.append('<a href="<?= $this->createUrl('/groups/groups/addedit', array('group_id' => '')); ?>' + options.data.id +'">' + options.data.group + '</a>');
					}
				},
				{
					dataField: 'description',
					caption: "Описание группы"
				}
			]
		});
	});
</script>

