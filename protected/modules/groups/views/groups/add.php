<?php
/* @var $this AclUsersController */
/* @var $model acl_users */
/* @var $form CActiveForm */
?>
<style>
	.top-line {
		margin-bottom: 10px;
		background-color: moccasin;
	}

	.hide {
		display: none;
	}

	span.top-line {
		margin-right: 20px;
		cursor: pointer;
		font-family: 'Helvetica Neue', 'Segoe UI', Helvetica, Verdana, sans-serif;
		font-size: 12px;
		font-weight: normal;
	}

	.left-parent {
		float: left;
		width: 400px;
		margin: 10px 10px 10px 0;
	}

	.right-parent {
		float: left;
		width: 400px;
		margin: 10px 10px 10px 0;
	}

	.left-table {
		border: #bbb 1px solid;
		height: 280px;
	}

	.right-table {
		border: #bbb 1px solid;
		height: 280px;
	}

	.clearfix{
		clear: both;
	}
	.buttons{
		margin-top: 10px;
	}
</style>

<div class="top-line">
	<span id="tab_main" class="top-line">Основа</span>
	<span id="tab_props" class="top-line">Свойства</span>
</div>

<script type="application/javascript">
	var renew = false;
	$('#tab_main').off('click').on('click', function () {
		$('.tab').addClass('hide');
		$('.tab_main').removeClass('hide');
	});
	$('#tab_props').off('click').on('click', function () {
		$('.tab').addClass('hide');
		$('.tab_props').removeClass('hide');
		if(renew === false){
			leftGrid.refresh();
			rightGrid.refresh();
			renew = true;
		}
	});
</script>

<div class="form">
	<div class="row">
		<div class="tab tab_main">
			<div style="width: 500px;">
				<?php
				$form = new dxForm();
				if(!empty($group)){
				$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id, 'group_id' => $group['id']));
				} else{
					$form->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));
				}

				$form->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/list"));
				?>
				<div class="row">
					<?php echo CHtml::label('Наименование группы *', 'group'); ?>
					<?php $form->dxTextField(
						'group',
						array(
							'value' => null,
							'placeholder' => 'Наименование группы'
						)
					); ?>
				</div>

				<div class="row">
					<?php echo CHtml::label('Описание группы', 'description'); ?>
					<?php $form->dxTextArea(
						'description',
						array(
							'height' => 200,
							//'width' => 500,
							'value' => null,
						)
					); ?>
				</div>

				<div class="row">
					<?php $form->dxCheckBox(
						'is_refs',
						array(
							'text' => 'Реферальная группа',
							'value' => false,
						)
					); ?>
				</div>
			</div>

		</div>
		<div class="tab tab_props hide">
			<div class="left-parent">
				<div id="leftGrid" class="left-table"></div>
			</div>
			<div class="right-parent">
				<div id="rightGrid" class="right-table"></div>
			</div>
			<div class="clearfix">
			</div>
		</div>
	</div>

	<div class="row buttons">
		<button class="dx-button" onClick="save();">
			save
		</button>
		<button class="hide dx-button dx-button-success" onClick="apply();">
			apply
		</button>
		<button class="hide dx-button dx-button-danger" onClick="cancel();">
			cancel
		</button>
	</div>
</div>
<!-- form -->

<script type="text/javascript">
	var is_redirect = false;
	var leftData = <?= $left ?>;
	var rightData = <?= $right ?>;


	//сбор данных со всепй формы
	function collect_data() {
		var dataForm = {};
		console.log(<?= $form->formName ?>);
		//сбор основных данных
		dataForm.group = <?= $form->formName ?>.group.option('value');
		dataForm.description = <?= $form->formName ?>.description.option('value');
		dataForm.is_refs = <?= $form->formName ?>.is_refs.option('value');
		dataForm.groupProps = rightData;

		console.log('[COLLECT DATAFORM]>',dataForm);
		return dataForm;
	}

	//редирект
	function redirect(url) {
		window.location = url;
	}

	//уведомление
	function notify(header, text, type) {
		var head = $('<span>')
			.css({
				'color':'#000000',
				'font-size': '20px'
			})
			.text(header)
			.append('<br>');
		var color = '#ff0000';
		if (type == "success") {
			color = '#bbffbb';
		} else if (type == "warning") {
			color = '#ffbbbb';
		}
		$('.notify').remove();
		var noti = $('.notify-template').clone().removeClass('notify-template').addClass('notify');
		noti.find('.inner-notify').css({'background-color': color}).html(head).append(text);
		$('body').prepend(noti);
	}

	//выполнится после отправки данных
	function after_send(response){
		console.log(response);
		if(response.errors == false){
			notify('Save success','redirect to "List Clients"','success');
			setTimeout(function(){
				if(is_redirect){
					redirect(redirectUrl);
				}
			},3000);
		} else {
			notify('Save error','Some errors...','warning');
		}
	}

	//отправка данных на сервер
	function send_data(url, data) {
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data
		}).success(function (response) {
			console.log('[send_data > response]',response);
			after_send(response);
		}).error(function (data, key, value) {

			return false;
			//after_send(data);
		});
	}

	//сохранить и закрыть (+ редирект)
	function save() {
		var data = collect_data();
		console.log(data, 'data');
		var send_url = saveUrl;
		//is_redirect = true;
		send_data(saveUrl, data);
	}

	//только сохранить
	function apply() {
		var data = collect_data();
		var send_url = saveUrl;
		console.log('[SAVEURL]', saveUrl);
		var send_result = send_data(send_url, data);
	}

	//редирект без сохранения
	function cancel() {
		notify('OK','ok','success');
		//DevExpress.ui.notify('text', 'type', 1000);
		//redirect('<?php $form->redirectUrl ?>');
	}

	var leftGrid = $("#leftGrid").dxDataGrid({
		dataSource: leftData,
		filterRow: {
			visible: true,
			applyFilter: "auto"
		},
		selection: {
			mode: 'single'
		},
		searchPanel: {
			visible: true,
			//width: 80,
			placeholder: 'Search...'
		},
		headerFilter: {
			visible: true
		},
		scrolling: {
			mode: 'virtual'
		},
		columns: [
			{
				caption: "Свойство",
				//width: 400,
				sortOrder: 'asc',
				dataField: 'name'
			},
			{
				caption: "Ед. изм.",
				width: 100,
				alignment: 'right',
				dataField: "measure"
			}
		],
		onRowClick: function (data) {
			rightData.push(data.data);
			var element = findInObjects2(leftData, 'id', data.data.id);
			leftData.splice(element.index, 1);
			leftGrid.refresh();
			rightGrid.refresh();
		}
	}).dxDataGrid('instance');

	var rightGrid = $("#rightGrid").dxDataGrid({
		dataSource: rightData,
		filterRow: {
			visible: true,
			applyFilter: "auto"
		},
		selection: {
			mode: 'single'
		},
		searchPanel: {
			visible: true,
			//width: 200,
			placeholder: 'Search...'
		},
		headerFilter: {
			visible: true
		},
		scrolling: {
			mode: 'virtual'
		},
		columns: [
			{
				caption: "Свойство",
				sortOrder: 'asc',
				dataField: 'name'
			},
			{
				caption: "Ед. изм.",
				width: 100,
				alignment: 'right',
				dataField: "measure"
			}
		],
		onRowClick: function (data) {
			leftData.push(data.data);
			var element = findInObjects2(rightData, 'id', data.data.id);
			rightData.splice(element.index, 1);
			leftGrid.refresh();
			rightGrid.refresh();
		}
	}).dxDataGrid('instance');
</script>
