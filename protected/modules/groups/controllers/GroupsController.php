<?php

class GroupsController extends Controller
{
	public $defaultAction = 'list';

	public function actionIndex() {
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('index');
	}

	public function actionAddEdit($group_id = 0) {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}

		$data = array();
		$group_id = (int)$group_id;
		//проверить группу на существование
		$oGroup = AppUserGroups::model()->findByPk($group_id);
		if ($group_id > 0 && empty($oGroup)) {
			throw new CDbException(404, 'Группа с указанным id не найдена.');
		}

		//если группа новая, то создать объект
		if ($group_id == 0) {
			$oGroup = new AppUserGroups();
		}

		//взять из базы все мои свойства
		$criteria = new CDbCriteria();
		$criteria->condition = 'acl_user_ref = :user_id';
		$criteria->params = array('user_id' => Yii::app()->user->id);
		$oProps = AppProperties::model()->findAll($criteria);
		$left = array();
		if (!empty($oProps)) {
			foreach ($oProps as $k => $prop) {
				$left[$prop->id] = $prop->attributes;
			}
		}

		$right = array();
		if ($group_id > 0) {
			//взять из базы свойства группы
			$criteria = new CDbCriteria();
			$criteria->addCondition('app_group_ref = :group_id');
			$criteria->params = array('group_id' => $group_id);
			$oProps = AppGroupHasProperty::model()->findAll($criteria);
			if (!empty($oProps)) {
				foreach ($oProps as $k => $prop) {
					$right[$prop->app_property_ref] = $prop->attributes;
				}
			}
		}

		//найти и удалить все элемены массива right из массива left и занести их в right
		foreach ($right as $key_right => $r) {
			$right[$key_right] = $left[$key_right];
			unset($left[$key_right]);
		}

		$left = array_values($left);
		$right = array_values($right);

		$js_left = CJSON::encode($left);
		$data['left'] = $js_left;

		$js_right = CJSON::encode($right);
		$data['right'] = $js_right;

		$formName = 'NewGroup';
		$data['formName'] = $formName;

		//проверяем, не передавалось ли что-нибудь уже...
		if (!empty($_POST)) {
			$JS = array(
				'errors' => false
			);
			$post = $_POST;
			if(!isset($post['groupProps'])){
				$post['groupProps'] = array();
			}
			//M::printr($post, '$post');
			//exit;
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if (empty(trim($post['group']))) {
					throw new Exception('Group name is empty.');
				}

				//сохранить группу в БД
				$oGroup->attributes = $post;
				$oGroup->is_refs = $oGroup->is_refs === 'true' ? true : false;
				$oGroup->acl_user_ref = Yii::app()->user->id;
				if (!$oGroup->save()) {
					throw new Exception($oGroup->getErrors());
				}

				//сохранить свойства в БД
				$new = $post['groupProps'];
				$old = $right;

				//привести массивы в нормальный вид (arr[id] = array...)
				$arr = array();
				foreach ($new as $k => $v) {
					$arr[$v['id']] = $v;
				}
				$new = $arr;
				$arr = array();
				foreach ($old as $k => $v) {
					$arr[$v['id']] = $v;
				}
				$old = $arr;

				//M::printr($old, '$old');
				//M::printr($new, '$new');

				//найти те, которые удалили
				$del = array();
				foreach ($old as $k => $v) {
					if (!isset($new[$k])) {
						//старое свойство удалено
						$del[] = $v;
					}
				}

				//найти те, которые добавили
				$add = array();
				foreach ($new as $k => $v) {
					if (!isset($old[$k])) {
						//новое добавлено
						$add[] = $v;
					}
				}
				//M::printr($del, '$del');
				//M::printr($add, '$add');


				//удалить из базы те, которых нет в новых данных
				foreach ($del as $k => $v) {
					$criteria = new CDbCriteria();
					$criteria->addCondition('app_group_ref = :group_id');
					$criteria->addCondition('app_property_ref = :prop_id');
					$criteria->params = array(
						'group_id' => $oGroup->id,
						'prop_id' => $v['id'],
					);
					//M::printr($criteria, '$criteria');
					AppGroupHasProperty::model()->deleteAll($criteria);
				}
				//добавить новые свойства
				foreach ($add as $k => $v) {
					$oProp = new AppGroupHasProperty();
					$oProp->app_group_ref = $oGroup->id;
					$oProp->app_property_ref = $v['id'];
					//M::printr($oProp, '$oProp');
					if(!$oProp->save()){
						throw new Exception($oProp->getErrors());
					}
				}
				$transaction->commit();
			} catch (Exception $E) {
				$JS['errors'] = true;
				$JS['error'] = $E->getMessage();
				$transaction->rollBack();
			}
			if (Yii::app()->request->isAjaxRequest) {
				print CJSON::encode($JS);
				exit;
			}
		}

		if ($group_id == 0) {
			$group = new AppUserGroups();;
		}
		$group = $oGroup->attributes;
		$data['group'] = $group;
		$this->render('addedit', $data);
	}

	public function actionEdit($group_id = 0) {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('login/login'));
		}
		$group_id = (int)$group_id;
		if (!$group_id) {
			$this->redirect(array('groups/list'));
		}

		$data = array();

		//взять группу пользователя

		$model = AppUserGroups::model()->findByPk($group_id);
		if (empty($model) || $model->acl_user_ref != Yii::app()->user->id) {
			//если такой нет, или чужая, то редирект на list
			$this->redirect(array('groups/list'));
		}
		//проверяем, не передавалось ли что-нибудь уже...
		if (isset($_POST['BaseAppUserGroups'])) {
			$model->attributes = $_POST['BaseAppUserGroups'];
			$model->acl_user_ref = Yii::app()->user->id;
			//проверяем валидность введенных данных
			if ($model->validate() && $model->save()) {
				//редиректим на страницу
				//$this->redirect(array('site/author','id'=>$model->id, 'title'=>$model->title));
				$this->redirect(array('groups/list'));
			}
		}
		$data['model'] = $model;
		$this->render('edit', $data);
	}

	public function actionList() {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('login/login'));
		}
		$data[] = array();

		$criteria = new CDbCriteria();
		$criteria->condition = "t.acl_user_ref = " . Yii::app()->user->id;
		$criteria->order = 't.id ASC';
		$groups = AppUserGroups::model()->findAll($criteria);

		$str = CJSON::encode($groups);

		$data['str'] = $str;
		$this->render('list', $data);
	}

}