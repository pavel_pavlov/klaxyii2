<?php

class PropertyController extends Controller
{
	//public $defaultAction = 'list';

	public function actionIndex() {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}
		$data = array();
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('index', $data);
	}

	public function actionAdd() {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}
		$data = array();

		$criteria = new CDbCriteria();
		$criteria->order = 'id ASC';
		$types = AppPropertyTypes::model()->findAll();
		$types = M::toArray($types);
		$data['types'] = $types;
		//M::printr($types, '$types');

		$formName = 'AddField';
		$data['formName'] = $formName;

		if (!empty($_POST)) {
			$JS = array(
				'errors' => false
			);

			$arr = array();
			foreach ($types as $k => $type) {
				$arr[$type['id']] = $type;
			}
			$types = $arr;
			//M::printr($types, '$types');

			//M::printr(Yii::app()->user->id, 'Yii::app()->user->id');
			$post = $_POST;
			//M::printr($post, '$post');
			//найти такое свойство в БД
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if (empty(trim($post['name']))) {
					throw new Exception('"name" is empty.');
				}
				$criteria = new CDbCriteria();
				$criteria->addCondition('name = :name');
				$criteria->params = array('name' => $post['name']);

				$oProp = AppProperties::model()->findAll($criteria);
				//M::printr($oProp, '$oProp');
				if (!empty($oProp)) {
					//если такое есть, то исключение с ошибкой
					throw new Exception('Property name "' . $post['name'] . '" is exist!');
				} else {
					//если такого нет, то добавить в БД
					$oProp = new AppProperties();
					$oProp->acl_user_ref = Yii::app()->user->id;
					$oProp->attributes = $post;
					if ($types[$oProp->app_property_type_ref]['is_multiple']) {
						//если множественное, то составить список
						$items = array();
						foreach($post['items'] as $key_item => $value){
							$item['id']=$key_item;
							$item['value'] = $value;
							$items[] = $item;
						}

						$oProp->data = CJSON::encode($items);
					}
					if (!$oProp->save()) {
						throw new Exception($oProp->errors);
					}
					//M::printr($oProp, '$oProp');
				}
				//exit;
				//сохранить данные в БД
				$transaction->commit();
			} catch (Exception $E) {
				//M::printr($E, '$E');
				$JS['errors'] = true;
				$JS['error'] = $E->getMessage();
				$transaction->rollBack();
			}
			if (Yii::app()->request->isAjaxRequest) {
				print CJSON::encode($JS);
				exit;
			}

			exit;
		}


		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('add', $data);
	}

	public function actionList() {
		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/auth/login/login'));
		}

		$data = array();
		//M::printr(Yii::app()->user, 'Yii::app()->user');

		//взять из базы все мои свойства

		$criteria = new CDbCriteria();
		$criteria->condition = 'acl_user_ref = :user_id';
		$criteria->params = array('user_id' => Yii::app()->user->id);
		$oProps = AppProperties::model()->findAll($criteria);
		$properties = CJSON::encode($oProps);
		//M::printr($properties, '$properties');

		$data['properties'] = $properties;

		$this->render('list', $data);
	}

}