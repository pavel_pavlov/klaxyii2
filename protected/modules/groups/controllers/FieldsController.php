<?php

class FieldsController extends Controller
{
	public $defaultAction = 'list';

	public function actionIndex() {
		$data = array();
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('index', $data);
	}

	public function actionAdd() {
		$data = array();

		$formName = 'AddField';
		$data['formName'] = $formName;
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('add', $data);
	}

	public function actionList() {
		$data = array();
		//M::printr(Yii::app()->user, 'Yii::app()->user');
		$this->render('list', $data);
	}

}