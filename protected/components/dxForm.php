<?php

// Виджет форм ввода для DevExtrime

/**
 * Class dxForm
 * Виджет форм ввода для DevExtreme
 * Документацию с примерами можно найти по адресу:
 * http://js.devexpress.com/Demos/WidgetsGallery/#index
 *
 * в переменные $params передаются значения в виде массива пар ключ-значение,
 * если значение массив -- он преобразовывается в json
 *
 *
 */
class dxForm
{
	public $formName = 'dxForm';
	public $saveUrl;
	public $redirectUrl;

	// Инициализируем javascript скрипты для подключения
	public function __construct($formName = false)
	{
		if ($formName) {
			$this->formName = $formName;
		}

		//по-умолчанию сохраняет в себя же
		$this->saveUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));

		//по-умолчанию пересылает на себя же
		$this->redirectUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id));

		$cs = Yii::app()->getClientScript();
		//$cs->registerCssFile($baseUrl.'/dx.light.compact.css');
		$cs->registerScriptFile('/js/functions.js', CClientScript::POS_HEAD);
		echo "
<style type='text/css'>
.Error{
	border: 1px solid #f88;
}
</style>
<script type='text/javascript'>
var {$this->formName} = {};

var saveUrl = '{$this->saveUrl}';
var redirectUrl = '{$this->redirectUrl}';
</script>
";
	}

	public function init() {
		echo "
<style type='text/css'>
.Error{
	border: 1px solid #f88;
}
</style>
<script type='text/javascript'>
{$this->formName} = {};

saveUrl = '{$this->saveUrl}';
redirectUrl = '{$this->redirectUrl}';
</script>
";
	}

		//преобразование данных $options в настройки для dx
	public function parseOptions($options)
	{
		$arr = array();
		foreach ($options as $key => $value) {
			if (is_object($value)) {
				$arr[] = "$key: " . $value;
			} elseif (is_array($value)) {
				//если массив объектов
				$arr[] = "$key: " . CJSON::encode($value);
			} elseif (is_bool($value)) {
				//если булево значение
				if ($value === true) {
					$arr[] = "$key: true";
				} else {
					$arr[] = "$key: false";
				}
			} elseif (is_numeric($value)) {
				//если число
				$arr[] = "$key: $value";
			} else {
				//если другое (строка)
				$arr[] = "$key: '$value'";
			}
		}
		return implode(",\n", $arr);
	}

	// Ввод даты с календаря
	/**
	 * Выводит форму для календаря
	 *
	 * @param $name - имя поля
	 * @param array $params - параметры для компонента dxTextBox в виде ассоциативного php массива
	 */
	public function dxDateBox($name, $params = array())
	{
		$default = array(
			//'showClearButton' => true,
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxDateBox({
{$jsOptions}
	}).dxDateBox('instance');
</script>
";
	}

	// Ввод даты с календаря
	/**
	 * Выводит форму для календаря
	 *
	 * @param $name - имя поля
	 * @param array $params - параметры для компонента dxTextBox в виде ассоциативного php массива
	 */
	public function dxCalendar($name, $params = array())
	{
		$default = array(
			//'showClearButton' => true,
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxCalendar({
{$jsOptions}
	}).dxCalendar('instance');
</script>
";
	}

	// Ввод строки
	/**
	 * Выводит форму для запроса строки
	 *
	 * @param $name - имя поля
	 * @param array $params - параметры для компонента dxTextBox в виде ассоциативного php массива
	 */
	public function dxTextField($name, $params = array())
	{
		$default = array(
			'showClearButton' => true,
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxTextBox({
{$jsOptions}
	}).dxTextBox('instance');
</script>
";
	}

	// Ввод строки
	/**
	 * Выводит форму для запроса строки
	 *
	 * @param $name - имя поля
	 * @param array $params - параметры для компонента dxTextBox в виде ассоциативного php массива
	 */
	public function dxTextBox($name, $params = array())
	{
		$default = array(
			'showClearButton' => true,
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxTextBox({
{$jsOptions}
	}).dxTextBox('instance');
</script>
";
	}

	//Скрытое поле
	/**
	 * Выводит форму для запроса строки
	 *
	 * @param $name - имя поля
	 * @param array $params - параметры для компонента dxTextBox в виде ассоциативного php массива
	 */
	public function dxHiddenField($name, $value)
	{
		echo "
<script type='text/javascript'>
	{$this->formName}['{$name}'] = '{$value}';
</script>
";
	}

	// Ввод пароля
	function dxPasswordField($name, $params = array())
	{
		$default = array(
			'showClearButton' => true,
			'placeholder' => 'Enter password',
			'mode' => 'password',
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxTextBox({
{$jsOptions}
	}).dxTextBox('instance');
</script>
";
	}

	//ввод текста в TextArea
	function dxTextArea($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxTextArea({
{$jsOptions}
	}).dxTextArea('instance');
</script>
";
	}

	public function dxSelectBox($name, $params = array())
	{
		$default = array(
			'placeholder' => 'Choose Item'
		);
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxSelectBox({
{$jsOptions}
	}).dxSelectBox('instance');
	//{$this->formName}['{$name}'].option('attr',{'class':'Error'});
</script>
";
	}

	public function dxRadioGroup($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxRadioGroup({
{$jsOptions}
	}).dxRadioGroup('instance');
</script>
";
	}

	public function dxCheckBox($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxCheckBox({
{$jsOptions}
	}).dxCheckBox('instance');
</script>
";
	}

	public function dxSwitch($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxSwitch({
{$jsOptions}
	}).dxSwitch('instance');
</script>
";
	}

	public function dxList($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxList({
{$jsOptions}
	}).dxList('instance');
</script>
";
	}

	//MultiSelect меню (dxTagBox)
	public function dxTagBox($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$this->formName}_{$name}'></div>
<script type='text/javascript'>
	{$this->formName}['{$name}'] = $('#{$this->formName}_{$name}').dxTagBox({
{$jsOptions}
	}).dxTagBox('instance');
</script>
";
	}

	//DataSource
	public function dxDataSource($name, $params = array())
	{
		$default = array();
		$options = array_merge($default, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<script type='text/javascript'>
	var {$name} = new DevExpress.data.ArrayStore({
{$jsOptions}
	});
</script>
";
	}

	// Кнопка отправки формы
	public function dxSubmitButton($name, $paramsDx = array(), $paramsAjax = array(), $redirect = false)
	{
		$defaultOptions = array(
			'text' => 'Apply',
			'type' => 'success'
		);
		$options = array_merge($defaultOptions, $paramsDx);
		$jsOptions = $this->parseOptions($options);
		$defaultAjax = array(
			'url' => CHtml::normalizeUrl(array("/" . Yii::app()->controller->module->id . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id)),
			'type' => 'POST',
			//'dataType' => 'json',
			'data' => new CDbExpression("dataForm.{$this->formName}"),
		);
		$ajax = array_merge($defaultAjax, $paramsAjax);
		$jsAjax = $this->parseOptions($ajax);
		if(!$redirect){
			$redirect = "false";
		}

		echo "
<div id='{$this->formName}_{$name}'></div>
<style>
.Error {
	background-color: #ff6161;
}
</style>
<script type='text/javascript'>
	$('#{$this->formName}_{$name}').dxButton({
{$jsOptions}
	}).click(function(e){
		e.preventDefault();
		var dataForm = {};
		var redirectForm = {$redirect};
		var z;

		dataForm['{$this->formName}'] = {};
		//console.log('formTable 1: ', {$this->formName});

		for (var i in {$this->formName}) {
			if (typeof({$this->formName}[i]) !== 'object') {
				dataForm.{$this->formName}[i]={$this->formName}[i];
				continue;
			}
			if({$this->formName}[i].NAME == 'dxDateBox'){
				dataForm.{$this->formName}[i] = {$this->formName}[i].option('text');
				continue;
			}
			if ({$this->formName}[i].option('values') !== undefined) {
				dataForm.{$this->formName}[i] = {$this->formName}[i].option('values');
				continue;
			}
			if ({$this->formName}[i].option('value') !== undefined){
				dataForm.{$this->formName}[i] = {$this->formName}[i].option('value');
				continue;
			}
		}
		//console.log('DATAFORM 2: ', dataForm);

		$.ajax({
{$jsAjax}
		}).success(function(response){
			var empty_div;
			console.log(response);
			/*
			if (response.errors === true) {
			//ошибки есть
				for (var i in response.error) {
					empty_div = $('<div>');
					console.log('[I]>', i);
					//пройтись по всем ошибкам и добавить в соответствующие места класс Error
					z = {$this->formName}[i].option('attr');
					empty_div.addClass(z.class);
					empty_div.addClass('Error');
					z.class = empty_div.attr('class');
					{$this->formName}[i].option('attr', z);
				}
			} else {
				if(redirectForm){
					window.location = redirectForm;
				}
			}
			*/
		}).error(function(data, key, value){
			alert('error: ' + data + ' : ' + key + ' : ' + value);
		});

	});
</script>
";
	}

	public function dxButton($name, $params = array(), $functionName = NULL)
	{
		$defaultOptions = array(
			'text' => 'Button',
		);
		$options = array_merge($defaultOptions, $params);
		$jsOptions = $this->parseOptions($options);
		echo "
<div id='{$name}'></div>
<script type='text/javascript'>
	$('#{$name}').dxButton({
{$jsOptions}
	});
</script>
";
	}

}