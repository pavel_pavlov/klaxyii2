<?php

class API
{
	public static function getClientById($id) {
		$oClient = AppClients::model()
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
					'appRequisites',
				)
			)
			->findByPk($id);
		if (empty($oClient)) {
			return false;
		}
		return $oClient;
	}

	public static function addQuestions($data) {
		$JS['errors'] = false;
		//M::printr($data, '$data');
		$oQuestions = new AppQuestions();
		$oQuestions->attributes = $data;
		$oQuestions->app_client_ref = $data['client_id'];
		//M::printr($oQuestions, '$oQuestions');
		if (!$oQuestions->save()) {
			$JS['errors'] = true;
			$JS['error'] = $oQuestions->getErrors();
		} else {
			$JS['errors'] = false;
			$JS['id'] = $data['client_id'];
		}

		if (Yii::app()->request->isAjaxRequest) {
			print CJSON::encode($JS);
			Yii::app()->end();
		}
	}

	public static function get_ref_group($ref) {
		$criteria = new CDbCriteria();
		$criteria->addCondition('acl_user_ref = :ref');
		$criteria->addCondition('is_refs = :is_refs');
		$criteria->params = array(':ref' => $ref, ':is_refs' => true);
		$oClient = AppUserGroups::model()->findAll($criteria);
		return $oClient[0]->attributes;
	}

	public static function reg_client($user) {
		$JS['errors'] = false;

		//проверить, указан ли телефон
		$phone = false;
		foreach ($user['responsibles'][0]['contacts'] as $k => $v) {
			if ($v['field_ref'] == 6 && $v['field_value'] !== null) {
				$phone = $v['field_value'];
			}
		}
		if (!$phone) {
			//если телефон не указан, то вернуть ошибку
			$JS['errors'] = true;
			$JS['error']['phone'] = "Не указан номер телефона.";
		}

		//Проверить, есть ли такой телефон в базе
		$criteria = new CDbCriteria();
		$criteria->addCondition('field_ref = 6');
		$criteria->addCondition('field_value = :phone');
		$criteria->params = array(':phone' => $phone);
		$oClient = AppContactFieldValues::model()->with(array('appRespRef'))->findAll($criteria);

		if (!empty($oClient)) {
			//в базе такой уже есть
			//посмотреть, проходил ли он опрос
			$client_id = $oClient[0]->appRespRef->app_client_ref;
			$criteria = new CDbCriteria();
			$criteria->addCondition('app_client_ref = :client_id');
			$criteria->params = array(':client_id' => $client_id);
			$oQuestions = AppQuestions::model()->find($criteria);
			//M::printr($oQuestions, '$oQuestions');

			if (!empty($oQuestions)) {
				//если опрос пройден, то ответить, что опрос пройден
				$JS['errors'] = true;
				$JS['error']['questions'] = "Опрос уже пройден.";
				$JS['id'] = $oQuestions->app_client_ref;
				$JS['is_questions'] = true;
			} else {
				//если опрос не пройден, то разрешить пройти опрос
				$JS['errors'] = false;
				$JS['id'] = $oClient[0]->appRespRef->app_client_ref;
			}
		} else {
			//в базе такого нет
			//зарегать
			//открываем транзакцию
			$transaction = Yii::app()->db->beginTransaction();
			try {
				//новый клиент
				$oClient = new AppClients();
				$oClient->app_client_ref = $user['app_client_ref'];
				$oClientRef = self::getClientById($user['app_client_ref']);
				$oClient->acl_user_ref = $oClientRef['acl_user_ref'];
				$header = trim($user['responsibles'][0]['first_name'] . " " . $user['responsibles'][0]['second_name']);
				$oClient->header = ($header !== '') ? $header : 'noname';
				$oClient->phones = isset($user['phones']) ? $user['phones'] : null;
				$oClient->email = isset($user['email']) ? $user['email'] : null;
				$oClient->sms_allow = false;
				if (!$oClient->save()) {
					//M::printr($oClient, '$oClient');
					throw new Exception('Exception: error save data in table AppClients');
				}

				//сохраняем ответственное лицо
				$oResponsible = new AppResponsibles();
				$oResponsible->app_client_ref = $oClient->id;
				$oResponsible->first_name = $user['responsibles'][0]['first_name'];
				$oResponsible->second_name = $user['responsibles'][0]['second_name'];
				$oResponsible->is_responsible = true;
				if (!$oResponsible->save()) {
					throw new Exception('Exception: error save data in table AppResponsibles');
				}

				//сохраняем контакты (email и телефон)
				foreach ($user['responsibles'][0]['contacts'] as $key_contact => $contact) {
					$oContact = new AppContactFieldValues();
					$oContact->field_ref = $contact['field_ref'];
					$oContact->field_value = $contact['field_value'];
					$oContact->app_resp_ref = $oResponsible->id;
					if (!$oContact->save()) {
						throw new Exception('Exception: error save data in table AppContactFieldValues');
					}
				}

				//сохраняем группы
				$oGroups = new AppClientHasGroups();
				$oGroups->app_client_ref = $oClient->id;
				$group = self::get_ref_group($oClientRef->acl_user_ref);
				$oGroups->app_user_group_ref = $group['id'];
				if (!$oGroups->save()) {
					throw new Exception('Exception: error save data in table AppClientHasGroups');
				}

				//сохраняем реферальную ссылку
				$oLink = new AppLinks();
				$oLink->app_client_ref = $oClient->id;
				$oLink->link = "http://{$_SERVER['SERVER_NAME']}/?ref={$oClient->id}";
				if (!$oLink->save()) {
					throw new Exception('Exception: error save data in table AppLinks');
				}

				//клиент создан, вернуть его id
				$JS['errors'] = false;
				$JS['id'] = $oClient->id;
				$transaction->commit();
			} catch (Exception $e) {
				$JS['errors'] = true;
				$JS['error'] = $e->getMessage();

				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				$transaction->rollBack();
			} //try

		}
		if (Yii::app()->request->isAjaxRequest) {
			print CJSON::encode($JS);
			Yii::app()->end();
		}
	}

	public static function getRatingQuestionsReferals() {
		$JS['errors'] = false;

		$q = Yii::app()->db->createCommand()
			->select(array('COUNT(ac.app_client_ref) top', 'ac.app_client_ref', 'acv.field_value phone', 'ac2.header view_name', 'ac2.dt_create'))
			->from('app_clients ac')
			->leftJoin('app_clients ac2', 'ac2.id = ac.app_client_ref')
			->leftJoin('app_responsibles ar', 'ar.app_client_ref = ac2.id')
			->leftJoin('app_contact_field_values acv', 'acv.app_resp_ref=ar.id AND acv.field_ref = 6')
			->leftJoin('app_questions aq', 'aq.app_client_ref = ac.id')
			->where('(coalesce(aq.req1,0)+coalesce(aq.req2,0)+coalesce(aq.req3,0)+coalesce(aq.req4,0)+coalesce(aq.req5,0)+coalesce(aq.req6,0)+coalesce(aq.req7,0)) > 0')
			->group(array('ac.app_client_ref', 'acv.field_value', 'ac2.header', 'ac2.dt_create'))
			->having('COUNT(ac.app_client_ref) > 0')
			->order(array('top DESC', 'dt_create ASC'));
		$rating = $q->queryAll();
		//M::printr($rating, '$rating');

		if (Yii::app()->request->isAjaxRequest) {
			print CJSON::encode($JS);
			Yii::app()->end();
		}

		return $rating;
	}


}