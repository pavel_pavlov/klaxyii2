<?php
// Виджет форм ввода для DevExtrime
class dxActiveForm extends CActiveForm
{
	public $hidden = 'hidden';

	// Инициализируем джава скрипты для подключения
	public function init()
	{
		Yii::app()->clientScript->registerScript('dxActiveForm','
function mainCreateTextArea(name,text)
{
	return $("#vis_"+name).dxTextArea({
		height: 90,
		value: text
	}).dxTextArea("instance");
}
function mainCreateTextBox(name,text)
{
	return $("#vis_"+name).dxTextBox({
		showClearButton: true,
		value: text
	}).dxTextBox("instance");
}
function mainCreateSelectBox(name,val,tabls)
{
	return $("#vis_"+name).dxSelectBox({
		items: tabls,
		value: val,
		displayExpr: "text",
		valueExpr: "id"
	}).dxSelectBox("instance");
}
var formTable = [];
		',0);
		return parent::init();
	}
	// Ввод текста
	function dxTextArea($model,$name,$aName='')
	{
		$dop_name = $aName.get_class($model).'['.$name.']';
		$uin_name = $aName.get_class($model).$name;
		echo '<div id="vis_'.$uin_name.'"></div>';
		echo '<input id="hid_'.$uin_name.'" type="'.$this->hidden.'" name="'.$dop_name.'"/>';
		echo "\n<script>\n	formTable['".$uin_name."'] = mainCreateTextArea('$uin_name','".str_replace('"','\\"',str_replace("\n",'\n',$model->$name))."');\n</script>";
	}

	// Ввод строки
	function dxTextField($model,$name,$aName='')
	{
		$dop_name = $aName.get_class($model).'['.$name.']';
		$uin_name = $aName.get_class($model).$name;
		echo '<div id="vis_'.$uin_name.'"></div>';
		echo '<input id="hid_'.$uin_name.'" type="'.$this->hidden.'" name="'.$dop_name.'"/>';
		echo "\n<script>\n	formTable['".$uin_name."'] = mainCreateTextBox('$uin_name','".str_replace('"','\\"',str_replace("\n",'\n',$model->$name))."');\n</script>";
	}

	// Выпадающий список
	function dxDropDownBox($model,$name,$table,$aName='')
	{
		$dop_name = $aName.get_class($model).'['.$name.']';
		$uin_name = $aName.get_class($model).$name;

		echo '<script>var tF_'.$uin_name.'=[';
		foreach($table as $i=>$val) echo '{"id":"'.$i.'","text":"'.str_replace('"','\\"',str_replace("\n",'\n',$val)).'"},';
		echo '];</script>';

		echo '<div id="vis_'.$uin_name.'"></div>';
		echo '<input id="hid_'.$uin_name.'" type="'.$this->hidden.'" name="'.$dop_name.'"/>';
		echo "\n<script>\n	formTable['".$uin_name."'] = mainCreateSelectBox('$uin_name','".$model->$name."',tF_".$uin_name.");\n</script>";
	}
	
	// Кнопка отправки формы
	function dxSubmitButton($value)
	{
		echo '<input value="'.$value.'" type="submit" onClick=\'for(var i in formTable)	$("#hid_" + i).val(formTable[i].option("value")); return true;\'></input>';
	}
}