<?php

/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 16.07.2015
 * Time: 10:52
 */
class M
{
	public static function printr($var, $name = false, $toVar = false) {
		ob_start();
		if (!YII_DEBUG) {
			return false;
		}
		print "<pre style='font-size: 13px; background-color: #ddd; padding: 5px; border: solid 1px #000;'>";
		if ($name) {
			print "<span style='color: #080; font-weight: bold; font-size: 20px;'>{$name}</span> => ";
		}
		print_r($var);
		print "</pre>\n";
		$mem = ob_get_clean();
		if ($toVar !== false) {
			return $mem;
		} else {
			print $mem;
		}
	}

	public static function toArray($objects, $field = false) {
		$arrays = array();
		foreach ($objects as $element) {
			if ($field) {
				$k = $element->$field;
				$arrays[$k] = $element->attributes;
			} else {
				$arrays[] = $element->attributes;
			}
		}
		return $arrays;
	}

	public static function json_encode($data) {
		if (is_object($data)) {
			$data = array($data);
		}
		foreach ($data as $k => $object) {
			M::printr($object, '$object');
			$relations = $object->relations();

			foreach ($relations as $key_relations => $relation) {
				M::printr($key_relations, '$key_relations');
				$new_object = $object->$key_relations;
				M::printr($new_object, '$new_object');
			}
		}


	}
}