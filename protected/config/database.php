<?php

// This is the database connection configuration.
return array(
	// uncomment the following lines to use a MySQL database
	//'connectionString' => 'pgsql:host=localhost;port=5432;dbname=clients_r0pub',
	'connectionString' => 'pgsql:host=localhost;port=5432;dbname=clients_dev',
	//'connectionString' => 'mysql:host=localhost;dbname=yiitest',
	'emulatePrepare' => true ,
	'username' => 'devteam',
	'password' => 'dbDevLinux',
	'charset' => 'utf8',
	/*
	// включаем профайлер
	'enableProfiling'=>true,
	// показываем значения параметров
	'enableParamLogging' => true,
	'log' => array(
		'class' => 'CLogRouter',
		'routes' => array(
			'db' => array(
				'class' => 'CWebLogRoute',
				'categories' => 'system.db.CDbCommand',
				'showInFireBug' => true //Показывать в FireBug или внизу каждой страницы
			)
		),
	)
	*/
);