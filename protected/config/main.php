<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('devextreme', dirname(__FILE__) . '/../extensions/devextreme');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'YiiTest Instance',
	//смена темы дизайна
	'theme' => 'bootstrap',

	// preloading 'log' component
	'preload' => array('log', 'bootstrap'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.models.base.*',
		'application.components.*',

		'application.modules.user.models.*',
		'application.modules.user.components.*',

		'application.modules.all.models.*',
		'application.modules.all.models.base.*',
		'application.modules.all.components.*',

		'application.modules.auth.models.*',
		'application.modules.auth.models.base.*',
		'application.modules.auth.components.*',

		'application.modules.clients.models.*',
		'application.modules.clients.models.base.*',
		'application.modules.clients.components.*',

		'application.modules.groups.models.*',
		'application.modules.groups.models.base.*',
		'application.modules.groups.components.*',

		'application.modules.sms.models.*',
		'application.modules.sms.models.base.*',
		'application.modules.sms.components.*',

		'application.modules.refs.models.*',
		'application.modules.refs.models.base.*',
		'application.modules.refs.components.*',

		'application.modules.questions.models.*',
		'application.modules.questions.models.base.*',
		'application.modules.questions.components.*',

		'application.modules.additionals.models.*',
		'application.modules.additionals.models.base.*',
		'application.modules.additionals.components.*',

		'application.modules.redirects.models.*',
		'application.modules.redirects.models.base.*',
		'application.modules.redirects.components.*',



	),

	'modules' => array(
		'dxTest',

		'user' => array(
			'tableUsers' => 'users',
			'tableProfiles' => 'profiles',
			'tableProfileFields' => 'profiles_fields',
		),

		'rights',
		'myModule',

		'auth'=>array(
			'defaultController' => 'login',
		),

		'clients'=>array(
			'defaultController' => 'clients',
		),

		'groups'=>array(
			'defaultController' => 'groups',
		),

		'sms'=>array(
			'defaultController' => 'sms',
		),

		'refs'=>array(
			'defaultController' => 'refs',
		),

		'questions'=>array(
			'defaultController' => 'questions',
		),

		'api'=>array(
			'defaultController' => 'api',
		),

		'testmod'=>array(
			'defaultController' => 'default',
		),

		'redirects'=>array(
			'defaultController' => 'default',
		),

		'additionals',

		// uncomment the following to enable the Gii tool
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'zxc123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('*'),
		),
	),

	// application components
	'components' => array(

		'devextreme' => array(
			'class' => 'devextreme.components.DxApplication',
		),

		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),

		'user' => array(
			//'class' => 'RWebUser',
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),

		'authManager' => array(
			'class' => 'RDbAuthManager',
			'defaultRoles' => array('Guest'),
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		//*/

		// database settings are configured in database.php
		'db' => require(dirname(__FILE__) . '/database.php'),

		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		/*
		'log'=>array(
			'class'=>'CLogRouter',
			'enabled'=>YII_DEBUG,
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				array(
					'class'=>'application.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters'=>array('*'),
				),
			),
		),
		//*/
		'mailer' => array(
			'class' => 'application.extensions.mailer.EMailer',
			'pathViews' => 'application.views.email',
			'pathLayouts' => 'application.views.email.layouts'
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'klaxwork@mail.ru',
		'supportControlEmail' => 'klaxwork@yandex.ru',
	),
);
