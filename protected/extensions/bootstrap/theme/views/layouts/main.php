<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Clients</title>

	<?php Yii::app()->devextreme->register(); ?>

</head>
<body>

<div id="menu"></div>

<script>
	$("#menu").ready(function () {
		var menuData = [
			{
				text: "Главная",
				url: "<?php echo Yii::app()->createUrl('/'); ?>"
			},
			{
				text: "Регистрация",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('/auth/reg/index'); ?>"
					},
					{
						text: "Reg",
						url: "<?php echo Yii::app()->createUrl('/auth/reg/reg'); ?>"
					}
				]
			},
			{
				text: "Авторизация",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('/auth/login/index'); ?>"
					},
					{
						text: "Авторизация",
						url: "<?php echo Yii::app()->createUrl('/auth/login/login'); ?>"
					},
					{
						text: "Выход",
						url: "<?php echo Yii::app()->createUrl('/auth/login/logout'); ?>"
					}
				]
			},
			{
				text: "Группы",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('groups/groups/index'); ?>"
					},
					{
						text: "Add",
						url: "<?php echo Yii::app()->createUrl('groups/groups/addedit'); ?>"
					},
					{
						text: "List",
						url: "<?php echo Yii::app()->createUrl('groups/groups/list'); ?>"
					}
				]
			},
			{
				text: "Дополнительные поля",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('groups/fields/index'); ?>"
					},
					{
						text: "Add",
						url: "<?php echo Yii::app()->createUrl('groups/fields/add'); ?>"
					},
					{
						text: "List",
						url: "<?php echo Yii::app()->createUrl('groups/fields/list'); ?>"
					}
				]
			},
			{
				text: "Клиенты",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('clients/clients/index'); ?>"
					},
					{
						text: "Add",
						url: "<?php echo Yii::app()->createUrl('clients/clients/addedit'); ?>"
					},
					{
						text: "List",
						url: "<?php echo Yii::app()->createUrl('clients/clients/list'); ?>"
					}
				]
			},
			{
				text: "Реферальная система",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('/refs/refs/index'); ?>"
					},
					{
						text: "Create",
						url: "<?php echo Yii::app()->createUrl('/refs/refs/create'); ?>"
					},
					{
						text: "List",
						url: "<?php echo Yii::app()->createUrl('/refs/refs/list'); ?>"
					}
				]
			},
			{
				text: "Статистика",
				items: [
					{
						text: "Index",
						url: "<?php echo Yii::app()->createUrl('/questions/questions/index'); ?>"
					},
					{
						text: "List",
						url: "<?php echo Yii::app()->createUrl('/questions/questions/list'); ?>"
					}
				]
			},
			{
				text: "SMS",
				items: [
					{
						text: "index",
						url: "<?php echo Yii::app()->createUrl('/sms/sms/index'); ?>"
					},
					{
						text: "New",
						url: "<?php echo Yii::app()->createUrl('/sms/sms/new'); ?>"
					}
				]
			}<?php if(!Yii::app()->user->isGuest): ?>,
			{
				text: "?Выход (<?= Yii::app()->user->name; ?>)",
				url: "<?php echo Yii::app()->createUrl('/auth/login/logout'); ?>"
			}
			<?php endif;?>
		];

		var dxMenu = $("#menu").dxMenu({
			dataSource: menuData,
			onItemClick: function (data) {
				var item = data.itemData;
				if (item.url) {
					location.href = item.url;
				}
			}
		}).dxMenu("instance");

	});

</script>


<?php echo $content; ?>

</body>
</html>
