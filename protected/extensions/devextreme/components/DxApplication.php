<?php

class DxApplication extends CApplicationComponent
{

	protected $forceCopyAssets = false;

	public function init()
	{
		parent::init();
	}

	public function register()
	{
		$this->registerAssets();
	}

	protected function registerAssets()
	{
		$basePath = Yii::getPathOfAlias('devextreme.assets');
		$baseUrl = Yii::app()->assetManager->publish($basePath, true, -1, $this->forceCopyAssets);

		$cs = Yii::app()->getClientScript();
		$ext = YII_DEBUG ? '.debug' : '';

		$cs->registerCssFile($baseUrl.'/css/dx.common.css');
		$cs->registerCssFile($baseUrl.'/css/dx.light.compact.css');

		$cs->registerScriptFile($baseUrl.'/js/jquery-2.1.3.min.js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($baseUrl.'/js/globalize.min.js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($baseUrl.'/js/dx.webappjs'.$ext.'.js', CClientScript::POS_HEAD);

	}
}
