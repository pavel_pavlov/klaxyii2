<?php

class TestController extends Controller
{

	//public $layout = '//layouts/friday13';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionIndex() {
		$data = array();

		// загрузим модуль и его кофиги
		//Yii::app()->setModules(array('photoalbum'));

		Yii::app()->getModule('testmod')->init();
		$x = new ZTestmod();
		M::printr($x, '$x');

		$this->renderPartial('index', $data);
	}

}