<?php

class RatingController extends Controller
{

	public $layout = '//layouts/friday13';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionIndex($ref = 1) {
		//$rating = API::getRatingQuestionsReferals();
		//M::printr($rating, '$rating');

		$criteria = new CDbCriteria();
		$criteria->limit = 10;
		$criteria->order = 'place ASC';
		$rating = AppRating::model()->findAll($criteria);
		//M::printr($rating, '$rating');

		$data['rating'] = $rating;
		$this->render('index', $data);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionLoad() {
		//действия по-умолчанию
		$default = array(
			'phone' => '',
			'offset' => 0,
			'limit' => 2,
		);
		$post = array_merge($default, $_POST);
		$post['offset'] = (int)$post['offset'];
		//M::printr($post, '$post');

		$criteria = new CDbCriteria();
		//если телефон есть, то указать условие отбора по последним 4-м цифрам
		if (!empty($post['phone'])) {
			$criteria->condition = "phone LIKE '%{$post['phone']}'";
		}
		$criteria->limit = $post['limit'];
		$criteria->offset = $post['offset'];
		$criteria->order = 'place ASC';
		$oRating = AppRating::model()->findAll($criteria);
		//M::printr($oRating, '$oRating');

		$rows = array();
		foreach ($oRating as $key => $row) {
			$pre = substr($row->phone, -10, 3);
			$row->phone = "+7({$pre})***-" . substr($row->phone, -4);
			$rows[] = $row->attributes;
		}
		//M::printr($rows, '$rows');

		if (Yii::app()->request->isAjaxRequest) {
			$JS = array(
				'errors' => false,
				'offset' => $post['offset'],
				'rows' => $rows
			);
			print CJSON::encode($JS);
			Yii::app()->end();
		}
	}

}