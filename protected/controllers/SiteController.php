<?php

class SiteController extends Controller
{

	public $layout = '//layouts/friday13old';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionIndex($ref = 1) {

		if (isset($_GET['rcode'])) {
			$criteria = new CDbCriteria();
			$criteria->addCondition('code = :code');
			$criteria->params = array('code' => $_GET['rcode']);
			$oRedirect = AppRedirects::model()->find($criteria);
			//M::printr($oRedirect, '$oRedirect');
			if (!empty($oRedirect) and isset($oRedirect->link)) {
				$this->layout = false;
				/*foreach (Yii::app()->log->routes as $route) {
					if($route instanceof CWebLogRoute) {
						$route->enabled = false;
					}
				}*/
				header("HTTP/1.1 301 Moved Permanently");
				header("location: {$oRedirect->link}");
				Yii::app()->end();
			}
		}


		$this->layout = '//layouts/friday13old';
		//$ref = 5;
		$data['ref'] = $ref;
		//M::printr($ref, '$ref');
		$this->render('index', $data);
	}


	public function actionStop() {

		Yii::app()->mailer->ClearAddresses();
		Yii::app()->mailer->Host = '10.10.0.8';
		Yii::app()->mailer->Username = 'postmaster@userdev.ru';
		Yii::app()->mailer->Password = 'ZdE32Df341';
		Yii::app()->mailer->IsSMTP(true);
		Yii::app()->mailer->SMTPAuth = true;
		Yii::app()->mailer->From = Yii::app()->params['adminEmail'];
		Yii::app()->mailer->FromName = "friday13.net";
		Yii::app()->mailer->IsHTML(true);
		Yii::app()->mailer->CharSet = 'UTF-8';
		Yii::app()->mailer->ContentType = 'text/html';
		Yii::app()->mailer->AddAddress("klaxwork@mail.ru");
		Yii::app()->mailer->AddAddress("deadmin@mail.ru");
		Yii::app()->mailer->Subject = 'friday13.net';
		$dt = strftime('%Y-%m-%d %H:%M:%S');
		$server = M::printr($_SERVER, '$_SERVER', true);
		Yii::app()->mailer->Body = $this->renderPartial('_mail', "{$dt}<br>\n{$server}", true);
		Yii::app()->mailer->Send();

		$this->render('stop');
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionSuccess($ref = 0) {
		$this->layout = '//layouts/friday13old';

		$ref = (int)$ref;
		$data = array();
		if ((int)$ref > 0) {
			$criteria = new CDbCriteria();
			$criteria->addCondition('app_client_ref = :ref');
			$criteria->params = array(':ref' => $ref);
			$oLink = AppLinks::model()->find($criteria);
			//M::printr($oLink, '$oLink');
			$link = $oLink->link;
			$data['link'] = $link;
		} else {
			$data['link'] = "http://{$_SERVER['SERVER_NAME']}/?ref=1";
		}
		$data['link_to_rating'] = "http://{$_SERVER['SERVER_NAME']}" . Yii::app()->createUrl("/rating");

		//достать почту чела с id = $ref;
		$criteria = new CDbCriteria();
		$criteria->condition = 't.id = ' . (int)$ref;
		$oMail = AppClients::model()
			->with(
				array(
					'appResponsibles',
					'appResponsibles.appContactFieldValues',
				)
			)
			->findAll($criteria);
		//M::printr($oMail, '$oMail');
		$oMail = $oMail[0]->appResponsibles[0]->appContactFieldValues;
		$email = Yii::app()->params['adminEmail'];
		foreach ($oMail as $key => $value) {
			if ($value->field_ref == 2) {
				$email = $value->field_value;
				break;
			}
		}
		//M::printr($email, '$email');

		Yii::app()->mailer->ClearAddresses();
		Yii::app()->mailer->Host = '10.10.0.8';
		Yii::app()->mailer->Username = 'postmaster@userdev.ru';
		Yii::app()->mailer->Password = 'ZdE32Df341';
		Yii::app()->mailer->IsSMTP(true);
		Yii::app()->mailer->SMTPAuth = true;
		Yii::app()->mailer->From = Yii::app()->params['adminEmail'];
		Yii::app()->mailer->FromName = "friday13.net";
		Yii::app()->mailer->IsHTML(true);
		Yii::app()->mailer->CharSet = 'UTF-8';
		Yii::app()->mailer->ContentType = 'text/html';
		Yii::app()->mailer->AddAddress($email);
		//Yii::app()->mailer->AddAddress(Yii::app()->params['supportControlEmail']);
		Yii::app()->mailer->Subject = 'friday13.net';
		Yii::app()->mailer->Body = $this->renderPartial('_mail', $data, true);
		Yii::app()->mailer->Send();


		$this->render('success', $data);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionMessage($ref = 0) {
		$this->layout = '//layouts/friday13old';

		$ref = (int)$ref;
		$data = array();
		if ((int)$ref > 0) {
			$criteria = new CDbCriteria();
			$criteria->addCondition('app_client_ref = ' . $ref);
			$oLink = AppLinks::model()->find($criteria);
			$link = $oLink->link;
			$data['link'] = $link;
		} else {
			$data['link'] = "http://{$_SERVER['SERVER_NAME']}/?ref=1";
		}
		//exit;
		$this->render('message', $data);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
					"Reply-To: {$model->email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
				Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact', array('model' => $model));
	}

	// Запрос на главной странице (поплавок)
	public function actionContact1() {
		//$this->catalogmenu = CatCategory::model()->getTree(null);
		$this->layout = 'catalog';
		$model = new ContactForm1;
		$page = Pages::model()->multilang()->findByPk(4);

		$this->pageTitle = "Контакты";
		if (isset($_POST['ContactForm1'])) {
			$model->attributes = $_POST['ContactForm1'];
//print_r($model);
			if ($model->validate()) {
//				print_r($model);die;
				Yii::app()->mailer->ClearAddresses();
				Yii::app()->mailer->From = Yii::app()->params['adminEmail'];
				Yii::app()->mailer->FromName = Yii::app()->params['adminEmail'];
				Yii::app()->mailer->IsHTML(true);
				Yii::app()->mailer->CharSet = 'UTF-8';
				Yii::app()->mailer->ContentType = 'text/html';
				Yii::app()->mailer->AddAddress(Yii::app()->params['adminEmail']);
				Yii::app()->mailer->AddAddress(Yii::app()->params['supportControlEmail']);
				Yii::app()->mailer->Subject = 'Сообщение с формы запроса на сайте';
				$data = array();
				$data['link'] = "http://{$_SERVER['SERVER_NAME']}/?ref=";
				Yii::app()->mailer->Body = $this->renderPartial('_mail', $data, true);
				Yii::app()->mailer->Send();
//                            print_r(Yii::app()->mailer);
//                            Die('---------------------------------');

				Yii::app()->user->setFlash('contact', 'Спасибо за Ваше письмо. Мы ответим так скоро насколько возможно.');
				echo '<div class="nays-form__errors" style = "border: 2px  solid #139420"><div class="nays-form__errors__title" style = "background: none repeat scroll 0 0 #E8FFE8">Сообщение успешно отправлено.</div><div class="nays-form__errors__list"><p>Спасибо за Ваш интерес.</p><p>Мы ответим так скоро насколько возможно.</p></div></div><script>$(".nays-form__unit").empty();</script>';
				Yii::app()->end();
				return;
			}
		}
		$this->layout = false;
		echo '<div class="nays-form__errors"><div class="nays-form__errors__title">При отправке сообщения были обнаружены следующие ошибки:</div><ul class="nays-form__errors__list">';
		foreach (($model->getErrors()) as $val)
			foreach ($val as $v) {
				echo '<li class="nays-form__errors__list__item">' . $v . '</li>';
			}
		echo '</ul></div>';
//		echo $this->renderPartial('application.views.front._contact',compact('model','page'));
//		$this->render('application.views.front._contact',compact('model','page'));
		Yii::app()->end();
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionRules() {
		$this->render('rules');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLog() {
		$dt = strftime('%Y-%m-%d %H:%M:%S');
		$server = M::printr($_SERVER, '$_SERVER', true);
		$post = M::printr($_POST, '$_POST', true);
		$f = fopen($_SERVER["DOCUMENT_ROOT"] . "/errors_js.log", 'a');
		fputs($f, "{$dt}\n");
		fputs($f, "{$_SERVER['HTTP_REFERER']}\n");
		fputs($f, "{$post}\n");
		fputs($f, "\n");
		fclose($f);

	}

}