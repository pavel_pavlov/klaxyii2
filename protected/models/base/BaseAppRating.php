<?php

/**
 * This is the model class for table "app_rating".
 *
 * The followings are the available columns in table 'app_rating':
 * @property string $id
 * @property integer $place
 * @property integer $refs_count
 * @property string $app_clients_ref
 * @property string $phone
 * @property string $view_name
 * @property string $dt_create
 * @property integer $discount
 *
 * The followings are the available model relations:
 * @property AppClients $appClientsRef
 */
class BaseAppRating extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_rating';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_clients_ref', 'required'),
			array('place, refs_count, discount', 'numerical', 'integerOnly'=>true),
			array('phone, view_name, dt_create', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, place, refs_count, app_clients_ref, phone, view_name, dt_create, discount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appClientsRef' => array(self::BELONGS_TO, 'AppClients', 'app_clients_ref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'place' => 'Place',
			'refs_count' => 'Refs Count',
			'app_clients_ref' => 'App Clients Ref',
			'phone' => 'Phone',
			'view_name' => 'View Name',
			'dt_create' => 'Dt Create',
			'discount' => 'Discount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('place',$this->place);
		$criteria->compare('refs_count',$this->refs_count);
		$criteria->compare('app_clients_ref',$this->app_clients_ref,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('view_name',$this->view_name,true);
		$criteria->compare('dt_create',$this->dt_create,true);
		$criteria->compare('discount',$this->discount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseAppRating the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
