<?php
$this->pageTitle = Yii::app()->name . ' - Контакты';
$this->breadcrumbs = array(
	'Контакты',
);
?>
<!-- template of row -->
<!-- map-1 -->
<div class="contact-zone--contacts col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">

	<h2>Главный офис</h2>
	<div class="contact-zone--map" id="map-01"></div>

	<div class="info clearfix">
		<div class="col-lg-4 col-md-12 col-sm-12">
			<h3>Связь</h3>
			<p><span>Телефон</span> +7(499)403-36-07</p>
			<p><span>E-mail</span> info@friday13.net</p>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12">
			<h3>График</h3>
			<p>ПН-ВС — <span><em>круглосуточно</em></span></p>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12">
			<h3>Адрес</h3>
			<p>Москва, Большой Демидовский переулок, д.12</p>
		</div>
	</div>
</div>


<!-- scripts -->
<script src="/friday13/assets/js/jquery.min.js"></script>
<script src="/friday13/assets/js/jquery.mousewheel.min.js"></script>

<!-- для кнопко поделиться -->
<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>

<script src="/friday13/assets/js/bootstrap.min.js"></script>
<script src="/friday13/assets/js/smoothscroll.js"></script>
<script src="/friday13/assets/js/c.js"></script>
<!-- maps   -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
	var MY_MAPTYPE_ID = 'custom_style';
	function initialize() {
		// стили
		var featureOpts=[{elementType:"geometry.fill",stylers:[{color:"#010c23"}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{hue:"#08ff00"},{color:"#000000"},{invert_lightness:!0},{lightness:-39}]},{featureType:"road.arterial",elementType:"geometry.stroke",stylers:[{visibility:"on"}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{visibility:"on"},{saturation:-74}]},{},{featureType:"road.local",stylers:[{visibility:"off"}]},{featureType:"landscape.man_made",elementType:"geometry.stroke",stylers:[{visibility:"on"},{color:"#808080"}]},{elementType:"labels.text.stroke",stylers:[{color:"#ffffff"},{visibility:"on"},{weight:.1}]},{elementType:"labels.text.fill",stylers:[{visibility:"off"}]},{featureType:"poi",elementType:"labels",stylers:[{visibility:"off"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]},{},{featureType:"road.arterial",stylers:[{visibility:"on"},{color:"#808080"},{saturation:-69}]},{},{elementType:"labels.text.stroke",stylers:[{color:"#999999"}]},{featureType:"administrative.locality",elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#00ffab"},{saturation:-49}]},{},{featureType:"water",elementType:"geometry.fill",stylers:[{visibility:"on"},{color:"#41658b"}]},{},{featureType:"road.arterial",elementType:"geometry.stroke",stylers:[{visibility:"off"}]},{featureType:"road.arterial",elementType:"geometry.fill",stylers:[{visibility:"on"},{color:"#676d7a"}]},{}];
		var mapOptions={zoom:15,scrollwheel:!0,disableDefaultUI:!0,
				center:new google.maps.LatLng(55.771896,37.684088), // ЦЕНТР
				mapTypeControlOptions:{mapTypeIds:[google.maps.MapTypeId.ROADMAP,MY_MAPTYPE_ID]},mapTypeId:MY_MAPTYPE_ID},
		// ---------------- ПИН 1 ----------------------
			map=new google.maps.Map(document.getElementById("map-01"), // ID КАРТЫ
				mapOptions),styledMapOptions={name:"Custom Style"},customMapType=new google.maps.StyledMapType(featureOpts,styledMapOptions);map.mapTypes.set(MY_MAPTYPE_ID,customMapType);
		var image = '/friday13/assets/img/pin.png';
		var myLatLng = new google.maps.LatLng(55.7683658,37.6782409); // Координаты
		var marker = new google.maps.Marker({position: myLatLng, map: map, icon: image,
			title:"Главный офис Пятница-13"
		});
		var contentString = '<div id="map_window"><h4>Главный офис</h4><p>Москва, <br>Большой Демидовский переулок, 12, <br>м. Бауманская</p><p>Эл. почта: <a href="mailto:info@friday13.net">info@friday13.net</a></p></div>';
		var infowindow = new google.maps.InfoWindow({content: contentString});
		google.maps.event.addListener(marker, 'click', function() { infowindow.open( map, marker); });

		// ---------------- ПИН 2 ----------------------
		map2=new google.maps.Map(document.getElementById("map-02"), // ID КАРТЫ
			mapOptions),styledMapOptions={name:"Custom Style"},customMapType=new google.maps.StyledMapType(featureOpts,styledMapOptions);map2.mapTypes.set(MY_MAPTYPE_ID,customMapType);
		var myLatLng2 = new google.maps.LatLng(55.7683658,37.6782409); // Координаты
		var marker2 = new google.maps.Marker({position: myLatLng2, map: map2, icon: image,
			title:"Главный офис Пятница-13"
		});
		var contentString2 = '<div id="map_window"><h4>Главный офис</h4><p>Москва, <br>Большой Демидовский переулок, 12, <br>м. Бауманская</p><p>Эл. почта: <a href="mailto:info@friday13.net">info@friday13.net</a></p></div>';
		var infowindow = new google.maps.InfoWindow({content: contentString2});
		google.maps.event.addListener(marker2, 'click', function() { infowindow.open( map2, marker2); });
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
