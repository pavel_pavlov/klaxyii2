<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Пятница-13 | Опрос</title>

	<meta name="description" content=""/>
	<meta name="keywords" content=" "/>

	<link rel="icon" type="image/png" href="/friday13v002/assets/img/fav.png"/>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- style -->
	<link href="/friday13v002/assets/css/bootstrap.css" rel="stylesheet">
	<link href="/friday13v002/assets/css/style.css" rel="stylesheet">

	<script src="/friday13v002/assets/js/jquery.min.js"></script>
</head>
<body>

<!-- vacansy -->
<div class="vacansy container">
	<div class="row">

		<!-- modal vacansy: start -->
		<div class="modal fade vacant" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-big">
				<div class="continue-form container-fluid">
					<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10">
						<a href="#fake" class="close" data-dismiss="modal" aria-label="Close"><img
								src="/friday13/assets/img/close.png">Закрыть</a>

						<h1>Вакансии</h1>

						<h2>Программист PHP</h2>

						<h2><span>от 40 000 до 75 000 руб.</span> <i>опыт работы 1-3 года</i></h2>

						<p>Разыскиваем грамотного программиста для работы в команде над крупным web-проектом в сфере
							финансов, учета и аналитики.</p>

						<p><strong>Что мы ждём от Вас и с чем придется работать:</strong></p>
						<ul>
							<li>опыт работы от 1 года</li>
							<li>владение навыками работы с каким-либо php-фреймворком (Yii, Symfony, Laravel,
								CodeIgniter, Zend Framework)
							</li>
							<li>понимание принципов ООП, модели MVC, паттернов</li>
							<li>хорошее владение MySQL (умение читать журнал запросов и оптимизировать их; построение
								индексов, внешних ключей, процедур, триггеров)
							</li>
							<li>приветствуется опыт c MongoDB, PostgreSQL</li>
							<li>AJAX, JavaScript, JQuery, верстка также приветствуется (верстать шаблоны целиком не
								надо, но при необходимости уметь внести правки или сверстать отдельные элементы)
							</li>
							<li>SVN</li>
							<li>умение разбираться в чужом коде</li>
							<li>желание повышать свою квалификацию в процессе работы</li>
							<li>отличное чувство юмора</li>
						</ul>
						<p><strong>Что НЕ нужно будет делать:</strong></p>
						<ul>
							<li>писать стандартные веб-сайты с использованием популярных CMS, таких как Bitrix, Drupal,
								Joomla и т.д., путём "натягивания" HTML-шаблонов на готовые модули
							</li>
							<li>использовать устаревшие приёмы программирования и технологии</li>
						</ul>
						<p><strong>Наше предложение:</strong></p>
						<ul>
							<li>испытательный срок 2 месяца</li>
							<li>оформление по трудовой</li>
							<li>оборудованное рабочее место</li>
							<li>молодой и дружный коллектив, который любит работать и активно отдыхать</li>
							<li>возможность самореализации</li>
							<li>заработная плата по итогам собеседования</li>
						</ul>
						<h3>Ключевые навыки</h3>

						<p>PHP, PHP5, MS SQL, PostgreSQL, BackboneNode.js, JQuery, MVC, .NET Framework</p>

						<h3>Адрес</h3>

						<p>Москва, Большой Демидовский переулок, 12, м. Бауманская</p>

						<p>Напишите нам: <a href="mailto:info@friday13.net">info@friday13.net</a></p>

						<h3>Тип занятости</h3>

						<p>Полная занятость, полный день</p>
					</div>
				</div>
			</div>
		</div>
		<!-- modal vacansy: end -->
	</div>
</div>




<!-- header menu -->
<div class="header-top">
	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">=</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="mini-logo visible-xs-inline" href="<?php echo Yii::app()->createUrl('/'); ?>"><img src="/friday13v002/assets/img/logo-mini@2x.png" height="30" width="54" alt=""></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">

<!--
					<div class="regions hidden-sm">
						Регион <span>Москва</span>
						<div class="cit-list">
							<p><a href="#">Архангельск</a></p>
							<p><a href="#">Владивосток</a></p>
							<p><a href="#">Екатеринбург</a></p>
							<p><a href="#">Москва</a></p>
							<p><a href="#">Санкт-Петербург</a></p>
							<p><a href="#">Тверь</a></p>
							<p><a href="#">Ярославль</a></p>
						</div>
					</div>
//-->


					<ul class="navigation-top">
						<!--li><a href="#fake" data-toggle="modal" data-target=".vacant"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Вакансии </a></li-->
						<li><a href="<?php echo Yii::app()->createUrl('/'); ?>">Опрос</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('rating'); ?>">Рейтинг</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('site/rules'); ?>">Правила</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('contact'); ?>">Контакты</a></li>
					</ul>


<!--
					<ul class="social social--right hidden-sm">
						<li><a href="#fake" id="tw">
								<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 22 22"
									 preserveAspectRatio="none">
									<path fill-rule="evenodd" clip-rule="evenodd"
										  d="M11.2,0C2.2,0,0,2.2,0,11.1C0,19.7,2.2,22,11.1,22    c8.7,0,11.2-2.2,11.2-10.9C22.3,2.3,19.9,0,11.2,0z M16.2,7.9c-0.3,0.4-0.6,0.8-1,1.1c0,0.1,0,0.2,0,0.3c0,2.7-2.1,5.9-6,5.9    c-1.2,0-2.3-0.3-3.2-0.9c0.2,0,0.3,0,0.5,0c1,0,1.9-0.3,2.6-0.9c-0.9,0-1.7-0.6-2-1.4c0.1,0,0.3,0,0.4,0c0.2,0,0.4,0,0.6-0.1    c-1-0.2-1.7-1-1.7-2c0,0,0,0,0,0C6.7,9.9,7.1,10,7.4,10C6.8,9.7,6.5,9,6.5,8.3c0-0.4,0.1-0.7,0.3-1c1,1.2,2.6,2.1,4.3,2.2    C11,9.3,11,9.1,11,9c0-1.1,0.9-2.1,2.1-2.1c0.6,0,1.1,0.3,1.5,0.7c0.5-0.1,0.9-0.3,1.3-0.5c-0.2,0.5-0.5,0.9-0.9,1.1    C15.5,8.1,15.9,8,16.2,7.9L16.2,7.9z"/>
							</a></li>
						<li><a href="#fake" id="ig">
								<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 22 22"
									 preserveAspectRatio="none">
									<g>
										<path fill-rule="evenodd" clip-rule="evenodd"
											  d="M13.8,10.4h-0.6c0,0.2,0.1,0.4,0.1,0.6c0,1.2-1,2.1-2.1,2.1       C10,13.1,9,12.2,9,11c0-0.2,0-0.4,0.1-0.6H8.5c-0.3,0-0.6,0.3-0.6,0.6v2.7c0,0.3,0.3,0.6,0.6,0.6h5.4c0.3,0,0.6-0.3,0.6-0.6V11      C14.4,10.7,14.1,10.4,13.8,10.4z"/>
										<path fill-rule="evenodd" clip-rule="evenodd"
											  d="M13.3,9.4h0.5c0.3,0,0.5-0.2,0.5-0.5V8.3c0-0.3-0.2-0.5-0.5-0.5       h-0.5c-0.3,0-0.5,0.2-0.5,0.5v0.5C12.7,9.2,13,9.4,13.3,9.4z"/>
										<path fill-rule="evenodd" clip-rule="evenodd"
											  d="M11.1,12.1c1.1,0,1.1-1.1,1.1-1.1s-0.1-1.1-1.1-1.1       c-1,0-1.1,1.1-1.1,1.1S10,12.1,11.1,12.1z"/>
										<path fill-rule="evenodd" clip-rule="evenodd"
											  d="M11.2,0C2.2,0,0,2.2,0,11.1C0,19.7,2.2,22,11.1,22        c8.7,0,11.2-2.2,11.2-10.9C22.3,2.3,19.9,0,11.2,0z M15.4,14.2c0,0.6-0.5,1.1-1.1,1.1H7.9c-0.6,0-1.1-0.5-1.1-1.1V7.8       c0-0.6,0.5-1.1,1.1-1.1h6.4c0.6,0,1.1,0.5,1.1,1.1V14.2z"/>
									</g>
								</svg>
							</a></li>
						<li><a href="#fake" id="vm">
								<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 22 22"
									 preserveAspectRatio="none">
									<path fill-rule="evenodd" clip-rule="evenodd"
										  d="M11.2,0C2.2,0,0,2.2,0,11.1C0,19.7,2.2,22,11.1,22    c8.7,0,11.2-2.2,11.2-10.9C22.3,2.3,19.9,0,11.2,0z M15.8,8.9c-0.5,2.9-3.5,5.4-4.4,6c-0.9,0.6-1.7-0.2-2-0.8   c-0.3-0.7-1.3-4.4-1.6-4.7C7.6,9,6.8,9.6,6.8,9.6L6.4,9.1c0,0,1.6-1.9,2.8-2.1c1.3-0.3,1.3,2,1.6,3.2c0.3,1.2,0.5,1.9,0.8,1.9   c0.3,0,0.8-0.7,1.3-1.7c0.6-1,0-1.9-1.1-1.3C12.3,6.5,16.3,5.9,15.8,8.9z"/>
								</svg>
							</a></li>
						<li><a href="#fake" id="fb">
								<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 22 22"
									 preserveAspectRatio="none">
									<path fill-rule="evenodd" clip-rule="evenodd"
										  d="M11.2,0C2.2,0,0,2.2,0,11.1C0,19.7,2.2,22,11.1,22    c8.7,0,11.2-2.2,11.2-10.9C22.3,2.3,19.9,0,11.2,0z M13.2,11.6l-1.2,0l0,3.9l-2,0v-3.9l-1.2,0V9.9l1.2,0l0-1.8  c0-0.8,0.8-1.6,1.4-1.6l1.8,0l0,1.7l-0.9,0C12,8.2,12,8.5,12,8.6v1.2h1.6L13.2,11.6z"/>
								</svg>
							</a></li>
					</ul>
-->

				</div>
			</div>
		</nav>
	</div>
</div>

<!-- logo -->
<div class="index--logo container hidden-xs">
	<a href="<?= Yii::app()->createUrl('/') ?>"><img src="/friday13v002/assets/img/logo.png" alt="пятница 13"></a>
</div>

<!-- contact -->
<div class="content-zone rating-search content-zone--promo">
	<div class="container">

<?php echo $content; ?>

	</div>
</div>

<footer>
	<div class="fog-1"></div>
	<div class="fog-2"></div>
	<div class="safe hidden-sm hidden-xs"></div>

	<div class="container">

		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">© 2015 — Пятница 13-е</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs shared">
				<!-- <p>&nbsp;</p> -->
				<!-- Yandex.Metrika counter -><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32057686 = new Ya.Metrika({ id:32057686, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/32057686" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
				<span>Рассказать друзьям</span><br>

				<div class="yashare-auto-init" data-yashareLink="http://friday13.net/" data-yashareL10n="ru"
					 data-yashareType="none"
					 data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6"><a href="http://holmax.ru" target="_blank">Разработка сайта</a> —
				holmax.ru
			</div>
		</div>
	</div>
</footer>

<!-- scripts -->
<script src="/friday13v002/assets/js/jquery.mousewheel.min.js"></script>

<!-- для кнопко поделиться -->
<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>

<script src="/friday13v002/assets/js/bootstrap.min.js"></script>
<script src="/friday13v002/assets/js/smoothscroll.js"></script>
<script src="/friday13v002/assets/js/c.js"></script>
</body>
</html>

