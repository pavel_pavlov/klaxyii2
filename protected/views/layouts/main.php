<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <title>Report</title>

	<?php Yii::app()->devextreme->register(); ?>

</head>
<body>

<div id="menu"></div>

<script>
$("#menu").ready(function()
{


var menuData = [
{
    text: "XГлавная",
    url: "<?php echo Yii::app()->createUrl('/'); ?>"
},
{
    text: "Отчеты",
    items: [
    {
        text: "Отчет: Карточка счета",
        url: "<?php echo Yii::app()->createUrl('finReport/r1AP'); ?>"
    },
    {
        text: "Отчет: Оборотно сальдовая ведомость по счету",
        url: "<?php echo Yii::app()->createUrl('finReport/r2OSV'); ?>"
    }
    ]
},
{
    text: "Документы",
    items: [
    {
        text: "Все документы",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/admin'); ?>"
    },
    {
        text: "Новый документ",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/create'); ?>"
    }
    ]
},
{
    text: "Ввести документ",
    items: [
    {
        text: "Выручка IN",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/inin'); ?>"
    },
    {
        text: "Выручка OUT",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/inouta'); ?>"
    },
    {
        text: "Прочий расход",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/prrasxod'); ?>"
    },
    {
        text: "Возврат подотчетных средств",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/retpod'); ?>"
    },
    {
        text: "Выдача подотчетных средств",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/getpod'); ?>"
    },
    {
        text: "Поступление перемещения",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/postper'); ?>"
    },
    {
        text: "Выбытие перемещения",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/outper'); ?>"
    },
    {
        text: "Выплаты контрагенты",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/vypkontr'); ?>"
    },
    {
        text: "Поступление от контрагентов",
        url: "<?php echo Yii::app()->createUrl('m1AppDocumentBase/postkontr'); ?>"
    }
    ]
},
{
    text: "Справочники",
    items: [
		{
			text: "Счета учета",
			items: [
				{
					text: "Просмотреть счета учета",
					url: "<?php echo Yii::app()->createUrl('m1DicAccountPlan/admin'); ?>"
				},
				{
					text: "Новый счет учета",
					url: "<?php echo Yii::app()->createUrl('m1DicAccountPlan/create'); ?>"
				},
				{
					text: "Новый субсчет учета",
					url: "<?php echo Yii::app()->createUrl('m1DicAccountSubplan/create'); ?>"
				}
			]
		}
    ]
}
];

var dxMenu = $("#menu").dxMenu({
    dataSource: menuData,
    onItemClick: function (data) {
        var item = data.itemData;
        if (item.url)
        {
            location.href = item.url;
        }
    }
}).dxMenu("instance");

});

</script>


<?php echo $content; ?>

</body>
</html>
