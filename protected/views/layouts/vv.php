
<!-- vacansy -->
<div class="vacansy container">
	<div class="row">
		<p class="col-lg-12">
			<!-- menu-vacansy -->
			<a href="#fake" data-toggle="modal" data-target=".vacant"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Вакансии </a>
		</p>

		<!-- modal vacansy: start -->
		<div class="modal fade vacant" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-big">
				<div class="continue-form container-fluid">
					<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10">
						<a href="#fake" class="close" data-dismiss="modal" aria-label="Close"><img
								src="/friday13/assets/img/close.png">Закрыть</a>

						<h1>Вакансии</h1>

						<h2>Программист PHP</h2>

						<h2><span>от 40 000 до 75 000 руб.</span> <i>опыт работы 1-3 года</i></h2>

						<p>Разыскиваем грамотного программиста для работы в команде над крупным web-проектом в сфере
							финансов, учета и аналитики.</p>

						<p><strong>Что мы ждём от Вас и с чем придется работать:</strong></p>
						<ul>
							<li>опыт работы от 1 года</li>
							<li>владение навыками работы с каким-либо php-фреймворком (Yii, Symfony, Laravel,
								CodeIgniter, Zend Framework)
							</li>
							<li>понимание принципов ООП, модели MVC, паттернов</li>
							<li>хорошее владение MySQL (умение читать журнал запросов и оптимизировать их; построение
								индексов, внешних ключей, процедур, триггеров)
							</li>
							<li>приветствуется опыт c MongoDB, PostgreSQL</li>
							<li>AJAX, JavaScript, JQuery, верстка также приветствуется (верстать шаблоны целиком не
								надо, но при необходимости уметь внести правки или сверстать отдельные элементы)
							</li>
							<li>SVN</li>
							<li>умение разбираться в чужом коде</li>
							<li>желание повышать свою квалификацию в процессе работы</li>
							<li>отличное чувство юмора</li>
						</ul>
						<p><strong>Что НЕ нужно будет делать:</strong></p>
						<ul>
							<li>писать стандартные веб-сайты с использованием популярных CMS, таких как Bitrix, Drupal,
								Joomla и т.д., путём "натягивания" HTML-шаблонов на готовые модули
							</li>
							<li>использовать устаревшие приёмы программирования и технологии</li>
						</ul>
						<p><strong>Наше предложение:</strong></p>
						<ul>
							<li>испытательный срок 2 месяца</li>
							<li>оформление по трудовой</li>
							<li>оборудованное рабочее место</li>
							<li>молодой и дружный коллектив, который любит работать и активно отдыхать</li>
							<li>возможность самореализации</li>
							<li>заработная плата по итогам собеседования</li>
						</ul>
						<h3>Ключевые навыки</h3>

						<p>PHP, PHP5, MS SQL, PostgreSQL, BackboneNode.js, JQuery, MVC, .NET Framework</p>

						<h3>Адрес</h3>

						<p>Москва, Большой Демидовский переулок, 12, м. Бауманская</p>

						<p>Напишите нам: <a href="mailto:info@friday13.net">info@friday13.net</a></p>

						<h3>Тип занятости</h3>

						<p>Полная занятость, полный день</p>
					</div>
				</div>
			</div>
		</div>
		<!-- modal vacansy: end -->
	</div>
</div>

