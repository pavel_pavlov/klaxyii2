<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Пятница-13 | Опрос</title>

	<meta name="description" content=""/>
	<meta name="keywords" content=" "/>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="developer" content="Компания ХОЛМАКС (г. Тверь) - www.holmax.ru, 8 800 222-90-22"/>
	<link rel="icon" type="image/png" href="/friday13/assets/img/fav.png"/>
	<!-- style -->
	<link href="/friday13/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/friday13/assets/css/style.min.css" rel="stylesheet">
</head>
<body>

<!-- vacansy -->
<div class="vacansy container">
	<div class="row">
		<p class="col-lg-12 aaasss">
			<!--a href="#fake" data-toggle="modal" data-target=".vacant">
				<span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
				Вакансии
			</a-->
			<a href="<?php echo Yii::app()->createUrl('/'); ?>">Опрос</a>
			<a href="<?php echo Yii::app()->createUrl('rating'); ?>">Рейтинг</a>
			<a href="<?php echo Yii::app()->createUrl('site/rules'); ?>">Правила</a>
			<a href="<?php echo Yii::app()->createUrl('contact'); ?>">Контакты</a>
		</p>

		<!-- modal vacansy: start -->
		<div class="modal fade vacant" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-big">
				<div class="continue-form container-fluid">
					<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10">
						<a href="#fake" class="close" data-dismiss="modal" aria-label="Close"><img src="/friday13/assets/img/close.png" >Закрыть</a>
						<h1>Вакансии</h1>
						<h2>Программист PHP</h2>
						<h2><span>от 40 000 до 75 000 руб.</span> <i>опыт работы 1-3 года</i></h2>
						<p>Разыскиваем грамотного программиста для работы в команде над крупным web-проектом в сфере финансов, учета и аналитики.</p>
						<p><strong>Что мы ждём от Вас и с чем придется работать:</strong></p>
						<ul>
							<li>опыт работы от 1 года</li>
							<li>владение навыками работы с каким-либо php-фреймворком (Yii, Symfony, Laravel, CodeIgniter, Zend Framework)</li>
							<li>понимание принципов ООП, модели MVC, паттернов</li>
							<li>хорошее владение MySQL (умение читать журнал запросов и оптимизировать их; построение индексов, внешних ключей, процедур, триггеров)</li>
							<li>приветствуется опыт c MongoDB, PostgreSQL</li>
							<li>AJAX, JavaScript, JQuery, верстка также приветствуется (верстать шаблоны целиком не надо, но при необходимости уметь внести правки или сверстать отдельные элементы)</li>
							<li>SVN</li>
							<li>умение разбираться в чужом коде</li>
							<li>желание повышать свою квалификацию в процессе работы</li>
							<li>отличное чувство юмора</li>
						</ul>
						<p><strong>Что НЕ нужно будет делать:</strong></p>
						<ul>
							<li>писать стандартные веб-сайты с использованием популярных CMS, таких как Bitrix, Drupal, Joomla и т.д., путём "натягивания" HTML-шаблонов на готовые модули</li>
							<li>использовать устаревшие приёмы программирования и технологии</li>
						</ul>
						<p><strong>Наше предложение:</strong></p>
						<ul>
							<li>испытательный срок 2 месяца</li>
							<li>оформление по трудовой</li>
							<li>оборудованное рабочее место</li>
							<li>молодой и дружный коллектив, который любит работать и активно отдыхать</li>
							<li>возможность самореализации</li>
							<li>заработная плата по итогам собеседования</li>
						</ul>
						<h3>Ключевые навыки</h3>
						<p>PHP, PHP5, MS SQL, PostgreSQL, BackboneNode.js, JQuery, MVC, .NET Framework</p>
						<h3>Адрес</h3>
						<p>Москва, Большой Демидовский переулок, 12, м. Бауманская</p>
						<p>Напишите нам: <a href="mailto:info@friday13.net">info@friday13.net</a></p>
						<h3>Тип занятости</h3>
						<p>Полная занятость, полный день</p>
					</div>
				</div>
			</div>
		</div>
		<!-- modal vacansy: end -->
	</div>
</div>

<!-- big-logo -->
<div class="index--logo container">
	<a href="<?= Yii::app()->createUrl('/') ?>"><img src="/friday13/assets/img/logo.png" alt="пятница 13"></a>
</div>

<!-- content -->
<div class="content-zone content-zone--promo">
	<div class="container">
		<?php echo $content; ?>
	</div>
</div>

<!-- footer -->
<footer>
	<div class="fog-1"></div>
	<div class="fog-2"></div>
	<div class="safe hidden-sm hidden-xs"></div>

	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">© 2015 — Пятница 13-е</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs shared">
				<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32057686 = new Ya.Metrika({ id:32057686, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/32057686" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
				<!-- Заменить абзац с пробелом на кнопки соц сетей (коммент ниже), если их использовать. -->
				<p>&nbsp;</p>

				<!-- Кнопки соц сетей с Яндекса. --><!-- <span>Рассказать друзьям</span><br>
                    <script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,lj"></div> -->

			</div>
			<div class="col-lg-4 col-md-4 col-sm-6"><a href="http://holmax.ru" target="_blank">Разработка сайта</a> —
				holmax.ru
			</div>
		</div>
	</div>
</footer>

<!-- paralax -->
<div class="paralax hidden-sm hidden-xs">
	<div class="left hidden-xs">
		<div class="face hidden-xs"></div>
	</div>
	<div class="right hidden-xs">
		<div class="bat hidden-xs hidden-sm hidden-md"></div>
	</div>
</div>


</body>
</html>

