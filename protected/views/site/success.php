<div class="content-zone content-zone--promo">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10" style="text-align: center;">

				<h1>Спасибо. Вы загеристрированы в системе.</h1>

				<p style="font-size: 1em;">Ваша индивидуальная ссылка для передачи друзьям и знакомым: <span style="text-transform: none; color: #00FFAB;"><?php echo $link; ?></span></p>
				<p style="font-size: 1em;">Также мы отправили ее на указанный Вами электронный адрес.<br />
				Размер своей скидки, количество людей, прошедших по Вашей ссылке и место в рейтинге Вы можете отслеживать в верхнем меню, во вкладке «Рейтинг».</p>
				<p style="font-size: 1em;">Желаем удачи! Команда "Пятница 13-е"</p>

			</div>
		</div>
	</div>
</div>

