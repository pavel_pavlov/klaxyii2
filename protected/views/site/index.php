<div class="uptop">
<h2 class="green header1">Новый формат развлечений в Москве!</h2>
<p class="green newtext1">Стать героем экшна, триллера, ужаса или детектива теперь реально благодаря реалити-квестам от проекта «Пятница 13-е»,  где тебя и твоих друзей мы погрузим в мир интриг, тайн и страха, откуда вы должны будете благополучно выбраться!</p>
</div>
<div class="row">
	<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10"
		 style="text-align: center;">

		<h1>Скоро открытие</h1>

		<p>Ответь на 7 вопросов и получи возможность пройти<br>один из квестов <span>абсолютно бесплатно</span>!
		</p>


		<form action="" id="opros">
			<!-- inputs -->
			<p><input name="name" placeholder="Имя" type="text"><input name="sname" placeholder="Фамилия" type="text"></p>
			<p><input id="email" name="email" class="error" placeholder="Эл. почта" type="text"><input id="phone" name="phone" class="error" placeholder="Номер телефона" type="text"></p>

			<div id="required_rows_error" class="panel panel-danger" style="display: none;">
				<div class="panel-heading">
					<h3 class="panel-title">Внимание!</h3>
				</div>
				<div class="panel-body">
					Для участия Вы должны обязательно указать свою электронную почту и номер телефона.
				</div>
				<br/>
			</div>

			<p><em style="font-weight: 400; color: rgba(255, 255, 255, 0.5); font-size: 0.819em;">*Перед прохождением
					опроса ознакомьтесь с <a href="<?= Yii::app()->createUrl('/site/rules'); ?>">правилами</a>.</em></p>

			<p style="text-align: center;">
				<input name="ref" type="hidden" value="<?php print $ref; ?>"> <input id="sendAjax" type="submit"
																					 value="Пройти опрос">
			</p>

			<div class="modal fade questions" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-big">
					<div class="continue-form container-fluid">
						<div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-6 col-md-8 col-sm-10">
							<a href="#fake" class="close" data-dismiss="modal" aria-label="Close"><img src="/friday13/assets/img/close.png" >Закрыть</a>

							<h1>Ответье на вопросы</h1>

							<!-- question 0 -->
							<h2>1</h2>
							<p>Участвовали ли ранее в каких-либо квестах?</p>
							<div class="answer">
								<label><input type="radio" name="req0" value="1"><span>Да</span></label><br>
								<label><input type="radio" name="req0" value="2"><span>Нет</span></label>
								<label><input type="radio" name="req0" value="3"><span>Не понял</span></label>
							</div>

							<!-- question 1 -->
							<h2>2</h2>
							<p>На квест по мотивам какого жанра кино, литературного <br>произведения Вы бы пошли с друзьями?</p>
							<div class="answer answer-2">
								<label><input type="radio" name="req1" value="1"><span>Ужас</span></label>
								<label><input type="radio" name="req1" value="2"><span>Комедия</span></label>
								<label><input type="radio" name="req1" value="3"><span>Приключения</span></label>
								<label><input type="radio" name="req1" value="4"><span>Криминал</span></label>
								<label><input type="radio" name="req1" value="5"><span>Фантастика</span></label>
								<label><input type="radio" name="req1" value="6"><span>Эротика</span></label>
							</div>

							<!-- question 2 -->
							<h2>3</h2>
							<p>Что произвело бы на Вас впечатление при прохождении<br> квеста в жанре хоррор?</p>
							<div class="answer">
								<label><input type="radio" name="req2" value="1"><span>Мистика, сверхъестественные создания</span></label><br>
								<label><input type="radio" name="req2" value="2"><span>Изуродованные тела, оторванные конечности</span></label>
							</div>

							<!-- question 3 -->
							<h2>4</h2>
							<p>Какие спецэффекты Вы желаете видеть в квестах?</p>
							<div class="answer answer-2">
								<label><input type="radio" name="req3" value="1"><span>Световые</span></label>
								<label><input type="radio" name="req3" value="2"><span>Звуковые</span></label>
								<label><input type="radio" name="req3" value="3"><span>Механические</span></label>
								<label><input type="radio" name="req3" value="4"><span>Электрические</span></label>
								<label><input type="radio" name="req3" value="5"><span>Сенсорные</span></label>
							</div>

							<!-- question 4 -->
							<h2>5</h2>
							<p>Какие задачи Вы бы с удовольствием решали <bR>при прохождении квеста:</p>
							<div class="answer">
								<label><input type="radio" name="req4" value="1"><span>Математические</span></label><br>
								<label><input type="radio" name="req4" value="2"><span>Логические (например решаемые с конца)</span></label><br>
								<label><input type="radio" name="req4" value="3"><span>Пазлы</span></label><br>
								<label><input type="radio" name="req4" value="4"><span>Химические</span></label><br>
								<label><input type="radio" name="req4" value="5"><span>Физические (применение физической силы)</span></label>
							</div>

							<!-- question 5 -->
							<h2>6</h2>
							<p>Какой стиль игры Вы предпочитаете?</p>
							<div class="answer">
								<label><input type="radio" name="req5" value="1"><span>Командная работа (слаженные действия, к победе приходит вся команда)</span></label><br>
								<label><input type="radio" name="req5" value="2"><span>Соревноваться с участниками своей команды (победитель один)</span></label>
							</div>

							<!-- question 6 -->
							<h2>7</h2>
							<p>Вы бы пожертвовали своей одеждой <br>(например испачкать, порвать, намочить и т.п.) <br>во время прохождения квеста?</p>
							<div class="answer">
								<label><input type="radio" name="req6" value="1"><span>Да</span></label><br>
								<label><input type="radio" name="req6" value="2"><span>Нет</span></label>
							</div>

							<!-- question 7 -->
							<h2>8</h2>
							<p>Квест с каким финалом Вам интересен?</p>
							<div class="answer">
								<label><input type="radio" name="req7" value="1"><span>Выйти из комнаты</span></label><br>
								<label><input type="radio" name="req7" value="2"><span>Освободить друга</span></label><br>
								<label><input type="radio" name="req7" value="3"><span>Набрать большее кол-во баллов</span></label><br>
								<label><input type="radio" name="req7" value="4"><span>Выиграть приз</span></label>
							</div>

							<div class="send">
								<input id="client_id" type="hidden" name="client_id" value=""><input id="sendQ"
																									 type="submit"
																									 value="Отправить">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>

<!-- scripts -->
<script src="/friday13/assets/js/jquery.min.js"></script>
<script src="/friday13/assets/js/bootstrap.min.js"></script>
<script src="/friday13/assets/js/c.min.js"></script>
<script type="application/javascript">
	$('#sendAjax').click(function (e) {
		e.preventDefault();

		var rexpr = /^(8|\+7)\d{10}$/;
		if ($("#email").val().indexOf('@') == -1 || !rexpr.test($("#phone").val())) {
			$("#required_rows_error").show();
			return;
		}

		//$('.questions').modal('show');
		//$('.continue-form.container-fluid').css({'display': 'none'});

		$.ajax({
			url: '<?= Yii::app()->createUrl('questions/questions/Friday13Client'); ?>',
			type: 'post',
			dataType: 'json',
			data: $('input[name="name"],input[name="sname"],input[name="email"],input[name="phone"],input[name="ref"]')
		})
		.error(function (xhr, status, error) {
			window.location = '<?php echo Yii::app()->createUrl('site/stop'); ?>';
		})
		.success(function (response) {
			console.log('[RESPONSE]>', response);
			if (response['errors'] === false) {
				//ошибок нет
				$('.questions').modal('show');
				//$('.continue-form.container-fluid').css({'display': 'block'});
				$('#client_id').val(response['id']);
			} else if (response['errors'] === true) {
				//ошибки есть
				if (response['is_questions'] === true) {
					//опрос уже пройден
					window.location = '<?= Yii::app()->createUrl('site/message',array('ref' => '__id__')); ?>'.replace(/__id__/, response.id);
					//setInterval($('.questions').remove(),500);
				}
			}
		});
	});

	$('#sendQ').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: '<?= Yii::app()->createUrl('questions/questions/Friday13Questions'); ?>',
			type: 'post',
			dataType: 'json',
			data: $('#opros').serialize()
		}).success(function (response) {
			console.log('[RESPONSE]>', response);
			if (response.errors === false) {
				console.log(response);
				window.location = '<?= Yii::app()->createUrl('site/success',array('ref' => '__id__')); ?>'.replace(/__id__/, response.id);
			}
		});

		//Отправить результаты опроса на сервер
	});
	//name sname email phone
</script>
