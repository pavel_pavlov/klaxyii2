<p>Добрый день!</p>
<p>Ваша индивидуальная ссылка для передачи друзьям и знакомым <?= $link ?>.</p>
<p>Размер своей скидки, количество людей, прошедших по Вашей ссылке и место в рейтинге Вы можете отслеживать <a href="<?= $link_to_rating ?>">здесь</a>.</p>
<p>Желаем удачи! Команда "Пятница 13-е".</p>