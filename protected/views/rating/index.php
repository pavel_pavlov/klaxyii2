<?php
$this->pageTitle = Yii::app()->name . ' - Рейтинг';
$this->breadcrumbs = array(
	'Рейтинг',
);
?>
<!-- template of row -->
<div style="display:none;">
	<table id="template_row"><!-- тэг таблицы браться не будет -->
		<tr>
			<td class='hidden-xs'>
				<row_place/>
			</td>
			<td>
				<row_view_name/>
			</td>
			<td>
				<row_phone/>
			</td>
			<td class='hidden-xs'>
				<row_refs_count/>
			</td>
			<td>
				<row_discount/>
			</td>
		</tr>
	</table>
</div>
<!-- template of row -->

<h1 class="center">Рейтинг участников опроса</h1>

<p class="actio">по акции «<a href="<?php echo Yii::app()->createUrl('/'); ?>">Пройди опрос</a>»</p>

<h2>Опрос закончится 13.11.2015 в 13:13:13</h2>

<p><em style="font-size: 0.75em;">*Воспользоваться скидкой пользователи могут в <a
			href="https://ru.wikipedia.org/wiki/%D0%9D%D0%BE%D0%B2%D0%BE%D0%BB%D1%83%D0%BD%D0%B8%D0%B5"
			target="_blank">период новолуния</a>.</em></p>

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

	<div class="form center">
		<p>Поиск по номеру телефона <input id="phone" name="phone" type="text" placeholder="Последние 4 цифры"> <input
				id="find" type="submit" value=""></p>
	</div>

	<table class="table" id="table_rating">
		<tr>
			<th class="hidden-xs">№</th>
			<th>Имя</th>
			<th>Телефон</th>
			<th class="hidden-xs">Кол-во друзей</th>
			<th>Скидка</th>
		</tr>
		<?php
		/*
		foreach ($rating as $key_row => $row) {
			$phone_len = strlen($row['phone']);
			$pre = substr($row['phone'], -10, 3);
			$phone = "+7({$pre})***-" . substr($row['phone'], -4);

			print "
				<tr id='row_{$row['place']}'>
					<td class='hidden-xs'>{$row['place']}</td>
					<td>{$row['view_name']}</td>
					<td>{$phone}</td>
					<td class='hidden-xs'>{$row['refs_count']}</td>
					<td>{$row['discount']}</td>
				</tr>
					";
		}
		*/
		?>
	</table>

	<p class="center"><a id="more" href="#" class="show-more">Показать еще</a></p>

	<div class="star">
		<p><span class="green">С 1 по 5 место</span> &mdash; скидка 100% на прохождение одного квеста</p>

		<p><span class="green">С 6 по 16 место</span> &mdash; скидка 40% на прохождение одного квеста</p>

		<p><span class="green">С 17 по 27 место</span> &mdash; скидка 30% на прохождение одного квеста</p>

		<p><span class="green">С 28 по 38 место</span> &mdash; скидка 20% на прохождение одного квеста</p>

		<p><span class="green">Остальные счастливчики</span> за свои старания, получат скидку 10% на прохождение одного
			квеста</p>
	</div>
</div>

<script type="application/javascript">
	var
		phone = '',
		offset = 0,
		limit = 10,
		template;

	function load_more() {
		var data = {};
		data.phone = phone;
		data.offset = offset;
		data.limit = limit;

		$.ajax({
			url: '<?= Yii::app()->createUrl('rating/load') ?>',
			type: 'post',
			dataType: 'json',
			data: data
		}).success(function (response) {
			var row;
			//принять данные от сервера
			console.log(response);
			if (response.errors === false) {
				//занести данные в шаблон
				var i;
				for (i in response.rows) {
					row = $('#template_row').find('tr').clone();
					row.attr({'id': 'row_' + response.rows[i].place})
					row.find('row_place').replaceWith(response.rows[i].place);
					row.find('row_view_name').replaceWith(response.rows[i].view_name);
					row.find('row_phone').replaceWith(response.rows[i].phone);
					row.find('row_refs_count').replaceWith(response.rows[i].refs_count);
					row.find('row_discount').replaceWith(response.rows[i].discount);

					//добавить шаблон в таблицу
					$('#table_rating').append(row);
				}
			}
			offset += response.rows.length;
		}).error(function (data, key, value) {
			//alert('error: ' + data + ' : ' + key + ' : ' + value);
		});
	}

	$('#find').off('click').on('click', function (e) {
		e.preventDefault();
		$('tr[id ^= "row_"]').remove();
		var p = $('#phone').val();
		offset = 0;
		if (p.length == 4) {
			phone = p;
		}else{
			phone = '';
		}
		load_more();
	});

	$('#more').off('click').on('click', function (e) {
		e.preventDefault();
		load_more();
	});

	$(document).ready(function () {
		load_more();
	});
</script>
