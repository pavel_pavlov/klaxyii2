/**
 *
 * Быстрый поиск элемента в массиве объектов по свойству элемента
 *
 * findInObjects(arr, 'name', value);
 * вернет элемент, в котором element.name = value
 *
 * findInObjects(arr, value);
 * вернет элемент, в котором element.id = value
 *
 *
 * @param arr -- массив объектов или массивов
 * @param field -- поле, по которому нужно искать
 * @param value -- значение, которое должно быть в поле
 * @returns {boolean}
 */
function findInObjects(arr, field, value) {
	if (value == undefined) {
		value = field;
		field = 'id';
	}
	for (var i = 0, l = arr.length; arr[i] && arr[i][field] !== value; i++) {
	}
	return i === l ? false : arr[i];
}

/**
 * делает то же, что и предыдущая функция,
 * но в отличие от предыдущей функции вернет не только индекс объекта
 * а массив с индексом и самим искомым объектом
 *
 * @param arr
 * @param field
 * @param value
 * @returns {boolean}
 */
function findInObjects2(arr, field, value) {
	if (value == undefined) {
		value = field;
		field = 'id';
	}
	for (var i = 0, l = arr.length; arr[i] && arr[i][field] !== value; i++) {
	}
	var x = {};
	x.index = i;
	x.element = arr[i];
	return i === l ? false : x;
}

function saveToLog(data) {
	console.log('[DATA]', data);
	$.ajax({
		url: 'index.php?r=site/log',
		type: 'POST',
		dataType: 'json',
		data: data
	});
	alert('saveToLog');
}

/**
 * функция возвращает копию объекта, без учета функции, если таковые имелись
 * @param arr Array()
 * @returns arr2 Array()
 */
function copy(arr) {
	var arr2 = [];
	for (var index in arr) {
		var type = typeof arr[index];
		if (type === 'function') {
			continue;
		}
		if (type === 'array' || type === 'object') {
			arr2[index] = {};
			arr2[index] = copy(arr[index]);
		}
		if (type === 'string' || type === 'number') {
			arr2[index] = arr[index];
		}
	}
	return arr2;
}

/**
 * Функция удаляет из из массивов все элементы, имеющиеся в обоих массивах
 * и возвращает массив оставшихся элементов
 * arr[0] = left;
 * arr[1] = right;
 * @param left
 * @param right
 * @returns {*}
 */
function diffArrays(left, right) {
	var arr1 = copy(left);
	var arr2 = copy(right);
	var pos;
	var index;
	//удаляем из первого массива все, что есть во втором
	for (index in arr2) {
		var elem2 = arr2[index];
		pos = arr1.indexOf(elem2);
		if (pos > -1) {
			delete arr2[index];
			delete arr1[pos];
		}
	}
	//удаляем из второго массива все, что есть в первом
	for (index in arr1) {
		var elem1 = arr1[index];
		pos = arr2.indexOf(elem1);
		if (pos > -1) {
			delete arr1[index];
			delete arr2[pos];
		}
	}
	var arr = [];
	arr[0] = array_values(arr1);
	arr[1] = array_values(arr2);
	return arr;
}


function compareArrays(arr1, arr2) {
	arr1.sort();
	arr2.sort();

	for (var index in arr2) {
		pos = arr1.indexOf(arr2[index]);
	}

	return arr;
}


/**
 * функция array_values возвращает только значения массива, без ключец
 *
 * @param input
 * @returns {Array}
 */
function array_values(input) {
	//
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

	var tmp_arr = new Array(), cnt = 0;

	for (var key in input) {
		tmp_arr[cnt] = input[key];
		cnt++;
	}

	return tmp_arr;
}


/**
 * проверка объекта на пустоту
 * @param obj
 * @returns {boolean}
 */
function empty(obj) {
	for (var i in obj) {
		if (obj.hasOwnProperty(i)) {
			return false;
		}
	}
	return true;
}


















